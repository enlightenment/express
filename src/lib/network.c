#include "private.h"

static void 
_cb_server_free(void *data)
{
   Express_Server *srv;

   if (!(srv = data)) return;
   if (srv->name) eina_stringshare_del(srv->name);
   if (srv->realname) eina_stringshare_del(srv->realname);
   free(srv);
}

static int 
_find_crlf(const unsigned char *data, int length, int *lf)
{
   int i = 0;

   *lf = 0;
   for (; i < length; i++)
     {
        /* find crlf (\r\n) */
        if ((data[i] == 0x0D) && 
            (i < (length - 1)) && (data[i + 1] == 0x0A))
          {
             *lf = 2;
             return i;
          }

        /* find just lf */
        if (data[i] == 0x0A) 
          {
             *lf = 1;
             return i;
          }
     }

   return -1;
}

static void 
_process_buffer(Express_Network *net, char *data, int length)
{
   const char *params[16]; // RFC 1459 says Max 15 params for a message
   const char *prefix = 0, *cmd = 0;
   char *ptr, *str, *end;
   int index = 0, code = 0;

   memset((char *)params, 0, sizeof(params));

   end = (data + length);
   ptr = data;

   fprintf(stderr, "Process Buffer: %d %s", length, data);

   /* strip off the prefix */
   if (data[0] == ':')
     {
        while ((*ptr) && (*ptr != ' '))
          ptr++;

        *ptr++ = '\0';

        /* skip leading colon */
        prefix = (data + 1);

        /* strip off nick */
        for (str = (data + 1); *str; str++)
          {
             if ((*str == '@') || (*str == '!'))
               {
                  *str = '\0';
                  break;
               }
          }
     }
   /* else */
   /*   { */
   /*      CRI("Partial Prefix Message: %s", data); */
   /*   } */
   if (prefix)
      prefix = eina_stringshare_add(prefix);

   /* parse out any 3-digit command numbers */
   if ((isdigit(ptr[0]) && (isdigit(ptr[1])) && (isdigit(ptr[2]))))
     {
        ptr[3] = '\0';
        code = atoi(ptr);
        ptr += 4;
     }
   else
     {
        str = ptr;
        while ((*ptr) && (*ptr != ' '))
          ptr++;
        *ptr++ = '\0';
        cmd = str;
     }

   /* parse any message paramaters (max 15) */
   while ((*ptr) && (index < 15))
     {
        if (*ptr == ':')
          {
             params[index++] = (ptr + 1);
             break;
          }
        for (str = ptr; 
             ((*ptr) && (*ptr != ' ') && (*ptr != '\r') && (*ptr != '\n')); 
             ptr++)
          ;
        params[index++] = str;
        if (!*ptr) break;
        *ptr++ = '\0';
     }

   /* deal with ping command */
   if ((cmd) && (!strncmp(cmd, "PING", end - cmd)) && (params[0]))
     {
        char buff[512];
        int len = 0;

        len = snprintf(buff, sizeof(buff), "PONG %s", params[0]);
        express_network_data_send(net, buff, len);
        return;
     }

   switch (code)
     {
        /* skip parts of the motd */
        /* if ((code == 4) || (code == 5)) return; */
        /* if ((code >= 252) && (code < 255)) return; */
        /* if ((code == 265) || (code == 266)) return; */

        //fprintf(stderr, "Code: %d\n", code);

        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 64:
        case 154:
        case 250:
        case 251:
        case 252:
        case 253:
        case 254:
        case 255:
        case 256:
        case 257:
        case 258:
        case 259:
        case 265:
        case 266:
        case 302:
        case 311:
        case 312:
        case 314:
        case 318:
        case 330:
        case 351:
        case 369:
        case 372:
        case 375:
        case 376:
        case 401:
        case 402:
        case 406:
        case 418:
        case 422:
        case 461:
        case 462:
          {
             if (net->callbacks.motd)
               (*net->callbacks.motd)(net, "MOTD", prefix, params, index, 
                                      net->callbacks.data);
             if (code == 5)
               {
                  int x = 1;

                  /* try to get the network type */
                  for (; x < index; x++)
                    {
                       if (!strncmp(params[x], "NETWORK=", 8))
                         {
                            if (!strcasecmp(params[x] + 8, "freenode"))
                              net->type = EXPRESS_NETWORK_TYPE_FREENODE;
                            else if (!strcasecmp(params[x] + 8, "RusNet"))
                              net->type = EXPRESS_NETWORK_TYPE_RUSNET;
                            else if (!strcasecmp(params[x] + 8, "UniBG"))
                              net->type = EXPRESS_NETWORK_TYPE_UNIBG;
                            else if (!strcasecmp(params[x] + 8, "QuakeNet"))
                              net->type = EXPRESS_NETWORK_TYPE_QUAKENET;
                            else if (!strcasecmp(params[x] + 8, "DALnet"))
                              net->type = EXPRESS_NETWORK_TYPE_DALNET;
                            else if (!strcasecmp(params[x] + 8, "BRASnet"))
                              net->type = EXPRESS_NETWORK_TYPE_DALNET;
                         }
                       else if (!strncmp(params[x], "NICKLEN=", 8))
                         net->nicklen = atoi(params[x] + 8);
                       else if (!strncmp(params[x], "CHANNELLEN=", 11))
                         net->channellen = atoi(params[x] + 11);
                       else if (!strncmp(params[x], "TOPICLEN=", 9))
                         net->topiclen = atoi(params[x] + 9);
                    }
               }

             /* wait until End of MOTD before identifying */
             if (code == 376) express_network_nick_password_send(net);
          }
          break;
        case 305:
        case 306:
          {
             if (net->callbacks.command)
               (*net->callbacks.command)(net, "AWAY", prefix, params, index,
                                       net->callbacks.data);
          }
          break;
        case 403:
        case 704:
        case 705:
        case 706:
          {
             if (net->callbacks.command)
               (*net->callbacks.command)(net, "HELP", prefix, params, index,
                                       net->callbacks.data);
          }
          break;
        case 332:
          {
             if (net->callbacks.topic)
               (*net->callbacks.topic)(net, "TOPIC", prefix, params, index, 
                                       net->callbacks.data);
          }
          break;
        case 333:
          {
             if (net->callbacks.topic_time)
               (*net->callbacks.topic_time)(net, "TOPIC_TIME", prefix, params, 
                                            index, net->callbacks.data);
          }
          break;
        case 353:
          {
             if (net->callbacks.channel_names)
               (*net->callbacks.channel_names)(net, "NAMES", prefix, params, 
                                               index, net->callbacks.data);
          }
          break;
        default:
          if (*net->callbacks.numeric)
          (*net->callbacks.numeric)(net, code, prefix, params, index, 
                                    net->callbacks.data);
        case 0:
          break;
     }

   if (!code)
     {
        /* DBG("Command: %s", cmd); */

        /* handle various commands */
        if (!strncmp(cmd, "ERROR", end - cmd))
          {
             if (net->callbacks.error)
               (*net->callbacks.error)(net, cmd, prefix, params, index,
                                       net->callbacks.data);
          }
        else if (!strncmp(cmd, "NICK", end - cmd))
          {
             char nick[256];

             express_network_nick_strip(prefix, nick, sizeof(nick));
             if ((!strncmp(nick, net->nick, strlen(net->nick))) && 
                 (index > 0))
               {
                  eina_stringshare_replace(&net->nick, params[0]);
               }
             if (net->callbacks.nick)
               (*net->callbacks.nick)(net, cmd, prefix, params, index, 
                                      net->callbacks.data);
          }
        else if (!strncmp(cmd, "QUIT", end - cmd))
          {
             if (net->callbacks.quit)
               (*net->callbacks.quit)(net, cmd, prefix, params, index, 
                                      net->callbacks.data);
          }
        else if (!strncmp(cmd, "JOIN", end - cmd))
          {
             if (net->callbacks.join)
               (*net->callbacks.join)(net, cmd, prefix, params, index, 
                                      net->callbacks.data);
          }
        else if (!strncmp(cmd, "PART", end - cmd))
          {
             if (net->callbacks.part)
               (*net->callbacks.part)(net, cmd, prefix, params, index, 
                                      net->callbacks.data);
          }
        else if (!strncmp(cmd, "MODE", end - cmd))
          {
             if ((index > 0) && 
                 (!strncmp(params[0], net->nick, strlen(net->nick))))
               {
                  params[0] = params[1];
                  index = 1;
                  if (net->callbacks.user_mode)
                    (*net->callbacks.user_mode)(net, cmd, prefix, 
                                                params, index, 
                                                net->callbacks.data);
               }
             else
               {
                  if (net->callbacks.mode)
                    (*net->callbacks.mode)(net, cmd, prefix, params, index, 
                                           net->callbacks.data);
               }
          }
        else if (!strncmp(cmd, "TOPIC", end - cmd))
          {
             if (net->callbacks.topic)
               (*net->callbacks.topic)(net, cmd, prefix, params, index, 
                                       net->callbacks.data);
          }
        else if (!strncmp(cmd, "KICK", end - cmd))
          {
             if (net->callbacks.kick)
               (*net->callbacks.kick)(net, cmd, prefix, params, index, 
                                      net->callbacks.data);
          }
        else if (!strncmp(cmd, "PRIVMSG", end - cmd))
          {
             if (index > 1)
               {
                  size_t len;

                  len = strlen(params[1]);
                  if ((params[1][0] == 0x01) && (params[1][1] == 0x01))
                    {
                       char ctcp[128];

                       len -= 2;
                       if (len > (sizeof(ctcp) - 1)) len = (sizeof(ctcp) - 1);
                       memcpy(ctcp, params[1] + 1, len);
                       ctcp[len] = '\0';

                       if (!strncasecmp(ctcp, "DCC", len))
                         {
                            /* TODO: Dcc Request */
                            DBG("\tDeal with dcc request");
                         }
                       else if ((!strncasecmp(ctcp, "ACTION", len)) && 
                                (net->callbacks.ctcp_action))
                         {
                            params[1] = ctcp + 7;
                            index = 2;

                            (*net->callbacks.ctcp_action)
                              (net, "ACTION", prefix, params, index, 
                                  net->callbacks.data);
                         }
                       else if (net->callbacks.ctcp_request)
                         {
                            params[0] = ctcp;
                            index = 1;

                            (*net->callbacks.ctcp_request)
                              (net, "CTCP", prefix, params, index, 
                                  net->callbacks.data);
                         }
                    }
                  else if (!strncasecmp(params[0], 
                                        net->nick, strlen(net->nick)))
                    {
                       if (!strncasecmp(params[1], "VERSION", len))
                         {
                            /* TODO: Implement version support */
                         }
                       else
                         {
                            if (net->callbacks.priv_msg)
                              (*net->callbacks.priv_msg)(net, "PRIVMSG",
                                                         prefix, params,
                                                         index,
                                                         net->callbacks.data);
                         }
                    }
                  else
                    {
                       if (net->callbacks.channel_msg)
                         (*net->callbacks.channel_msg)(net, "CHANNELMSG", 
                                                       prefix, params, index, 
                                                       net->callbacks.data);
                    }
               }
          }
        else if (!strncmp(cmd, "NOTICE", end - cmd))
          {
             size_t len;

             len = strlen(params[1]);
             if ((index > 1) && 
                 (params[1][0] == 0x01) && (params[1][len - 1] == 0x01))
               {
                  char ctcp[512];

                  len -= 2;
                  if (len > (sizeof(ctcp) - 1)) len = (sizeof(ctcp) - 1);

                  DBG("\tCTCP Message");

                  memcpy(ctcp, params[1] + 1, len);
                  ctcp[len] = '\0';
                  params[0] = ctcp;
                  index = 1;

                  if (net->callbacks.ctcp_reply)
                    (*net->callbacks.ctcp_reply)(net, "CTCP", prefix, params, 
                                                 index, net->callbacks.data);
               }
             else if ((net->nick) && 
                      (!strncasecmp(params[0], net->nick, strlen(net->nick))))
               {
                  if (net->callbacks.notice)
                    (*net->callbacks.notice)(net, cmd, prefix, params, index, 
                                             net->callbacks.data);
               }
             else
               {
                  if (net->callbacks.channel_notice)
                    (*net->callbacks.channel_notice)(net, cmd, prefix, params, 
                                                     index, 
                                                     net->callbacks.data);
               }
          }
        else if (!strncmp(cmd, "INVITE", end - cmd))
          {
             if (net->callbacks.invite)
               (*net->callbacks.invite)(net, cmd, prefix, params, index, 
                                        net->callbacks.data);
          }
        else if (!strncmp(cmd, "KILL", end - cmd))
          {
             /* ignored */
             DBG("\tKill Message");
          }
        else
          {
             if (net->callbacks.unknown)
               (*net->callbacks.unknown)(net, cmd, prefix, params, index, 
                                         net->callbacks.data);
          }
     }
}

void 
_express_network_data_process(Express_Network *net, void *data, int length)
{
   int crlf = 0, lf = 0, len = 0;

   if (!eina_binbuf_append_length(net->buff, data, length)) return;
   len = eina_binbuf_length_get(net->buff);

   while (len > 0)
     {
        const unsigned char *str;

        str = eina_binbuf_string_get(net->buff);

        /* search for crlf */
        crlf = _find_crlf(str, len, &lf);
        if (crlf > 0)
          {
             char buff[crlf + lf + 1];

             /* copy string into a buffer we can manipulate */
             memcpy(buff, str, crlf + lf);
             buff[crlf + lf] = '\0';

             /* process this string */
             _process_buffer(net, buff, crlf + lf);

             /* chop off the stuff we processed */
             eina_binbuf_remove(net->buff, 0, crlf + lf);
          }
        else
          break;

        len = eina_binbuf_length_get(net->buff);
     }
}

EXAPI Express_Network *
express_network_find(const char *name)
{
   if (!name) return NULL;
   return eina_hash_find(_networks, name);
}

EXAPI Express_Network *
express_network_create(Express_Callbacks *callbacks, const char *name)
{
   Express_Network *net;

   if (!name) return NULL;

   /* try to allocate space for network structure */
   if (!(net = malloc(sizeof(Express_Network)))) return NULL;

   memset(net, 0, sizeof(Express_Network));

   /* set callbacks */
   memcpy(&net->callbacks, callbacks, sizeof(Express_Callbacks));

   net->connecting = EINA_FALSE;
   net->connected = EINA_FALSE;

   net->name = eina_stringshare_add(name);

   /* set some defaults */
   net->type = EXPRESS_NETWORK_TYPE_UNKNOWN;
   net->nicklen = 16;
   net->channellen = 50;
   net->topiclen = 390;

   net->buff = eina_binbuf_new();

   /* create server hash */
   net->servers = eina_hash_string_small_new(_cb_server_free);

   /* if (!net->callbacks.ctcp_request) */
   /*   { */
   /*      DBG("TODO: Setup default ctcp request callback"); */
   /*   } */

   /* add this network to the hash */
   eina_hash_direct_add(_networks, net->name, net);

   return net;
}

EXAPI void 
express_network_destroy(Express_Network *net)
{
   if (!net) return;
   eina_hash_del_by_key(_networks, net->name);
}

EXAPI void 
express_network_name_set(Express_Network *net, const char *name)
{
   if (!net) return;
   eina_stringshare_replace(&net->name, name);
}

EXAPI const char *
express_network_name_get(Express_Network *net)
{
   if (!net) return NULL;
   return net->name;
}

EXAPI void 
express_network_username_set(Express_Network *net, const char *name)
{
   if (!net) return;
   eina_stringshare_replace(&net->user, name);
}

EXAPI const char *
express_network_username_get(Express_Network *net)
{
   if (!net) return NULL;
   return net->user;
}

EXAPI void 
express_network_username_send(Express_Network *net)
{
   char data[512], host[64];
   int len = 0;

   if ((!net) || (!net->conn)) return;

   if (gethostname(host, sizeof(host)) < 0)
     strcpy(host, "unknown");

   // FIXME the net->nick should really be a real name but we don't save that
   len = snprintf(data, sizeof(data), "USER %s %s %s :%s\r\n", 
                  net->user ? net->user : "nobody", host, host,
                  net->nick ? net->nick : "express_user");
   express_network_data_send(net, data, len);
}

EXAPI void 
express_network_nickname_set(Express_Network *net, const char *name)
{
   if (!net) return;
   eina_stringshare_replace(&net->nick, name);
}

EXAPI const char *
express_network_nickname_get(Express_Network *net)
{
   if (!net) return NULL;
   return net->nick;
}

EXAPI int 
express_network_nickname_length_get(Express_Network *net)
{
   if (!net) return -1;
   return net->nicklen;
}

EXAPI void 
express_network_nickname_send(Express_Network *net)
{
   char data[512];
   int len = 0;

   if ((!net) || (!net->conn) || (!net->nick)) return;

   len = snprintf(data, sizeof(data), "NICK %s\r\n", net->nick);
   express_network_data_send(net, data, len);
}

EXAPI void 
express_network_nick_password_set(Express_Network *net, const char *passwd)
{
   if (!net) return;
   eina_stringshare_replace(&net->nick_pass, passwd);
}

EXAPI void 
express_network_nick_password_send(Express_Network *net)
{
   char data[512];
   int len = 0;

   if ((!net) || (!net->conn) || (!net->nick_pass)) return;

   switch (net->type)
     {
      case EXPRESS_NETWORK_TYPE_DALNET:
      case EXPRESS_NETWORK_TYPE_RUSNET:
        len = snprintf(data, sizeof(data), 
                       "NICKSERV IDENTIFY %s\r\n", net->nick_pass);
        break;
      case EXPRESS_NETWORK_TYPE_UNIBG:
        len = snprintf(data, sizeof(data), 
                       "PRIVMSG NS IDENTIFY %s\r\n", net->nick_pass);
        break;
      case EXPRESS_NETWORK_TYPE_QUAKENET:
        len = snprintf(data, sizeof(data), 
                       "AUTH IDENTIFY %s\r\n", net->nick_pass);
        break;
      case EXPRESS_NETWORK_TYPE_FREENODE:
        len = snprintf(data, sizeof(data), "NICKSERV IDENTIFY %s %s\r\n", 
                       net->nick, net->nick_pass);
        break;
      case EXPRESS_NETWORK_TYPE_UNKNOWN:
      default:
        len = snprintf(data, sizeof(data), 
                       "PRIVMSG NICKSERV IDENTIFY %s\r\n", net->nick_pass);
        break;
     }

   express_network_data_send(net, data, len);
}

EXAPI void 
express_network_nick_strip(const char *buff, char *nick, size_t size)
{
   unsigned int len;
   char *p;

   if (!buff) return;
   p = strstr(buff, "!");

   if (p) len = (p - buff);
   else len = strlen(buff);
   if (len > (size - 1)) len = (size - 1);
   memcpy(nick, buff, len);
   nick[len] = '\0';
}

EXAPI void 
express_network_server_password_set(Express_Network *net, const char *passwd)
{
   if (!net) return;
   eina_stringshare_replace(&net->server_pass, passwd);
}

EXAPI void 
express_network_server_password_send(Express_Network *net)
{
   char data[512];
   int len = 0;

   if ((!net) || (!net->conn) || (!net->server_pass)) return;

   len = snprintf(data, sizeof(data), "PASS %s\r\n", net->server_pass);
   express_network_data_send(net, data, len);
}

EXAPI void 
express_network_autoconnect_set(Express_Network *net, Eina_Bool autoconnect)
{
   if (!net) return;
   net->autoconnect = autoconnect;
}

EXAPI Eina_Bool 
express_network_autoconnect_get(Express_Network *net)
{
   if (!net) return EINA_FALSE;
   return net->autoconnect;
}

EXAPI void 
express_network_bypass_proxy_set(Express_Network *net, Eina_Bool bypass)
{
   if (!net) return;
   net->bypass_proxy = bypass;
}

EXAPI Eina_Bool 
express_network_bypass_proxy_get(Express_Network *net)
{
   if (!net) return EINA_FALSE;
   return net->bypass_proxy;
}

EXAPI void 
express_network_use_ssl_set(Express_Network *net, Eina_Bool use_ssl)
{
   if (!net) return;
   net->use_ssl = use_ssl;
}

EXAPI Eina_Bool 
express_network_use_ssl_get(Express_Network *net)
{
   if (!net) return EINA_FALSE;
   return net->use_ssl;
}

EXAPI Eina_Bool 
express_network_connecting_get(Express_Network *net)
{
   if (!net) return EINA_FALSE;
   return net->connecting;
}

EXAPI Eina_Bool 
express_network_connected_get(Express_Network *net)
{
   if (!net) return EINA_FALSE;
   return net->connected;
}

EXAPI void 
express_network_connect(Express_Network *net)
{
   Eina_Iterator *iter;
   Express_Server *srv;
   int flags = 0;

   if (!net) return;

   /* if we are already connected, get out */
   if ((net->conn) && (ecore_con_server_connected_get(net->conn))) return;

   /* set connection flags */
   flags = ECORE_CON_REMOTE_TCP;// | ECORE_CON_REMOTE_NODELAY;
   if (net->use_ssl) 
     flags |= (ECORE_CON_USE_MIXED | ECORE_CON_LOAD_CERT);
   if (net->bypass_proxy) flags |= ECORE_CON_NO_PROXY;

   net->connecting = EINA_TRUE;
   net->connected = EINA_FALSE;

   iter = eina_hash_iterator_data_new(net->servers);
   EINA_ITERATOR_FOREACH(iter, srv)
     {
        if (srv->skip) continue;

        if (!net->conn)
          {
             net->conn = 
               ecore_con_server_connect(flags, srv->name, srv->port, net);
          }

        if (net->conn)
          {
             if (net->use_ssl)
               ecore_con_ssl_server_verify_basic(net->conn);

             express_network_server_password_send(net);
             express_network_username_send(net);
             express_network_nickname_send(net);

             break;
          }
     }
   eina_iterator_free(iter);
}

EXAPI void 
express_network_disconnect(Express_Network *net)
{
   if (!net) return;
   if (ecore_con_server_connected_get(net->conn))
     {
        ecore_con_server_del(net->conn);
        net->conn = NULL;
     }
   net->connected = EINA_FALSE;
   net->connecting = EINA_FALSE;
}

EXAPI Express_Server *
express_network_server_find(Express_Network *net, const char *server_name)
{
   if ((!net) || (!server_name)) return NULL;
   return eina_hash_find(net->servers, server_name);
}

EXAPI const char *
express_network_server_name_get(Express_Server *srv)
{
   return srv->name;
}

EXAPI const char *
express_network_server_realname_get(Express_Server *srv)
{
   return srv->realname;
}

EXAPI Express_Server *
express_network_server_connected_get(Express_Network *net)
{
   Eina_Iterator *iter;
   Express_Server *srv = NULL;
   Eina_Bool found = EINA_FALSE;

   iter = eina_hash_iterator_data_new(net->servers);
   EINA_ITERATOR_FOREACH(iter, srv)
     {
        if (srv->connected)
          {
             found = EINA_TRUE;
             break;
          }
     }
   eina_iterator_free(iter);

   if (found) return srv;
   return NULL;
}

EXAPI Express_Server *
express_network_server_add(Express_Network *net, const char *server_name, int port)
{
   Express_Server *srv;

   if ((!net) || (!server_name)) return NULL;

   /* try to allocate space for server structure */
   if (!(srv = calloc(1, sizeof(Express_Server)))) return NULL;

   srv->name = eina_stringshare_add(server_name);
   srv->port = port;
   srv->skip = EINA_FALSE;

   /* add this server to the hash */
   eina_hash_direct_add(net->servers, srv->name, srv);

   return srv;
}

EXAPI void 
express_network_server_del(Express_Network *net, const char *server_name)
{
   if (!net) return;
   eina_hash_del_by_key(net->servers, server_name);
}

EXAPI void 
express_network_data_send(Express_Network *net, const char *data, int len)
{
   if ((!net) || (!net->conn) || (!data)) return;
   ecore_con_server_send(net->conn, data, len);
   ecore_con_server_flush(net->conn);
}

EXAPI Eina_Bool 
express_network_channel_join(Express_Network *net, const char *channel, const char *pass)
{
   char data[512];
   int len = 0;

   if ((!net) || (!net->conn) || (!channel)) return EINA_FALSE;

   if (pass)
     len = snprintf(data, sizeof(data), "JOIN %s :%s\r\n", channel, pass);
   else
     len = snprintf(data, sizeof(data), "JOIN %s\r\n", channel);

   express_network_data_send(net, data, len);

   return EINA_TRUE;
}

EAPI void 
express_network_channel_part(Express_Network *net, const char *channel)
{
   char data[512];
   int len = 0;

   if ((!net) || (!net->conn) || (!channel)) return;

   len = snprintf(data, sizeof(data), "PART %s\r\n", channel);
   express_network_data_send(net, data, len);
}

EXAPI void 
express_network_channel_priv_send(Express_Network *net, const char *channel, const char *msg)
{
   char data[512];
   int len = 0;

   if ((!net) || (!net->conn) || (!channel)) return;

   len = snprintf(data, sizeof(data), "PRIVMSG %s :%s\r\n", channel, msg);
   express_network_data_send(net, data, len);
}

EXAPI void 
express_network_command_send(Express_Network *net, const char *cmd)
{
   if ((!net) || (!cmd)) return;
   express_network_data_send(net, cmd, strlen(cmd));
}
