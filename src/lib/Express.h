#ifndef EXPRESS_H
# define EXPRESS_H

/* EFL Headers */
# include <Eina.h>

# ifdef EXAPI
#  undef EXAPI
# endif

# ifdef __GNUC__
#  if __GNUC__ >= 4
#   define EXAPI __attribute__ ((visibility("default")))
#  else
#   define EXAPI
#  endif
# else
#  define EXAPI
# endif

/* # ifdef __cplusplus */
/* extern "C" { */
/* # endif */

# define EXPRESS_BUFF_SIZE 2050

typedef struct _Express_Network Express_Network;
typedef struct _Express_Server Express_Server;
typedef struct _Express_Callbacks Express_Callbacks;

typedef void (*Express_Event_Callback)(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
typedef void (*Express_Event_Numeric_Callback)(Express_Network *net, unsigned int event, const char *source, const char **params, unsigned int count, void *data);

/* NB: See http://irchelp.org/irchelp/rfc/chapter4.html for parameters */
typedef struct _Express_Callbacks
{
   /* params[1]: Status message. */
   Express_Event_Callback command;

   Express_Event_Callback error;

   /* source: server which was connected
    * params: none */
   Express_Event_Callback connect;

   Express_Event_Callback motd;

   /* source: person who changed nick
    * params[0]: new nick */
   Express_Event_Callback nick;

   /* source: who quit
    * params[0]: (optional) reason */
   Express_Event_Callback quit;

   /* source: person who joined
    * params[0]: channel they joined */
   Express_Event_Callback join;

   /* source: person who left
    * params[0]: (optional) reason */
   Express_Event_Callback part;

   /* source: person who changed channel mode
    * params[0]: channel name
    * params[1]: changed mode
    * params[2]: (optional) mode argument or user who got ops */
   Express_Event_Callback mode;

   /* source: person who changed user mode
    * params[0]: the changed mode */
   Express_Event_Callback user_mode;

   /* source: the person who changed the topic
    * params[0]: channel name
    * params[1]: new topic (optional) */
   Express_Event_Callback topic;

   /* source: the person who changed the topic
    * params[0]: channel name
    * params[1]: nickname
    * params[2]: time */
   Express_Event_Callback topic_time;

   /* source: the person who kicked your arse
    * params[0]: channel name
    * params[1]: you nick (optional)
    * params[2]: kick reason */
   Express_Event_Callback kick;

   /* source: person who sent the message
    * params[0]: channel name
    * params[1]: message (optional) */
   Express_Event_Callback channel_msg;

   /* source: person who sent the message
    * params[0]: your nick
    * params[1]: message (optional) */
   Express_Event_Callback priv_msg;

   /* source: who generated the message
    * params[0]: target nick name
    * params[1]: message (optional) */
   Express_Event_Callback notice;

   /* source: who generated the message
    * params[0]: channel name
    * params[1]: message (optional) */
   Express_Event_Callback channel_notice;

   /* source: who generated the message
    * params[0]: channel name
    * params[1]: nicknames of users on the channel */
   Express_Event_Callback channel_names;

   /* source: who did the inviting
    * params[0]: your nick
    * params[1]: channel name you were invited to */
   Express_Event_Callback invite;

   /* source: who generated the message
    * params[0]: ctcp message */
   Express_Event_Callback ctcp_request;

   /* source: who generated the message
    * params[0]: ctcp message */
   Express_Event_Callback ctcp_reply;

   /* source: who generated the message
    * params[0]: ctcp message */
   Express_Event_Callback ctcp_action;

   /* TODO */
   Express_Event_Callback unknown;

   Express_Event_Numeric_Callback numeric;

   Express_Event_Callback dcc_chat_request;
   Express_Event_Callback dcc_send_request;

   void *data;
} Express_Callbacks;

EXAPI int express_init(void);
EXAPI int express_shutdown(void);

/* Public network functions */
EXAPI Express_Network *express_network_find(const char *name);
EXAPI Express_Network *express_network_create(Express_Callbacks *callbacks, const char *name);
EXAPI void express_network_destroy(Express_Network *net);
EXAPI void express_network_name_set(Express_Network *net, const char *name);
EXAPI const char *express_network_name_get(Express_Network *net);
EXAPI void express_network_username_set(Express_Network *net, const char *name);
EXAPI const char *express_network_username_get(Express_Network *net);
EXAPI void express_network_username_send(Express_Network *net);
EXAPI void express_network_nickname_set(Express_Network *net, const char *name);
EXAPI const char *express_network_nickname_get(Express_Network *net);
EXAPI int express_network_nickname_length_get(Express_Network *net);
EXAPI void express_network_nickname_send(Express_Network *net);
EXAPI void express_network_nick_password_set(Express_Network *net, const char *passwd);
EXAPI void express_network_nick_password_send(Express_Network *net);
EXAPI void express_network_nick_strip(const char *buff, char *nick, size_t size);
EXAPI void express_network_server_password_set(Express_Network *net, const char *passwd);
EXAPI void express_network_server_password_send(Express_Network *net);
EXAPI void express_network_autoconnect_set(Express_Network *net, Eina_Bool autoconnect);
EXAPI Eina_Bool express_network_autoconnect_get(Express_Network *net);
EXAPI void express_network_bypass_proxy_set(Express_Network *net, Eina_Bool bypass);
EXAPI Eina_Bool express_network_bypass_proxy_get(Express_Network *net);
EXAPI void express_network_use_ssl_set(Express_Network *net, Eina_Bool use_ssl);
EXAPI Eina_Bool express_network_use_ssl_get(Express_Network *net);
EXAPI Eina_Bool express_network_connecting_get(Express_Network *net);
EXAPI Eina_Bool express_network_connected_get(Express_Network *net);
EXAPI void express_network_connect(Express_Network *net);
EXAPI void express_network_disconnect(Express_Network *net);
EXAPI Express_Server *express_network_server_find(Express_Network *net, const char *server_name);
EXAPI Express_Server *express_network_server_add(Express_Network *net, const char *server_name, int port);
EXAPI void express_network_server_del(Express_Network *net, const char *server_name);
EXAPI Express_Server *express_network_server_connected_get(Express_Network *net);
EXAPI void express_network_data_send(Express_Network *net, const char *data, int len);
EXAPI const char *express_network_server_name_get(Express_Server *srv);
EXAPI const char *express_network_server_realname_get(Express_Server *srv);
EXAPI Eina_Bool express_network_channel_join(Express_Network *net, const char *channel, const char *pass);
EXAPI void express_network_channel_part(Express_Network *net, const char *channel);
EXAPI void express_network_channel_priv_send(Express_Network *net, const char *channel, const char *msg);
EXAPI void express_network_command_send(Express_Network *net, const char *cmd);

/* # ifdef __cplusplus */
/* } */
/* # endif */

#endif
