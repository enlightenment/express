#ifndef _PRIVATE_H
# define _PRIVATE_H

# ifdef HAVE_CONFIG_H
#  include "express_config.h"
# endif

/* standard headers */
# include <ctype.h>
# include <unistd.h>
/* #include <stdio.h> */
/* #include <stdlib.h> */

# ifdef _WIN32
#  include <winsock2.h>
# endif

/* EFL Headers */
# include <Eina.h>
# include <Ecore.h>
# include <Ecore_Con.h>

# include "Express.h"

/* logging */
extern int _exp_log_dom;

# ifdef EXP_DEFAULT_LOG_COLOR
#  undef EXP_DEFAULT_LOG_COLOR
# endif
# define EXP_DEFAULT_LOG_COLOR EINA_COLOR_BLUE

# ifdef ERR
#  undef ERR
# endif
# define ERR(...) EINA_LOG_DOM_ERR(_exp_log_dom, __VA_ARGS__)

# ifdef DBG
#  undef DBG
# endif
# define DBG(...) EINA_LOG_DOM_DBG(_exp_log_dom, __VA_ARGS__)

# ifdef INF
#  undef INF
# endif
# define INF(...) EINA_LOG_DOM_INFO(_exp_log_dom, __VA_ARGS__)

# ifdef WRN
#  undef WRN
# endif
# define WRN(...) EINA_LOG_DOM_WARN(_exp_log_dom, __VA_ARGS__)

# ifdef CRI
#  undef CRI
# endif
# define CRI(...) EINA_LOG_DOM_CRIT(_exp_log_dom, __VA_ARGS__)

enum _Express_Network_Type
{
   EXPRESS_NETWORK_TYPE_UNKNOWN = 0,
   EXPRESS_NETWORK_TYPE_FREENODE,
   EXPRESS_NETWORK_TYPE_DALNET,
   EXPRESS_NETWORK_TYPE_RUSNET,
   EXPRESS_NETWORK_TYPE_UNIBG,
   EXPRESS_NETWORK_TYPE_QUAKENET
};

struct _Express_Network
{
   const char *name, *user;
   const char *nick, *nick_pass;
   const char *server_pass;

   int type;
   int nicklen, channellen, topiclen;

   Eina_Bool autoconnect : 1;
   Eina_Bool bypass_proxy : 1;
   Eina_Bool use_ssl : 1;

   Express_Callbacks callbacks;

   Eina_Hash *servers;

   Eina_Bool connecting : 1;
   Eina_Bool connected : 1;

   Ecore_Con_Server *conn;

   Eina_Binbuf *buff;
};

struct _Express_Server
{
   const char *name;
   const char *realname;
   int port;

   Eina_Bool skip : 1;
   Eina_Bool connected : 1;
};

extern Eina_Hash *_networks;

void _express_network_data_send(Express_Network *net, const char *data, int len);
void _express_network_data_process(Express_Network *net, void *data, int offset);

#endif
