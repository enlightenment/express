#include "private.h"
#include "options_channels.h"
#include "config.h"

/* local variables */
static Evas_Object *o_list = NULL;
static Evas_Object *o_del = NULL;
static Evas_Object *o_name = NULL, *o_pass = NULL;
static Config_Network *_cfg_net = NULL;

static void 
_cb_name_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   Elm_Object_Item *item;
   Config_Channel *cfg_chl = NULL;
   const char *txt;

   if (!(item = elm_list_selected_item_get(o_list))) return;
   if (!(cfg_chl = elm_object_item_data_get(item))) return;

   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_chl->name, txt);
   _config_save();

   elm_object_item_part_text_set(item, "elm.text", txt);
   elm_object_focus_set(obj, EINA_TRUE);
}

static void 
_cb_pass_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   Elm_Object_Item *item;
   Config_Channel *cfg_chl = NULL;
   const char *txt;

   if (!(item = elm_list_selected_item_get(o_list))) return;
   if (!(cfg_chl = elm_object_item_data_get(item))) return;

   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_chl->pass, txt);
   _config_save();
   elm_object_focus_set(obj, EINA_TRUE);
}

static void 
_cb_chl_sel(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Elm_Object_Item *item;
   Config_Channel *cfg_chl = NULL;

   if (!(item = elm_list_selected_item_get(o_list))) return;
   if (!(cfg_chl = elm_object_item_data_get(item))) return;

   elm_object_disabled_set(o_del, EINA_FALSE);

   elm_entry_entry_set(o_name, cfg_chl->name);

   elm_object_disabled_set(o_name, EINA_FALSE);
   elm_object_disabled_set(o_pass, EINA_FALSE);
}

static void
_cb_chl_add(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Config_Channel *cfg_chl;
   Elm_Object_Item *item;
   Evas_Object *icon;

   if ((item = elm_list_selected_item_get(o_list)))
     elm_list_item_selected_set(item, EINA_FALSE);

   if (!(cfg_chl = malloc(sizeof(Config_Channel)))) return;

   cfg_chl->name = eina_stringshare_add("#new");
   cfg_chl->pass = NULL;

   _cfg_net->channels = eina_list_append(_cfg_net->channels, cfg_chl);
   _config_save();

   icon = elm_icon_add(o_list);
   elm_icon_standard_set(icon, "network-wireless");
   evas_object_size_hint_aspect_set(icon,
                                    EVAS_ASPECT_CONTROL_VERTICAL, 1, 1);
   item = elm_list_item_append(o_list, cfg_chl->name, icon, NULL, 
                               _cb_chl_sel, cfg_chl);
   elm_list_item_selected_set(item, EINA_TRUE);
   elm_list_go(o_list);

   elm_entry_entry_set(o_name, cfg_chl->name);
   elm_entry_entry_set(o_pass, cfg_chl->pass);

   elm_object_disabled_set(o_name, EINA_FALSE);
   elm_object_disabled_set(o_pass, EINA_FALSE);

   elm_object_focus_set(o_name, EINA_TRUE);
   elm_entry_cursor_end_set(o_name);
   elm_entry_select_all(o_name);
}

static void
_cb_chl_del(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Elm_Object_Item *item;

   if ((item = elm_list_selected_item_get(o_list)))
     {
        Config_Channel *cfg_chl;

        if ((cfg_chl = elm_object_item_data_get(item)))
          {
             _cfg_net->channels = 
               eina_list_remove(_cfg_net->channels, cfg_chl);

             if (cfg_chl->name) eina_stringshare_del(cfg_chl->name);
             if (cfg_chl->pass) eina_stringshare_del(cfg_chl->pass);

             free(cfg_chl);

             _config_save();
          }

        elm_object_item_del(item);

        elm_object_disabled_set(o_del, EINA_TRUE);

        elm_entry_entry_set(o_name, NULL);

        elm_object_disabled_set(o_name, EINA_TRUE);
        elm_object_disabled_set(o_pass, EINA_TRUE);
     }
}

void 
_options_channels(Evas_Object *box, Evas_Object *base EINA_UNUSED, Config_Network *cfg_net)
{
   Evas_Object *fr, *vbox, *tb, *o;
   Eina_List *l = NULL;
   Config_Channel *cfg_chl;

   _cfg_net = cfg_net;

   fr = elm_frame_add(box);
   elm_object_text_set(fr, "Channels");
   WEIGHT_SET(fr, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(fr, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(box, fr);
   evas_object_show(fr);

   vbox = elm_box_add(box);
   elm_box_horizontal_set(vbox, EINA_FALSE);
   elm_box_align_set(vbox, 0.0, 0.0);
   WEIGHT_SET(vbox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(vbox, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(fr, vbox);
   evas_object_show(vbox);

   tb = elm_table_add(box);
   elm_table_homogeneous_set(tb, EINA_FALSE);
   elm_table_padding_set(tb, 4, 1);
   WEIGHT_SET(tb, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(tb, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, tb);
   evas_object_show(tb);

   /* servers list */
   o_list = elm_list_add(box);
   WEIGHT_SET(o_list, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_list, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_table_pack(tb, o_list, 0, 0, 1, 3);
   evas_object_show(o_list);

   EINA_LIST_FOREACH(cfg_net->channels, l, cfg_chl)
     {
        Evas_Object *icon;

        icon = elm_icon_add(o_list);
        elm_icon_standard_set(icon, "network-wireless");
        evas_object_size_hint_aspect_set(icon,
                                         EVAS_ASPECT_CONTROL_VERTICAL, 1, 1);
        elm_list_item_append(o_list, cfg_chl->name, icon, NULL, 
                             _cb_chl_sel, cfg_chl);
     }
   elm_list_go(o_list);

   o = elm_button_add(box);
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "clicked", _cb_chl_add, NULL);
   elm_object_text_set(o, "Add");
   elm_table_pack(tb, o, 1, 0, 1, 1);
   evas_object_show(o);

   o_del = elm_button_add(box);
   WEIGHT_SET(o_del, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o_del, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o_del, "clicked", _cb_chl_del, NULL);
   elm_object_text_set(o_del, "Remove");
   elm_object_disabled_set(o_del, EINA_TRUE);
   elm_table_pack(tb, o_del, 1, 1, 1, 1);
   evas_object_show(o_del);

   o = elm_separator_add(box);
   elm_separator_horizontal_set(o, EINA_TRUE);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   tb = elm_table_add(box);
   elm_table_homogeneous_set(tb, EINA_FALSE);
   elm_table_padding_set(tb, 4, 1);
   WEIGHT_SET(tb, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(tb, EVAS_HINT_FILL, 0.0);
   elm_box_pack_end(vbox, tb);
   evas_object_show(tb);

   o = elm_label_add(box);
   elm_object_text_set(o, "Name");
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_table_pack(tb, o, 0, 0, 1, 1);
   evas_object_show(o);

   o_name = elm_entry_add(box);
   elm_entry_single_line_set(o_name, EINA_TRUE);
   elm_entry_scrollable_set(o_name, EINA_TRUE);
   elm_entry_input_panel_layout_set(o_name, ELM_INPUT_PANEL_LAYOUT_PASSWORD);
   elm_scroller_policy_set(o_name, ELM_SCROLLER_POLICY_OFF,
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o_name, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_name, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o_name, "changed,user", 
                                  _cb_name_changed, NULL);
   elm_table_pack(tb, o_name, 1, 0, 2, 1);
   evas_object_show(o_name);

   o = elm_label_add(box);
   elm_object_text_set(o, "Password");
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_table_pack(tb, o, 0, 1, 1, 1);
   evas_object_show(o);

   o_pass = elm_entry_add(box);
   elm_entry_single_line_set(o_pass, EINA_TRUE);
   elm_entry_scrollable_set(o_pass, EINA_TRUE);
   elm_entry_input_panel_layout_set(o_pass, ELM_INPUT_PANEL_LAYOUT_PASSWORD);
   elm_scroller_policy_set(o_pass, ELM_SCROLLER_POLICY_OFF,
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o_pass, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_pass, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o_pass, "changed,user", 
                                  _cb_pass_changed, NULL);
   elm_table_pack(tb, o_pass, 1, 1, 2, 1);
   evas_object_show(o_pass);

   elm_object_disabled_set(o_name, EINA_TRUE);
   elm_object_disabled_set(o_pass, EINA_TRUE);
}
