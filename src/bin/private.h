#ifdef HAVE_CONFIG_H
# include "express_config.h"
#endif

#ifdef _WIN32
# define _POSIX
#endif

/* standard headers */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef _WIN32
# include <io.h>
#endif

/* EFL Headers */
#include <Elementary.h>
#include <Efreet.h>
#include <Express.h>

extern int _log_dom;

#ifndef MIN
# define MIN(a,b) ((a) < (b)) ? (a) : (b)
#endif

#ifndef MAX
# define MAX(a,b) ((a) > (b)) ? (a) : (b)
#endif

#ifdef EXPRESS_DEFAULT_LOG_COLOR
# undef EXPRESS_DEFAULT_LOG_COOR
#endif
#define EXPRESS_DEFAULT_LOG_COLOR EINA_COLOR_GREEN

#define CRIT(...) EINA_LOG_DOM_CRIT(_log_dom, __VA_ARGS__)
#define ERR(...) EINA_LOG_DOM_ERR(_log_dom, __VA_ARGS__)
#define WRN(...) EINA_LOG_DOM_WARN(_log_dom, __VA_ARGS__)
#define INF(...) EINA_LOG_DOM_INFO(_log_dom, __VA_ARGS__)
#define DBG(...) EINA_LOG_DOM_DBG(_log_dom, __VA_ARGS__)

#define WEIGHT_SET(OBJ, WIDTH, HEIGHT) \
   evas_object_size_hint_weight_set(OBJ, WIDTH, HEIGHT)

#define FILL_SET(OBJ, WIDTH, HEIGHT) \
   evas_object_size_hint_fill_set(OBJ, WIDTH, HEIGHT)

#define ALIGN_SET(OBJ, WIDTH, HEIGHT) \
   evas_object_size_hint_align_set(OBJ, WIDTH, HEIGHT)

typedef struct _Window Window;
typedef struct _Channel Channel;

#define SUPPORT_ITALIC 1
//#define SUPPORT_DBLWIDTH 1
//#define SUPPORT_80_132_COLUMNS 1


/* Row color is used to pass the color information that
 * goes with the row text around.
 * Row Colors should be created when the row text is
 * created, they will be destroyed once the row has been
 * added to the textgrid.
 */

typedef struct _Row_Color Row_Color;

struct _Row_Color
{
   int fg_primary_color;
   int bg_primary_color;
};

/* Creates a Row_Color struct with the forground color passed in
 * and no overrides, this is enough for most cases */
Row_Color *_row_color_simple_create(int fg_primary_color);
