#include "private.h"
#include "selector.h"
#include "config.h"
#include "theme.h"
#include "channel.h"

typedef struct _Selector Selector;
typedef struct _Entry Entry;

struct _Selector
{
   Evas_Object_Smart_Clipped_Data __clipped_data;
   Evas *evas;

   Evas_Object *o_clip;
   Evas_Object *o_event;

   Eina_List *entries;

   int w, h;
   Evas_Coord px, px0, px1;
   Evas_Coord py, py0, py1;

   double interp;
   double start, total;
   double zoom, zoom0, zoom1;
   double orig_zoom;
   double last_time;

   struct 
     {
        Evas_Coord x, y;
        Eina_Bool down : 1;
     } mouse;

   Ecore_Animator *anim;
   Ecore_Timer *autozoom;

   Eina_Bool select : 1;
   Eina_Bool exit_me : 1;
   Eina_Bool exit_now : 1;
   Eina_Bool exit_on_sel : 1;
   Eina_Bool pending : 1;
   Eina_Bool use_px : 1;
};

struct _Entry
{
   Evas_Object *obj;
   Evas_Object *o_bg;

   Channel *chl;

   Eina_Bool selected : 1;
   Eina_Bool before : 1;
   Eina_Bool orig : 1;
   Eina_Bool was_selected : 1;
};

/* local variables */
static Evas_Smart *_smart = NULL;
static Evas_Smart_Class _parent_sc = EVAS_SMART_CLASS_INIT_NULL;

/* local functions */
static void 
_cb_layout(Evas_Object *obj, Selector *sd)
{
   Entry *en;
   Eina_List *l;
   Evas_Coord ox = 0, oy = 0, ow = 0, oh = 0;
   Evas_Coord w = 0, h = 0, px = 0, py = 0;
   int count = 0, ew = 0, eh = 0, x = 0, y = 0;

   count = eina_list_count(sd->entries);
   if (count < 1) return;

   evas_object_geometry_get(obj, &ox, &oy, &ow, &oh);

   ew = sqrt(count);
   if (ew < 1) ew = 1;
   eh = ((count + (ew - 1)) / ew);
   if (eh < 1) eh = 1;

   w = (ow * sd->zoom);
   h = (oh * sd->zoom);

   if (!sd->use_px)
     {
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if (en->before)
               {
                  sd->px0 = (x * w);
                  sd->py0 = (y * h);
               }
             if ((sd->exit_on_sel) && (!sd->exit_now))
               {
                  if (en->before)
                    {
                       sd->px1 = (x * w);
                       sd->py1 = (y * h);
                    }
               }
             else
               {
                  if (en->selected)
                    {
                       sd->px1 = (x * w);
                       sd->py1 = (y * h);
                    }
               }
             x++;
             if (x >= ew)
               {
                  x = 0;
                  y++;
               }
          }
     }

   sd->px = (sd->px0 + ((sd->px1 - sd->px0) * sd->interp));
   sd->py = (sd->py0 + ((sd->py1 - sd->py0) * sd->interp));

   px = sd->px;
   py = sd->py;

   if ((ew > 0) && (w > 0) && (eh > 0) && (h > 0))
     {
        Evas_Coord ww, hh;

        ww = (ew * w);
        hh = (eh * h);
        if (ww <= w) px = (ww - ow) / 2;
        else px = px - ((px * (ow - w)) / (ww - w));
        if (hh <= h) py = (hh - oh) / 2;
        else py = py - ((py * (oh - h)) / (hh - h));
     }
   x = y = 0;

   EINA_LIST_FOREACH(sd->entries, l, en)
     {
        evas_object_move(en->o_bg, ox + (x * w) - px, oy + (y * h) - py);
        evas_object_resize(en->o_bg, w, h);
        evas_object_show(en->o_bg);
        x++;
        if (x >= ew)
          {
             x = 0;
             y++;
          }
     }

   if ((sd->w > 0) && (sd->h > 0) && (sd->pending))
     {
        sd->pending = EINA_FALSE;
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if ((!en->was_selected) && (en->selected))
               edje_object_signal_emit(en->o_bg, "selected", PACKAGE_NAME);
             else if ((en->was_selected) && (!en->selected))
               edje_object_signal_emit(en->o_bg, "unselected", PACKAGE_NAME);
          }
     }
}

static Eina_Bool 
_cb_anim(void *data)
{
   Evas_Object *obj;
   Selector *sd;
   double t = 1.0;

   obj = data;
   if (!(sd = evas_object_smart_data_get(obj))) return EINA_FALSE;

   if (sd->total > 0.0)
     {
        t = (ecore_loop_time_get() - sd->start) / sd->total;
        if (t < 0.0) t = 0.0;
        else if (t > 1.0) t = 1.0;
     }

   sd->interp = ecore_animator_pos_map(t, ECORE_POS_MAP_DECELERATE, 0, 0);
   sd->zoom = sd->zoom0 + ((sd->zoom1 - sd->zoom0) * sd->interp);
   _cb_layout(obj, sd);

   if (t >= 1.0)
     {
        sd->anim = NULL;
        if ((sd->exit_on_sel) && (!sd->exit_now))
          {
             if (sd->autozoom)
               {
                  ecore_timer_del(sd->autozoom);
                  sd->autozoom = NULL;
               }
             sd->exit_now = EINA_TRUE;
             evas_object_smart_callback_call(obj, "ending", NULL);
             _selector_zoom(obj, 1.0);

             return EINA_FALSE;
          }
        if ((sd->select) || (sd->exit_now))
          {
             Entry *en;
             Eina_List *l;
             Evas_Object *entry = NULL;

             EINA_LIST_FOREACH(sd->entries, l, en)
               {
                  if (en->selected) 
                    {
                       entry = en->obj;
                       evas_object_smart_callback_call(obj, "selected", entry);
                       break;
                    }
               }
          }
        else if (sd->exit_me)
          evas_object_smart_callback_call(obj, "exit", NULL);

        return EINA_FALSE;
     }

   return EINA_TRUE;
}

static void 
_cb_transit(Evas_Object *obj, Selector *sd, double t)
{
   sd->px0 = sd->px;
   sd->py0 = sd->py;
   sd->zoom0 = sd->zoom;
   sd->start = ecore_loop_time_get();
   sd->total = t;
   sd->interp = 0.0;
   if (!sd->anim) sd->anim = ecore_animator_add(_cb_anim, obj);
}

static void 
_cb_entry_del(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Entry *en;

   en = data;
   if (en->obj)
     evas_object_event_callback_del_full(en->obj, EVAS_CALLBACK_DEL, 
                                         _cb_entry_del, en);
   en->obj = NULL;
}

static Eina_Bool 
_cb_autozoom_reset(void *data)
{
   Selector *sd;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(data))) return EINA_FALSE;

   sd->autozoom = NULL;
   _selector_zoom(data, sd->orig_zoom);
   return EINA_FALSE;
}

static void 
_cb_autozoom(Evas_Object *obj, Selector *sd)
{
   double t = 0.0;

   t = ecore_loop_time_get();
   if ((t - sd->last_time) < 0.5)
     _selector_zoom(obj, (sd->zoom * 0.9));
   sd->last_time = t;
   if (sd->autozoom) ecore_timer_del(sd->autozoom);
   sd->autozoom = ecore_timer_add(0.5, _cb_autozoom_reset, obj);
}

static void 
_cb_mouse_up(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Selector *sd;
   Evas_Event_Mouse_Up *ev;
   Evas_Coord dx, dy, size;

   ev = event;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(data))) return;

   if (!sd->mouse.down) return;
   sd->mouse.down = EINA_FALSE;
   dx = abs(ev->canvas.x - sd->mouse.x);
   dy = abs(ev->canvas.y - sd->mouse.y);
   size = elm_config_finger_size_get();

   if ((dx <= size) && (dy <= size))
     {
        Entry *en;
        Eina_List *l;
        Evas_Coord x, y, ox, oy, ow, oh;

        x = ev->canvas.x;
        y = ev->canvas.y;
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             evas_object_geometry_get(en->o_bg, &ox, &oy, &ow, &oh);
             if (ELM_RECTS_INTERSECT(ox, oy, ow, oh, x, y, 1, 1))
               {
                  _selector_channel_selected_set(data, en->chl, EINA_FALSE);
                  sd->select = EINA_TRUE;
                  sd->exit_me = EINA_FALSE;
                  if (sd->autozoom)
                    {
                       ecore_timer_del(sd->autozoom);
                       sd->autozoom = NULL;
                    }
                  evas_object_smart_callback_call(data, "ending", NULL);
                  _selector_zoom(data, 1.0);
                  return;
               }
          }
        evas_object_smart_callback_call(data, "clicked", NULL);
     }
}

static void 
_cb_mouse_down(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Selector *sd;
   Evas_Event_Mouse_Down *ev;

   sd = data;
   ev = event;
   if (sd->mouse.down) return;
   if (ev->button != 1) return;
   sd->mouse.x = ev->canvas.x;
   sd->mouse.y = ev->canvas.y;
   sd->mouse.down = EINA_TRUE;
}

static void 
_cb_mouse_move(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Selector *sd;
   Evas_Event_Mouse_Move *ev;
   Evas_Coord x = 0, y = 0, w = 0, h = 0;
   Evas_Coord sw = 0, sh = 0;
   int ew = 0, eh = 0, count = 0;

   ev = event;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(data))) return;

   if ((sd->exit_me) || (sd->exit_now) || (sd->select) || (sd->anim)) return;

   count = eina_list_count(sd->entries);
   if (count < 1) return;

   ew = sqrt(count);
   if (ew < 1) ew = 1;
   eh = (count + (ew - 1)) / ew;
   if (eh < 1) eh = 1;

   evas_object_geometry_get(data, &x, &y, &w, &h);
   sw = w * sd->zoom;
   sh = h * sd->zoom;
   if (!sd->mouse.down)
     {
        if ((w > 0) && (h > 0))
          {
             _cb_transit(data, sd, 0.5);
             sd->px1 = ((ev->cur.canvas.x - x) * ((ew - 1) * sw)) / w;
             sd->py1 = ((ev->cur.canvas.y - y) * ((eh - 1) * sh)) / h;
             sd->use_px = EINA_TRUE;
          }
     }
}

static void 
_cb_key_down(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Selector *sd;
   Evas_Event_Key_Down *ev;
   Eina_List *l;
   Entry *en;

   ev = event;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(data))) return;

   if ((sd->exit_me) || (sd->exit_now) || (sd->select)) return;

   if ((!strcmp(ev->key, "Next")) || (!strcmp(ev->key, "Right")))
     {
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if ((en->selected) && (l->next))
               {
                  en = l->next->data;
                  _selector_channel_selected_set(obj, en->chl, EINA_FALSE);
                  break;
               }
          }
        sd->exit_now = EINA_FALSE;
        _cb_autozoom(data, sd);
     }
   else if ((!strcmp(ev->key, "Prior")) || (!strcmp(ev->key, "Left")))
     {
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if ((en->selected) && (l->prev))
               {
                  en = l->prev->data;
                  _selector_channel_selected_set(obj, en->chl, EINA_FALSE);
                  break;
               }
          }
        sd->exit_now = EINA_FALSE;
        _cb_autozoom(data, sd);
     }
   else if (!strcmp(ev->key, "Up"))
     {
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if (en->selected)
               {
                  Evas_Coord x = 0, y = 0, w = 0, h = 0;
                  Evas_Coord sx = 0, sy = 0;

                  evas_object_geometry_get(en->o_bg, &x, &y, &w, &h);
                  sx = (x + (w / 2));
                  sy = (y - (h / 2));
                  EINA_LIST_FOREACH(sd->entries, l, en)
                    {
                       evas_object_geometry_get(en->o_bg, &x, &y, &w, &h);
                       if (ELM_RECTS_INTERSECT(x, y, w, h, sx, sy, 1, 1))
                         {
                            _selector_channel_selected_set(obj, en->chl, 
                                                           EINA_FALSE);
                            break;
                         }
                    }
                  break;
               }
          }
        sd->exit_now = EINA_FALSE;
        _cb_autozoom(data, sd);
     }
   else if (!strcmp(ev->key, "Down"))
     {
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if (en->selected)
               {
                  Evas_Coord x = 0, y = 0, w = 0, h = 0;
                  Evas_Coord sx = 0, sy = 0;

                  evas_object_geometry_get(en->o_bg, &x, &y, &w, &h);
                  sx = (x + (w / 2));
                  sy = (y + h + (h / 2));
                  EINA_LIST_FOREACH(sd->entries, l, en)
                    {
                       evas_object_geometry_get(en->o_bg, &x, &y, &w, &h);
                       if (ELM_RECTS_INTERSECT(x, y, w, h, sx, sy, 1, 1))
                         {
                            _selector_channel_selected_set(obj, en->chl, 
                                                           EINA_FALSE);
                            break;
                         }
                    }
                  break;
               }
          }
        sd->exit_now = EINA_FALSE;
        _cb_autozoom(data, sd);
     }
   else if ((!strcmp(ev->key, "Return")) || (!strcmp(ev->key, "KP_Enter")) || 
            (!strcmp(ev->key, "space")))
     {
        sd->select = EINA_TRUE;
        sd->exit_me = EINA_FALSE;
        if (sd->autozoom)
          {
             ecore_timer_del(sd->autozoom);
             sd->autozoom = NULL;
          }
        evas_object_smart_callback_call(data, "ending", NULL);
        _selector_zoom(data, 1.0);
     }
   else if (!strcmp(ev->key, "Escape"))
     {
        EINA_LIST_FOREACH(sd->entries, l, en)
          {
             if (en->orig) 
               {
                  _selector_channel_selected_set(obj, en->chl, EINA_FALSE);
                  break;
               }
          }

        sd->select = EINA_FALSE;
        sd->exit_me = EINA_TRUE;
        if (sd->autozoom)
          {
             ecore_timer_del(sd->autozoom);
             sd->autozoom = NULL;
          }
        evas_object_smart_callback_call(data, "ending", NULL);
        _selector_zoom(data, 1.0);
     }
}

static void 
_smart_add(Evas_Object *obj)
{
   Selector *sd;

   /* try to allocate space for selector structure */
   if (!(sd = calloc(1, sizeof(Selector)))) return;

   evas_object_smart_data_set(obj, sd);
   _parent_sc.add(obj);

   sd->evas = evas_object_evas_get(obj);

   sd->o_clip = evas_object_rectangle_add(sd->evas);
   evas_object_color_set(sd->o_clip, 255, 255, 255, 255);
   evas_object_smart_member_add(sd->o_clip, obj);
}

static void 
_smart_del(Evas_Object *obj)
{
   Selector *sd;
   Entry *en;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;

   if (sd->o_clip) evas_object_del(sd->o_clip);
   if (sd->o_event) evas_object_del(sd->o_event);
   if (sd->anim) ecore_animator_del(sd->anim);
   if (sd->autozoom) ecore_timer_del(sd->autozoom);

   EINA_LIST_FREE(sd->entries, en)
     {
        if (en->obj) 
          {
             evas_object_event_callback_del_full(en->obj, EVAS_CALLBACK_DEL, 
                                                 _cb_entry_del, en);
             evas_object_del(en->obj);
          }
        if (en->o_bg) evas_object_del(en->o_bg);
        free(en);
     }

   _parent_sc.del(obj);
}

static void 
_smart_resize(Evas_Object *obj, Evas_Coord w, Evas_Coord h)
{
   /* Selector *sd; */
   Evas_Coord ow = 0, oh = 0;

   /* try to get the objects smart data */
   /* if (!(sd = evas_object_smart_data_get(obj))) return; */

   /* get geometry of the object */
   evas_object_geometry_get(obj, NULL, NULL, &ow, &oh);
   if ((ow == w) && (oh == h)) return;

   evas_object_smart_changed(obj);
}

static void 
_smart_move(Evas_Object *obj, Evas_Coord x EINA_UNUSED, Evas_Coord y EINA_UNUSED)
{
   evas_object_smart_changed(obj);
}

static void 
_smart_calculate(Evas_Object *obj)
{
   Selector *sd;
   Evas_Coord ox = 0, oy = 0, ow = 0, oh = 0;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;

   evas_object_geometry_get(obj, &ox, &oy, &ow, &oh);
   if ((sd->w == ow) && (sd->h == oh)) return;

   sd->w = ow;
   sd->h = oh;

   evas_object_move(sd->o_clip, ox, oy);
   evas_object_resize(sd->o_clip, ow, oh);
   evas_object_move(sd->o_event, ox, oy);
   evas_object_resize(sd->o_event, ow, oh);

   _cb_layout(obj, sd);
}

static void 
_smart_init(void)
{
   static Evas_Smart_Class sc;

   evas_object_smart_clipped_smart_set(&_parent_sc);

   sc = _parent_sc;
   sc.name = "selector";
   sc.version = EVAS_SMART_CLASS_VERSION;
   sc.add = _smart_add;
   sc.del = _smart_del;
   sc.resize = _smart_resize;
   sc.move = _smart_move;
   sc.calculate = _smart_calculate;

   _smart = evas_smart_class_new(&sc);
}

/* external functions */
Evas_Object *
_selector_create(Evas *evas)
{
   Evas_Object *obj;
   Selector *sd;

   if (!_smart) _smart_init();

   obj = evas_object_smart_add(evas, _smart);

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return obj;

   sd->o_event = evas_object_rectangle_add(evas);
   evas_object_color_set(sd->o_event, 0, 0, 0, 0);
   evas_object_repeat_events_set(sd->o_event, EINA_TRUE);
   evas_object_smart_member_add(sd->o_event, obj);
   evas_object_clip_set(sd->o_event, sd->o_clip);
   evas_object_show(sd->o_event);

   evas_object_event_callback_add(sd->o_event, EVAS_CALLBACK_MOUSE_UP, 
                                  _cb_mouse_up, obj);
   evas_object_event_callback_add(sd->o_event, EVAS_CALLBACK_MOUSE_DOWN, 
                                  _cb_mouse_down, sd);
   evas_object_event_callback_add(sd->o_event, EVAS_CALLBACK_MOUSE_MOVE, 
                                  _cb_mouse_move, obj);

   evas_object_event_callback_add(obj, EVAS_CALLBACK_KEY_DOWN, 
                                  _cb_key_down, obj);

   sd->zoom = 1.0;

   return obj;
}

void 
_selector_channel_add(Evas_Object *obj, Channel *chl, Eina_Bool selected)
{
   Selector *sd;
   Entry *en;
   Eina_Bool bell = EINA_FALSE;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;

   /* try to allocate space for the Entry structure */
   if (!(en = calloc(1, sizeof(Entry)))) return;

   /* add this entry to the list */
   sd->entries = eina_list_append(sd->entries, en);

   bell = _channel_missed_get(chl);

   en->chl = chl;
   en->obj = _channel_image_get(chl);
   en->selected = selected;
   en->before = selected;
   en->orig = selected;

   /* create entry background */
   en->o_bg = edje_object_add(evas_object_evas_get(obj));
   _theme_apply(en->o_bg, "express/selector/entry");
   evas_object_smart_member_add(en->o_bg, obj);
   evas_object_clip_set(en->o_bg, sd->o_clip);
   edje_object_part_swallow(en->o_bg, "content", en->obj);
   evas_object_show(en->obj);

   evas_object_stack_below(en->o_bg, sd->o_event);
   if (en->selected)
     edje_object_signal_emit(en->o_bg, "selected,start", PACKAGE_NAME);
   if (bell) 
     edje_object_signal_emit(en->o_bg, "bell", PACKAGE_NAME);

   if ((en->selected) || (bell))
     edje_object_message_signal_process(en->o_bg);

   sd->interp = 1.0;

   edje_object_part_text_set(en->o_bg, "label", _channel_name_get(chl));

   /* TODO: callbacks add for bell, title, icon ?? */

   evas_object_event_callback_add(en->obj, EVAS_CALLBACK_DEL, 
                                  _cb_entry_del, en);
}

void 
_selector_channel_selected_set(Evas_Object *obj, Channel *chl, Eina_Bool keep)
{
   Selector *sd;
   Entry *en;
   Eina_List *l;
   Evas_Object *img;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;

   img = _channel_image_get(chl);
   EINA_LIST_FOREACH(sd->entries, l, en)
     {
        if (en->obj == img)
          {
             if ((sd->w > 0) && (sd->h > 0))
               edje_object_signal_emit(en->o_bg, "selected", PACKAGE_NAME);
             else
               sd->pending = EINA_TRUE;

             evas_object_stack_below(en->o_bg, sd->o_event);

             en->was_selected = EINA_FALSE;
             en->selected = EINA_TRUE;
          }
        else if (en->obj != img)
          {
             if (en->selected)
               {
                  if ((sd->w > 0) && (sd->h > 0))
                    edje_object_signal_emit(en->o_bg, 
                                            "unselected", PACKAGE_NAME);
                  else
                    {
                       en->was_selected = EINA_TRUE;
                       sd->pending = EINA_TRUE;
                    }
                  en->selected = EINA_FALSE;
               }
          }

        if (!keep) en->before = EINA_FALSE;
     }

   sd->use_px = EINA_FALSE;
   _cb_transit(obj, sd, _ex_cfg->gui.zoom);
}

void 
_selector_zoom_set(Evas_Object *obj, double zoom)
{
   Selector *sd;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;
   sd->orig_zoom = zoom;
}

void 
_selector_zoom(Evas_Object *obj, double zoom)
{
   Selector *sd;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;
   sd->zoom1 = zoom;
   _cb_transit(obj, sd, _ex_cfg->gui.zoom);
}

void 
_selector_go(Evas_Object *obj)
{
   Selector *sd;
   Entry *en;
   Eina_List *l;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;

   _cb_layout(obj, sd);

   evas_object_show(sd->o_clip);
   EINA_LIST_FOREACH(sd->entries, l, en)
     {
        if (en->selected)
          {
             evas_object_stack_below(en->o_bg, sd->o_event);
             break;
          }
     }
}

void 
_selector_exit(Evas_Object *obj)
{
   Selector *sd;

   /* try to get the objects smart data */
   if (!(sd = evas_object_smart_data_get(obj))) return;
   sd->exit_on_sel = EINA_TRUE;
}
