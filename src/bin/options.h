#ifndef _OPTIONS_H_
# define _OPTIONS_H_ 1

void _options_toggle(Evas_Object *win, Evas_Object *base, Evas_Object *grid, void (*cb_done)(void *data), void *data);

#endif
