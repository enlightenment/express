#include "private.h"
#include "utils.h"
#include "grid.h"

const char *extn_img[] =
{
   ".png", ".jpg", ".jpeg", ".jpe", ".jfif", ".tif", ".tiff", ".gif", ".thm",
   ".bmp", ".ico", ".ppm", ".pgm", ".pbm", ".pnm", ".xpm", ".psd", ".wbmp",
   ".cur", ".xcf", ".xcf.gz", ".arw", ".cr2", ".crw", ".dcr", ".dng", ".k25",
   ".kdc", ".erf", ".mrw", ".nef", ".nrf", ".nrw", ".orf", ".raw", ".rw2",
   ".pef", ".raf", ".sr2", ".srf", ".x3f", ".webp", ".ppt", ".pptx", ".odp",
   ".tgv", ".tga",
   NULL
};

const char *extn_scale[] =
{
   ".svg", ".svgz", ".svg.gz", /* ".ps", ".ps.gz", ".pdf", */
   NULL
};

const char *extn_edj[] =
{
   ".edj",
   NULL
};

const char *extn_mov[] =
{
   ".asf", ".avi", ".bdm", ".bdmv", ".clpi", ".cpi", ".dv", ".fla", ".flv",
   ".m1v", ".m2t", ".m2v", ".m4v", ".mkv", ".mov", ".mp2", ".mp2ts", ".mp4",
   ".mpe", ".mpeg", ".mpg", ".mpl", ".mpls", ".mts", ".mxf", ".nut", ".nuv",
   ".ogg", ".ogm", ".ogv", ".qt", ".rm", ".rmj", ".rmm", ".rms", ".rmvb",
   ".rmx", ".rv", ".swf", ".ts", ".weba", ".webm", ".wmv", ".3g2", ".3gp",
   ".3gp2", ".3gpp", ".3gpp2", ".3p2", ".264",
   ".mp3", ".aac", ".wav", ".flac", ".m4a", ".opus",
   NULL
};

const char *extn_aud[] =
{
   ".mp3", ".aac", ".wav", ".flac", ".m4a", ".opus", ".oga",
   NULL
};

/* translates VT100 ACS escape codes to Unicode values.
 * Based on rxvt-unicode screen.C table.
 */
static const unsigned short vt100_to_unicode[62] =
{
// ?       ?       ?       ?       ?       ?       ?
// A=UPARR B=DNARR C=RTARR D=LFARR E=FLBLK F=3/4BL G=SNOMN
   0x2191, 0x2193, 0x2192, 0x2190, 0x2588, 0x259a, 0x2603,
// H=      I=      J=      K=      L=      M=      N=
   0,      0,      0,      0,      0,      0,      0,
// O=      P=      Q=      R=      S=      T=      U=
   0,      0,      0,      0,      0,      0,      0,
// V=      W=      X=      Y=      Z=      [=      \=
   0,      0,      0,      0,      0,      0,      0,
// ?       ?       v->0    v->1    v->2    v->3    v->4
// ]=      ^=      _=SPC   `=DIAMN a=HSMED b=HT    c=FF
   0,      0,      0x0020, 0x25c6, 0x2592, 0x2409, 0x240c,
// v->5    v->6    v->7    v->8    v->9    v->a    v->b
// d=CR    e=LF    f=DEGRE g=PLSMN h=NL    i=VT    j=SL-BR
   0x240d, 0x240a, 0x00b0, 0x00b1, 0x2424, 0x240b, 0x2518,
// v->c    v->d    v->e    v->f    v->10   v->11   v->12
// k=SL-TR l=SL-TL m=SL-BL n=SL-+  o=SL-T1 p=SL-T2 q=SL-HZ
   0x2510, 0x250c, 0x2514, 0x253c, 0x23ba, 0x23bb, 0x2500,
// v->13   v->14   v->15   v->16   v->17   v->18   v->19
// r=SL-T4 s=SL-T5 t=SL-VR u=SL-VL v=SL-HU w=Sl-HD x=SL-VT
   0x23bc, 0x23bd, 0x251c, 0x2524, 0x2534, 0x252c, 0x2502,
// v->1a   v->1b   b->1c   v->1d   v->1e/a3 v->1f
// y=LT-EQ z=GT-EQ {=PI    |=NOTEQ }=POUND ~=DOT
   0x2264, 0x2265, 0x03c0, 0x2260, 0x20a4, 0x00b7
};

const char *
_util_user_dir_get(void)
{
   static char dir[PATH_MAX];

   if (!dir[0])
     snprintf(dir, sizeof(dir), "%s/express", efreet_config_home_get());

   return dir;
}

char *
_util_user_name_get(void)
{
   return getlogin();
}

char *
_util_user_fullname_get(void)
{
   char fullname[1024];
   char *usr;
   struct passwd *p;
   char *tmp;
   size_t n;

   if (!(usr = getenv("LOGNAME")))
     if (!(usr = getlogin())) return NULL;

   p = getpwnam(usr);
   if ((!p) && (errno == 0)) return NULL;
   if (!p) return NULL;

   tmp = strchr(p->pw_gecos, ',');
   if (!tmp)
     return NULL;

   n = tmp - p->pw_gecos;
   if (n > (sizeof(fullname) - 1)) n = (sizeof(fullname) - 1);

   memcpy(fullname, p->pw_gecos, n);
   fullname[n] = '\0';

   return strdup(fullname);
}

Eina_Unicode
_util_charset_translate(Eina_Unicode g, void *state)
{
   Grid_State *st;

   if (!(st = (Grid_State *)state)) return g;

   switch (st->charsetch)
     {
      case '0': /* DEC Special Character & Line Drawing Set */
        if ((g >= 0x41) && (g <= 0x7e) &&
            (vt100_to_unicode[g - 0x41]))
          return vt100_to_unicode[g - 0x41];
        break;
      case 'A': /* UK, replaces # with GBP */
        if (g == '#') return 0x20a4;
        break;
      default:
        break;
     }

   if (st->att.fraktur)
     {
        if (g >= 'a' && g <= 'z')
          g += 0x1d51e - 'a';
        else if (g >= 'A' && g <= 'Z')
          g += 0x1d504 - 'A';
     }

   return g;
}

Eina_Bool
_util_dblwidth_slow_get(void *data, int g)
{
   Grid *sd;

   if (!(sd = data)) return EINA_FALSE;

   // check for east asian full-width (F), half-width (H), wide (W),
   // narrow (Na) or ambiguous (A) codepoints
   // ftp://ftp.unicode.org/Public/UNIDATA/EastAsianWidth.txt

   // (W)
   if (
       // 1XXX
       ((g >= 0x1100) && (g <= 0x115f)) ||
       // 2XXX
       ((g == 0x2329) || (g == 0x232a)) ||
       ((g >= 0x2e80) && (g <= 0x2ffb)) ||
       // 3XXX -> A4C6
       ((g >= 0x3001) && (g <= 0x303f)) ||
       ((g >= 0x3041) && (g <= 0x3247)) ||
       ((g >= 0x3250) && (g <= 0x4dbf)) ||
       ((g >= 0x4e00) && (g <= 0xa4c6)) ||
       // aXXX
       ((g >= 0xa960) && (g <= 0xa97c)) ||
       ((g >= 0xac00) && (g <= 0xd7a3)) ||
       // fXXX
       ((g >= 0xf900) && (g <= 0xfaff)) ||
       ((g >= 0xfe10) && (g <= 0xfe19)) ||
       ((g >= 0xfe30) && (g <= 0xfe6b)) ||
       // 1XXXX
       ((g >= 0x1b000) && (g <= 0x1b001)) ||
       ((g >= 0x1f200) && (g <= 0x1f202)) ||
       ((g >= 0x1f210) && (g <= 0x1f251)) ||
       // 2XXXX
       ((g >= 0x20000) && (g <= 0x2fffd)) ||
       // 3XXXX
       ((g >= 0x30000) && (g <= 0x3FFFD)))
     return EINA_TRUE;
   // FIXME: can optimize by breaking into tree and ranges
   // (A)
   if (sd->state.cjk_ambiguous_wide)
     {
        // grep ';A #' EastAsianWidth.txt | wc -l
        // :(
        if (
            // aX
            (((g >> 4) == 0xa) &&
                ((g == 0x00a1) ||
                 (g == 0x00a4) ||
                 ((g >= 0x00a7) && (g <= 0x00a8)) ||
                 (g == 0x00aa) ||
                 ((g >= 0x00ad) && (g <= 0x00ae)))) ||
            // bX
            (((g >> 4) == 0xb) &&
                (((g >= 0x00b0) && (g <= 0x00bf)))) ||
            // cX
            (((g >> 4) == 0xc) &&
                ((g == 0x00c6))) ||
            // dX
            (((g >> 4) == 0xd) &&
                ((g == 0x00d0) ||
                 ((g >= 0x00d7) && (g <= 0x00d8)) ||
                 ((g >= 0x00de) && (g <= 0x00df)))) ||
            // eX
            (((g >> 4) == 0xe) &&
                ((g == 0x00e0) ||
                 (g == 0x00e1) ||
                 (g == 0x00e6) ||
                 ((g >= 0x00e8) && (g <= 0x00e9)) ||
                 (g == 0x00ea) ||
                 ((g >= 0x00ec) && (g <= 0x00ed)))) ||
            // fX
            (((g >> 4) == 0xf) &&
                ((g == 0x00f0) ||
                 ((g >= 0x00f2) && (g <= 0x00f3)) ||
                 ((g >= 0x00f7) && (g <= 0x00f9)) ||
                 (g == 0x00fa) ||
                 (g == 0x00fc) ||
                 (g == 0x00fe))) ||
            // 1XX
            (((g >> 8) == 0x1) &&
                ((g == 0x0101) ||
                 (g == 0x0111) ||
                 (g == 0x0113) ||
                 (g == 0x011b) ||
                 ((g >= 0x0126) && (g <= 0x0127)) ||
                 (g == 0x012b) ||
                 ((g >= 0x0131) && (g <= 0x0133)) ||
                 (g == 0x0138) ||
                 ((g >= 0x013f) && (g <= 0x0142)) ||
                 (g == 0x0144) ||
                 ((g >= 0x0148) && (g <= 0x014b)) ||
                 (g == 0x014d) ||
                 ((g >= 0x0152) && (g <= 0x0153)) ||
                 ((g >= 0x0166) && (g <= 0x0167)) ||
                 (g == 0x016b) ||
                 (g == 0x01ce) ||
                 (g == 0x01d0) ||
                 (g == 0x01d2) ||
                 (g == 0x01d4) ||
                 (g == 0x01d6) ||
                 (g == 0x01d8) ||
                 (g == 0x01da) ||
                 (g == 0x01dc))) ||
            // 2XX
            (((g >> 8) == 0x2) &&
                ((g == 0x0251) ||
                 (g == 0x0261) ||
                 (g == 0x02c4) ||
                 (g == 0x02c7) ||
                 (g == 0x02c9) ||
                 ((g >= 0x02ca) && (g <= 0x02cb)) ||
                 (g == 0x02cd) ||
                 (g == 0x02d0) ||
                 ((g >= 0x02d8) && (g <= 0x02d9)) ||
                 ((g >= 0x02da) && (g <= 0x02db)) ||
                 (g == 0x02dd) ||
                 (g == 0x02df))) ||
            // 3XX
            (((g >> 8) == 0x3) &&
                (((g >= 0x0300) && (g <= 0x036f)) ||
                 ((g >= 0x0391) && (g <= 0x03c9)))) ||
            // 4XX
            (((g >> 8) == 0x4) &&
                ((g == 0x0401) ||
                 ((g >= 0x0410) && (g <= 0x044f)) ||
                 (g == 0x0451))) ||
            // 2XXX
            (((g >> 12) == 0x2) &&
                ((((g >> 8) == 0x20) &&
                  ((g == 0x2010) ||
                   ((g >= 0x2013) && (g <= 0x2016)) ||
                   ((g >= 0x2018) && (g <= 0x2019)) ||
                   (g == 0x201c) ||
                   (g == 0x201d) ||
                   ((g >= 0x2020) && (g <= 0x2022)) ||
                   ((g >= 0x2024) && (g <= 0x2027)) ||
                   (g == 0x2030) ||
                   ((g >= 0x2032) && (g <= 0x2033)) ||
                   (g == 0x2035) ||
                   (g == 0x203b) ||
                   (g == 0x203e) ||
                   (g == 0x2074) ||
                   (g == 0x207f) ||
                   ((g >= 0x2081) && (g <= 0x2084)) ||
                   (g == 0x20ac))) ||
                 (((g >> 8) == 0x21) &&
                  ((g == 0x2103) ||
                   (g == 0x2105) ||
                   (g == 0x2109) ||
                   (g == 0x2113) ||
                   (g == 0x2116) ||
                   ((g >= 0x2121) && (g <= 0x2122)) ||
                   (g == 0x2126) ||
                   (g == 0x212b) ||
                   ((g >= 0x2153) && (g <= 0x2154)) ||
                   ((g >= 0x215b) && (g <= 0x215e)) ||
                   ((g >= 0x2160) && (g <= 0x216b)) ||
                   ((g >= 0x2170) && (g <= 0x2179)) ||
                   ((g >= 0x2189) && (g <= 0x2199)) ||
                   ((g >= 0x21b8) && (g <= 0x21b9)) ||
                   (g == 0x21d2) ||
                   (g == 0x21d4) ||
                   (g == 0x21e7))) ||
                 (((g >> 8) == 0x22) &&
                  ((g == 0x2200) ||
                   ((g >= 0x2202) && (g <= 0x2203)) ||
                   ((g >= 0x2207) && (g <= 0x2208)) ||
                   (g == 0x220b) ||
                   (g == 0x220f) ||
                   (g == 0x2211) ||
                   (g == 0x2215) ||
                   (g == 0x221a) ||
                   ((g >= 0x221d) && (g <= 0x221f)) ||
                   (g == 0x2220) ||
                   (g == 0x2223) ||
                   (g == 0x2225) ||
                   ((g >= 0x2227) && (g <= 0x222e)) ||
                   ((g >= 0x2234) && (g <= 0x2237)) ||
                   ((g >= 0x223c) && (g <= 0x223d)) ||
                   (g == 0x2248) ||
                   (g == 0x224c) ||
                   (g == 0x2252) ||
                   ((g >= 0x2260) && (g <= 0x2261)) ||
                   ((g >= 0x2264) && (g <= 0x2267)) ||
                   ((g >= 0x226a) && (g <= 0x226b)) ||
                   ((g >= 0x226e) && (g <= 0x226f)) ||
                   ((g >= 0x2282) && (g <= 0x2283)) ||
                   ((g >= 0x2286) && (g <= 0x2287)) ||
                   (g == 0x2295) ||
                   (g == 0x2299) ||
                   (g == 0x22a5) ||
                   (g == 0x22bf))) ||
                 (((g >> 8) == 0x23) &&
                  ((g == 0x2312))) ||
                 ((((g >> 8) == 0x24) || ((g >> 8) == 0x25)) &&
                  (((g >= 0x2460) && (g <= 0x2595)))) ||
                 (((g >> 8) == 0x25) &&
                  (((g >= 0x25a0) && (g <= 0x25bd)) ||
                   ((g >= 0x25c0) && (g <= 0x25c1)) ||
                   ((g >= 0x25c6) && (g <= 0x25c7)) ||
                   (g == 0x25c8) ||
                   (g == 0x25cb) ||
                   ((g >= 0x25ce) && (g <= 0x25cf)) ||
                   ((g >= 0x25d0) && (g <= 0x25d1)) ||
                   ((g >= 0x25e2) && (g <= 0x25e3)) ||
                   ((g >= 0x25e4) && (g <= 0x25e5)) ||
                   (g == 0x25ef))) ||
                 (((g >> 8) == 0x26) &&
                  (((g >= 0x2605) && (g <= 0x2606)) ||
                   (g == 0x2609) ||
                   ((g >= 0x260e) && (g <= 0x260f)) ||
                   ((g >= 0x2614) && (g <= 0x2615)) ||
                   (g == 0x261c) ||
                   (g == 0x261e) ||
                   (g == 0x2640) ||
                   (g == 0x2642) ||
                   ((g >= 0x2660) && (g <= 0x2661)) ||
                   ((g >= 0x2663) && (g <= 0x2665)) ||
                   ((g >= 0x2667) && (g <= 0x266a)) ||
                   ((g >= 0x266c) && (g <= 0x266d)) ||
                   (g == 0x266f) ||
                   ((g >= 0x269e) && (g <= 0x269f)) ||
                   ((g >= 0x26be) && (g <= 0x26bf)) ||
                   ((g >= 0x26c4) && (g <= 0x26cd)) ||
                   (g == 0x26cf) ||
                   ((g >= 0x26d0) && (g <= 0x26e1)) ||
                   (g == 0x26e3) ||
                   ((g >= 0x26e8) && (g <= 0x26ff)))) ||
                 (((g >> 8) == 0x27) &&
                  ((g == 0x273d) ||
                   (g == 0x2757) ||
                   ((g >= 0x2776) && (g <= 0x277f)))) ||
                 (((g >> 8) == 0x2b) &&
                  (((g >= 0x2b55) && (g <= 0x2b59)))))) ||
            // 3XXX
            (((g >> 12) == 0x3) &&
                (((g >= 0x3248) && (g <= 0x324f)))) ||
            // eXXX
            (((g >> 12) == 0xe) &&
                (((g >= 0xe000) && (g <= 0xf8ff)))) ||
            // fXXX
            (((g >> 12) == 0xf) &&
                (((g >= 0xfe00) && (g <= 0xfe0f)) ||
                 (g == 0xfffd))) ||
            // 1XXXX
            (((g >> 16) == 0x1) &&
                (((g >= 0x1f100) && (g <= 0x1f12d)) ||
                 ((g >= 0x1f130) && (g <= 0x1f169)) ||
                 ((g >= 0x1f170) && (g <= 0x1f19a)))) ||
            // eXXXX
            (((g >> 16) == 0xe) &&
                (((g >= 0xe0100) && (g <= 0xe01ef)))) ||
            // fXXXX
            (((g >> 16) == 0xf) &&
                (((g >= 0xf0000) && (g <= 0xffffd)))) ||
            // 1XXXXX
            (((g >> 24) == 0x1) &&
                (((g >= 0x100000) && (g <= 0x10fffd)))))
          return EINA_TRUE;
     }

   // Na, H -> not checked
   return EINA_FALSE;
}

int
_util_codepoint_to_utf8(Eina_Unicode g, char *txt)
{
   if (g < (1 << (7)))
     { // 0xxxxxxx
        txt[0] = g & 0x7f;
        txt[1] = 0;
        return 1;
     }
   else if (g < (1 << (5 + 6)))
     { // 110xxxxx 10xxxxxx
        txt[0] = 0xc0 | ((g >> 6) & 0x1f);
        txt[1] = 0x80 | ((g     ) & 0x3f);
        txt[2] = 0;
        return 2;
     }
   else if (g < (1 << (4 + 6 + 6)))
     { // 1110xxxx 10xxxxxx 10xxxxxx
        txt[0] = 0xe0 | ((g >> 12) & 0x0f);
        txt[1] = 0x80 | ((g >> 6 ) & 0x3f);
        txt[2] = 0x80 | ((g      ) & 0x3f);
        txt[3] = 0;
        return 3;
     }
   else if (g < (1 << (3 + 6 + 6 + 6)))
     { // 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
        txt[0] = 0xf0 | ((g >> 18) & 0x07);
        txt[1] = 0x80 | ((g >> 12) & 0x3f);
        txt[2] = 0x80 | ((g >> 6 ) & 0x3f);
        txt[3] = 0x80 | ((g      ) & 0x3f);
        txt[4] = 0;
        return 4;
     }
   else if (g < (1 << (2 + 6 + 6 + 6 + 6)))
     { // 111110xx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        txt[0] = 0xf8 | ((g >> 24) & 0x03);
        txt[1] = 0x80 | ((g >> 18) & 0x3f);
        txt[2] = 0x80 | ((g >> 12) & 0x3f);
        txt[3] = 0x80 | ((g >> 6 ) & 0x3f);
        txt[4] = 0x80 | ((g      ) & 0x3f);
        txt[5] = 0;
        return 5;
     }
   else if ((unsigned int)g < (unsigned int)(1 << (1 + 6 + 6 + 6 + 6 + 6)))
     { // 1111110x 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx 10xxxxxx
        txt[0] = 0xfc | ((g >> 30) & 0x01);
        txt[1] = 0x80 | ((g >> 24) & 0x3f);
        txt[2] = 0x80 | ((g >> 18) & 0x3f);
        txt[3] = 0x80 | ((g >> 12) & 0x3f);
        txt[4] = 0x80 | ((g >> 6 ) & 0x3f);
        txt[5] = 0x80 | ((g      ) & 0x3f);
        txt[6]  = 0;
        return 6;
     }
   else
     { // error - cant encode this in utf8
        txt[0] = 0;
        return 0;
     }
}

Eina_Bool
_util_link_is_url(const char *str)
{
   if ((_util_link_is_protocol(str)) ||
       (eina_str_has_prefix(str, "www.")) ||
       (eina_str_has_prefix(str, "ftp.")))
     return EINA_TRUE;

   return EINA_FALSE;
}

Eina_Bool
_util_link_is_email(const char *str)
{
   const char *at;

   at = strchr(str, '@');
   if (at && strchr(at + 1, '.'))
     return EINA_TRUE;
   if (eina_str_has_prefix(str, "mailto:"))
     return EINA_TRUE;

   return EINA_FALSE;
}

Eina_Bool
_util_link_is_protocol(const char *str)
{
   if ((eina_str_has_prefix(str, "http://")) ||
       (eina_str_has_prefix(str, "https://")) ||
       (eina_str_has_prefix(str, "ftp://")) ||
       (eina_str_has_prefix(str, "file://")) ||
       (eina_str_has_prefix(str, "mailto:")))
     return EINA_TRUE;

   return EINA_FALSE;
}

Eina_Bool
_util_is_file(const char *str)
{
   switch (str[0])
     {
      case '/':
        return EINA_TRUE;
      case '~':
        if (str[1] == '/')
          return EINA_TRUE;
        return EINA_FALSE;
      case '.':
        if (str[1] == '/')
          return EINA_TRUE;
        else if ((str[1] == '.') && (str[2] == '/'))
          return EINA_TRUE;
        return EINA_FALSE;
      default:
        return EINA_FALSE;
     }
}

static Eina_Bool
_homedir_get(char *buf, size_t size)
{
   const char *home = NULL;

#if defined (HAVE_GETUID) && defined(HAVE_GETEUID)
   if (getuid() != geteuid())
     {
        struct passwd *pw;

        pw = getpwent();
        if ((pw) && (pw->pw_dir)) home = pw->pw_dir;
     }
   else
#endif
     home = eina_environment_home_get();

   if ((!home) || !*home)
     eina_environment_tmp_get();

   return eina_strlcpy(buf, home, size) < size;
}

static char *
_home_path_get(const Evas_Object *obj EINA_UNUSED, const char *relpath)
{
   char tmppath[PATH_MAX], homepath[PATH_MAX];

   if (!_homedir_get(homepath, sizeof(homepath)))
     return NULL;

   eina_file_path_join(tmppath, sizeof(tmppath), homepath, relpath);
   return strdup(tmppath);
}

static char *
_cwd_path_get(const char *relpath)
{
   char cwdpath[PATH_MAX], tmppath[PATH_MAX];
   char *tmp = NULL;

   tmp = getcwd(cwdpath, PATH_MAX);
   if (!tmp) return NULL;

   eina_file_path_join(tmppath, sizeof(tmppath), cwdpath, relpath);
   return strdup(tmppath);
}

char *
_util_local_path_get(const Evas_Object *obj, const char *relpath)
{
#ifdef _WIN32
   if (evil_path_is_absolute(relpath))
     return strdup(relpath);
   else
#endif
   if (relpath[0] == '/')
     return strdup(relpath);
   else if (eina_str_has_prefix(relpath, "~/"))
     return _home_path_get(obj, relpath + 2);
   else
     return _cwd_path_get(relpath);
}

Eina_Bool
_util_str_has_prefix(const char *str, const char *prefix)
{
   size_t slen, plen;

   slen = strlen(str);
   plen = eina_strlen_bounded(prefix, slen);
   if (plen == (size_t)-1) return EINA_FALSE;

   return (strncasecmp(str, prefix, plen) == 0);
}

const char *
_util_file_extension_get(const char *f, const char **extension)
{
   int i = 0, len = 0, l = 0;

   len = strlen(f);
   for (; extension[i]; i++)
     {
        l = strlen(extension[i]);
        if (len < l) continue;
        if (!strcasecmp(extension[i], f + len - l))
          return extension[i];
     }

   return NULL;
}
