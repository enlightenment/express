#include "private.h"
#include "channel.h"
#include "commands.h"

static Eina_Bool
_txt_handle(Express_Network *net, const char *name, char *str, char *buff)
{
   snprintf(buff, PATH_MAX, "%s\r\n", str);
   express_network_channel_priv_send(net, name, buff);

   return EINA_TRUE;
}

static Eina_Bool
_cmd_handle(Express_Network *net, char *cmd, char *parameter, char *buff)
{
   /* FIXME: We need to add support for lowercase command shortcuts
    * (ie: /j to join, etc) */
   cmd++;

   if (parameter)
     snprintf(buff, PATH_MAX, "%s %s\r\n", cmd, parameter);
   else
     snprintf(buff, PATH_MAX, "%s\r\n", cmd);

   express_network_command_send(net, buff);

   return EINA_FALSE;
}

Eina_Bool
_cmd_txt_handle(Express_Network *net, const char *name, char *str, char *buff)
{
   char **tokens;
   Eina_Bool ret;

   tokens = eina_str_split(str, " ", 2);

   if (tokens[0][0] == '/')
     ret = _cmd_handle(net, tokens[0], tokens[1], buff);
   else
     ret = _txt_handle(net, name, str, buff);

   free(tokens);
   return ret;
}
