#include "private.h"
#include "options_colors.h"
#include "config.h"
#include "theme.h"
#include "colors.h"

static const Color colors256[8] = 
{
   { 0x00, 0x00, 0x00, 0xff }, // COL_BLACK
   { 0xc0, 0x00, 0x00, 0xff }, // COL_RED
   { 0x00, 0xc0, 0x00, 0xff }, // COL_GREEN
   { 0xc0, 0xc0, 0x00, 0xff }, // COL_YELLOW
   { 0x00, 0x00, 0xc0, 0xff }, // COL_BLUE
   { 0xc0, 0x00, 0xc0, 0xff }, // COL_MAGENTA
   { 0x00, 0xc0, 0xc0, 0xff }, // COL_CYAN
   { 0xc0, 0xc0, 0xc0, 0xff }, // COL_WHITE
};

typedef struct _Opt_Color Opt_Color;
struct _Opt_Color
{
   const char *name;
   unsigned char r, g, b, a;
};

static Evas_Object *
_cb_color_content_get(void *data, Evas_Object *obj, const char *part EINA_UNUSED)
{
   Opt_Color *c;
   Evas_Object *b, *o;
   int i = 0;

   c = data;

   b = elm_box_add(obj);
   elm_box_horizontal_set(b, EINA_TRUE);
   WEIGHT_SET(b, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(b, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_min_set(b, ELM_SCALE_SIZE(128), 
                                 ELM_SCALE_SIZE(48));
   evas_object_show(b);

   o = elm_label_add(obj);
   elm_object_text_set(o, c->name);
   evas_object_show(o);
   elm_box_pack_end(b, o);

   o = elm_colorselector_add(obj);
   elm_colorselector_mode_set(o, ELM_COLORSELECTOR_PALETTE);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_FILL);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);

   for (; i < 8; i++)
     {
        Color color;

        color = colors256[i];
        elm_colorselector_palette_color_add(o, color.r, color.g, color.b, 
                                            color.a);
     }

   evas_object_show(o);
   elm_box_pack_end(b, o);

   return b;
}

static char *
_cb_group_text_get(void *data, Evas_Object *obj EINA_UNUSED, const char *part EINA_UNUSED)
{
   return strdup(data);
}

void 
_options_colors(Evas_Object *box, Evas_Object *grid EINA_UNUSED)
{
   Evas_Object *o_frame, *vbox, *o;
   Elm_Object_Item *it, *grp_it;
   Elm_Genlist_Item_Class *it_class, *it_group;
   Opt_Color *c;

   /* font frame */
   o_frame = elm_frame_add(box);
   WEIGHT_SET(o_frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o_frame, "Colors");
   evas_object_show(o_frame);

   vbox = elm_box_add(box);
   elm_box_horizontal_set(vbox, EINA_FALSE);
   elm_box_homogeneous_set(vbox, EINA_FALSE);
   WEIGHT_SET(vbox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(vbox, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(o_frame, vbox);
   evas_object_show(vbox);

   it_class = elm_genlist_item_class_new();
   it_class->item_style = "full";
   it_class->func.text_get = NULL;
   it_class->func.content_get = _cb_color_content_get;

   it_group = elm_genlist_item_class_new();
   it_group->item_style = "group_index";
   it_group->func.text_get = _cb_group_text_get;

   o = elm_genlist_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_genlist_mode_set(o, ELM_LIST_COMPRESS);
   elm_genlist_homogeneous_set(o, EINA_TRUE);
   evas_object_show(o);

   /* create text color group */
   grp_it = elm_genlist_item_append(o, it_group, "Text Colors", NULL, 
                                    ELM_GENLIST_ITEM_GROUP, NULL, NULL);
   elm_genlist_item_select_mode_set(grp_it, 
                                    ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);


   c = calloc(1, sizeof(Opt_Color));
   c->name = eina_stringshare_add("Foreground");

   /* create text color entries */
   it = elm_genlist_item_append(o, it_class, c, grp_it, 
                                ELM_GENLIST_ITEM_NONE, NULL, c);
   WEIGHT_SET(it, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(it, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_genlist_item_select_mode_set(it, 
                                    ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);


   c = calloc(1, sizeof(Opt_Color));
   c->name = eina_stringshare_add("Background");

   /* create text color entries */
   it = elm_genlist_item_append(o, it_class, c, grp_it, 
                                ELM_GENLIST_ITEM_NONE, NULL, c);
   WEIGHT_SET(it, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(it, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_genlist_item_select_mode_set(it, 
                                    ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);


   elm_genlist_item_class_free(it_class);
   elm_genlist_item_class_free(it_group);

   elm_box_pack_end(vbox, o);
   elm_box_pack_end(box, o_frame);
}
