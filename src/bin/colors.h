#ifndef _COLORS_H_
# define _COLORS_H_ 1

typedef struct _Color Color;

struct _Color
{
   unsigned char r, g, b, a;
};

void _colors_init(Evas_Object *obj, Evas_Object *bg);
void _colors_standard_get(int set, int col, unsigned char *r, unsigned char *g, unsigned char *b, unsigned char *a);

#endif
