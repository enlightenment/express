#include "private.h"
#include "options.h"
#include "options_general.h"
#include "options_font.h"
#include "options_colors.h"
#include "options_video.h"
#include "options_networks.h"
#include "options_tools.h"
#include "window.h"

/* local variables */
static Evas_Object *o_win = NULL;
static Evas_Object *o_base = NULL;
static Evas_Object *o_frame = NULL;
static Evas_Object *o_dismiss = NULL;
static Evas_Object *o_box = NULL;
static Evas_Object *o_tbr = NULL;
static Ecore_Timer *_opts_del_timer = NULL;
static Eina_Bool _opts_out = EINA_FALSE;
static void (*_opts_cb_done)(void *data) = NULL;
static void *_opts_cb_data = NULL;

static enum _opt_mode
{
   OPTION_NONE = 0,
   OPTION_GENERAL,
   OPTION_FONT,
   OPTION_COLORS,
   OPTION_VIDEO,
   OPTION_NETWORKS
} _mode = 0;

/* local functions */
static void 
_cb_dismiss_mouse_down(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   _options_tools(o_box, o_base, NULL);
   _options_toggle(o_win, o_base, data, _opts_cb_done, _opts_cb_data);
}

static void 
_cb_dismiss_del(void *data EINA_UNUSED, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   o_dismiss = NULL;
}

static Eina_Bool 
_cb_del_delay(void *data EINA_UNUSED)
{
   if (o_tbr) evas_object_del(o_tbr);
   o_tbr = NULL;
   if (o_frame) evas_object_del(o_frame);
   o_frame = NULL;
   if (o_box) evas_object_del(o_box);
   o_box = NULL;

   _opts_del_timer = NULL;
   elm_cache_all_flush();

   _window_update();

   return EINA_FALSE;
}

static void 
_cb_details_done(void *data, Evas_Object *obj EINA_UNUSED, const char *sig EINA_UNUSED, const char *src EINA_UNUSED)
{
   Evas_Object *grid;

   if (!(grid = data)) return;

   _options_tools(o_box, o_base, NULL);

   elm_box_clear(o_box);

   switch (_mode)
     {
      case OPTION_NONE:
        break;
      case OPTION_GENERAL:
        _options_general(o_box, grid);
        break;
      case OPTION_FONT:
        _options_font(o_box, grid);
        break;
      case OPTION_COLORS:
        _options_colors(o_box, grid);
        break;
      case OPTION_VIDEO:
        _options_video(o_box, grid);
        break;
      case OPTION_NETWORKS:
        _options_networks(o_box, o_base);
        break;
      default:
        break;
     }

   edje_object_signal_emit(o_base, "options,details,show", PACKAGE_NAME);
}

static void 
_cb_details_done2(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, const char *sig EINA_UNUSED, const char *src EINA_UNUSED)
{
   if (_opts_del_timer) ecore_timer_del(_opts_del_timer);
   _opts_del_timer = NULL;

   _cb_del_delay(NULL);

   edje_object_signal_callback_del(o_base, "options,details,hide,done", 
                                   PACKAGE_NAME, _cb_details_done2);
}

static void 
_cb_option(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   enum _opt_mode mode = (intptr_t)data;

   if (_mode == mode) return;
   _mode = mode;
   edje_object_signal_emit(o_base, "options,details,hide", PACKAGE_NAME);
}

/* external functions */
void 
_options_toggle(Evas_Object *win, Evas_Object *base, Evas_Object *grid, void (*cb_done)(void *data), void *data)
{
   Elm_Object_Item *o_itm = NULL;

   _mode = OPTION_NONE;

   if ((win != o_win) && (o_frame))
     {
        evas_object_del(o_dismiss);
        evas_object_del(o_tbr);
        evas_object_del(o_frame);
        evas_object_del(o_box);
        o_dismiss = NULL;
        o_tbr = NULL;
        o_frame = NULL;
        o_box = NULL;
     }

   o_win = win;
   o_base = base;

   if (!o_frame)
     {
        o_box = elm_box_add(win);
        elm_box_align_set(o_box, 0.0, 0.0);
        WEIGHT_SET(o_box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        ALIGN_SET(o_box, EVAS_HINT_FILL, EVAS_HINT_FILL);
        edje_object_part_swallow(base, "option_details", o_box);
        evas_object_show(o_box);

        o_frame = elm_frame_add(win);
        WEIGHT_SET(o_frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        ALIGN_SET(o_frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
        elm_object_text_set(o_frame, "Options");

        o_tbr = elm_toolbar_add(win);
        elm_toolbar_menu_parent_set(o_tbr, win);
        elm_toolbar_horizontal_set(o_tbr, EINA_FALSE);
        elm_toolbar_shrink_mode_set(o_tbr, ELM_TOOLBAR_SHRINK_SCROLL);
        elm_toolbar_select_mode_set(o_tbr, ELM_OBJECT_SELECT_MODE_ALWAYS);
        WEIGHT_SET(o_tbr, 0.0, EVAS_HINT_EXPAND);
        ALIGN_SET(o_tbr, EVAS_HINT_FILL, EVAS_HINT_FILL);

#define TB_APPEND(_icon, _label, _opt_mode) \
   elm_toolbar_item_append(o_tbr, _icon, _label, _cb_option, \
                           (void *) OPTION_##_opt_mode)

        o_itm = 
          TB_APPEND("preferences-system", "General", GENERAL);
        elm_toolbar_item_selected_set(o_itm, EINA_TRUE);

        TB_APPEND("preferences-desktop-font", "Font", FONT);
        /* TB_APPEND("preferences-desktop-theme", "Colors", COLORS); */
        TB_APPEND("video-display", "Video", VIDEO);
        TB_APPEND("preferences-system-network", "Networks", NETWORKS);
#undef TB_APPEND

        elm_object_content_set(o_frame, o_tbr);
        evas_object_show(o_tbr);

        edje_object_part_swallow(base, "options_details", o_box);
        edje_object_part_swallow(base, "options", o_frame);
        evas_object_show(o_frame);
     }
   else if ((o_box) && (!_opts_out))
     {
        edje_object_part_swallow(base, "options_details", o_box);
        edje_object_part_swallow(base, "options", o_frame);
        edje_object_signal_emit(base, "options,details,show", PACKAGE_NAME);
        edje_object_signal_emit(base, "options,show", PACKAGE_NAME);
     }

   if (!_opts_out)
     {
        edje_object_signal_callback_add(base, "options,details,hide,done", 
                                        PACKAGE_NAME, _cb_details_done, grid);

        o_dismiss = evas_object_rectangle_add(evas_object_evas_get(win));
        evas_object_color_set(o_dismiss, 0, 0, 0, 0);
        evas_object_event_callback_add(o_dismiss, EVAS_CALLBACK_MOUSE_DOWN, 
                                       _cb_dismiss_mouse_down, grid);
        evas_object_event_callback_add(o_dismiss, EVAS_CALLBACK_DEL, 
                                       _cb_dismiss_del, NULL);
        edje_object_part_swallow(base, "dismiss", o_dismiss);
        evas_object_show(o_dismiss);

        _opts_cb_done = cb_done;
        _opts_cb_data = data;

        edje_object_signal_emit(base, "options,details,hide", PACKAGE_NAME);
        edje_object_signal_emit(base, "options,show", PACKAGE_NAME);
        elm_object_focus_set(o_tbr, EINA_TRUE);
        _opts_out = EINA_TRUE;

        if (_opts_del_timer) ecore_timer_del(_opts_del_timer);
        _opts_del_timer = NULL;
     }
   else
     {
        edje_object_signal_callback_del(o_base, "options,details,hide,done", 
                                        PACKAGE_NAME, _cb_details_done);
        edje_object_signal_callback_add(o_base, "options,details,hide,done", 
                                        PACKAGE_NAME, _cb_details_done2, NULL);

        elm_object_focus_set(o_box, EINA_FALSE);
        elm_object_focus_set(o_tbr, EINA_FALSE);
        elm_object_focus_set(o_frame, EINA_FALSE);

        if (_opts_cb_done) _opts_cb_done(_opts_cb_data);

        if (o_dismiss) evas_object_del(o_dismiss);
        o_dismiss = NULL;

        edje_object_signal_emit(o_base, "options,hide", PACKAGE_NAME);
        edje_object_signal_emit(o_base, "options,details,hide", PACKAGE_NAME);
        _opts_out = EINA_FALSE;

        if (_opts_del_timer) ecore_timer_del(_opts_del_timer);
        _opts_del_timer = ecore_timer_add(10.0, _cb_del_delay, NULL);
     }
}
