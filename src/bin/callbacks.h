#ifndef _CALLBACKS_H_
# define _CALLBACKS_H_ 1

void _callback_server_connected(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_server_motd(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_server_notice(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_server_error(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);

void _callback_channel_message(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_channel_notice(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_channel_topic(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_channel_topic_time(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_channel_names(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);

void _callback_command(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_user_quit(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_user_part(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_user_join(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_user_private(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_user_nick(Express_Network *net, const char *event, const char *source, const char **params, unsigned int count, void *data);
void _callback_user_mode(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED);

#endif
