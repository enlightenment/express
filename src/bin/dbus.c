#include "private.h"
#include "dbus.h"
#ifdef HAVE_ELDBUS
#include <Eldbus.h>

static Eldbus_Connection *_dbus_conn = NULL;
static Eldbus_Object *_dbus_obj = NULL;
static Eina_Stringshare *_curr_url = NULL;

void 
_dbus_init(void)
{
   if (_dbus_conn) return;

   eldbus_init();

   if (!elm_need_sys_notify())
     WRN("Elementary does not have system notification support");

   _dbus_conn = eldbus_connection_get(ELDBUS_CONNECTION_TYPE_SESSION);
   _dbus_obj = 
     eldbus_object_get(_dbus_conn,"org.enlightenment.wm.service",
                       "/org/enlightenment/wm/RemoteObject");
}

void 
_dbus_shutdown(void)
{
   _dbus_link_hide();

   if (_dbus_conn) eldbus_connection_unref(_dbus_conn);
   _dbus_conn = NULL;

   _dbus_obj = NULL;

   eldbus_shutdown();
}

void 
_dbus_link_hide(void)
{
   Eldbus_Message *msg;

   if ((!_dbus_obj) || (!_curr_url)) return;

   msg = eldbus_message_method_call_new("org.enlightenment.wm.service",
                                        "/org/enlightenment/wm/RemoteObject",
                                        "org.enlightenment.wm.Teamwork",
                                        "LinkHide");

   eldbus_message_arguments_append(msg, "s", _curr_url);
   eldbus_object_send(_dbus_obj, msg, NULL, NULL, -1);

   eina_stringshare_replace(&_curr_url, NULL);
}

void 
_dbus_link_mousein(uint64_t win, const char *url, int x, int y)
{
   Eldbus_Message *msg;
   Eina_Stringshare *u;

   if (!_dbus_obj) return;

   u = eina_stringshare_add(url);
   /* if previous link exists, do MouseOut now */
   if (_curr_url && (u != _curr_url))
     _dbus_link_mouseout(win, _curr_url, x, y);
   eina_stringshare_del(_curr_url);
   _curr_url = u;

   msg = eldbus_message_method_call_new("org.enlightenment.wm.service",
                                        "/org/enlightenment/wm/RemoteObject",
                                        "org.enlightenment.wm.Teamwork",
                                        "LinkMouseIn");

#if (ELM_VERSION_MAJOR > 1) || (ELM_VERSION_MINOR > 8) // not a typo
   eldbus_message_arguments_append(msg, "sutii", _curr_url, 
                                   time(NULL), win, x, y);
#else
   eldbus_message_arguments_append(msg, "suxii", _curr_url, 
                                   time(NULL), (int64_t)win, x, y);
#endif
   eldbus_object_send(_dbus_obj, msg, NULL, NULL, -1);
}

void 
_dbus_link_mouseout(uint64_t win, const char *url, int x, int y)
{
   Eldbus_Message *msg;

   if (!_dbus_obj) return;

   msg = eldbus_message_method_call_new("org.enlightenment.wm.service",
                                        "/org/enlightenment/wm/RemoteObject",
                                        "org.enlightenment.wm.Teamwork",
                                        "LinkMouseOut");
#if (ELM_VERSION_MAJOR > 1) || (ELM_VERSION_MINOR > 8) // not a typo
   eldbus_message_arguments_append(msg, "sutii", url, 
                                   time(NULL), win, x, y);
#else
   eldbus_message_arguments_append(msg, "suxii", url, 
                                   time(NULL), (int64_t)win, x, y);
#endif
   eldbus_object_send(_dbus_obj, msg, NULL, NULL, -1);
   eina_stringshare_replace(&_curr_url, NULL);
}

#else
void 
_dbus_init(void)
{

}

void 
_dbus_shutdown(void)
{

}

void 
_dbus_link_hide(void)
{

}

void 
_dbus_link_mousein(uint64_t win EINA_UNUSED, const char *url EINA_UNUSED, int x EINA_UNUSED, int y EINA_UNUSED)
{

}

void 
_dbus_link_mouseout(uint64_t win EINA_UNUSED, const char *url EINA_UNUSED, int x EINA_UNUSED, int y EINA_UNUSED)
{

}
#endif
