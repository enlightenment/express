#include "private.h"
#include "utils.h"
#include "theme.h"
#include "config.h"
#include "media.h"
#include <Ethumb_Client.h>
#include <Emotion.h>

typedef struct _Media Media;
struct _Media
{
   Evas_Object_Smart_Clipped_Data __clipped_data;
   Evas_Object *o_clip;
   Evas_Object *o_img;
   Evas_Object *o_ctrl;
   Evas_Object *o_busy;
   Evas_Object *o_event;
   Evas_Object *o_tmp;

   Ecore_Con_Url *url;
   Ecore_Event_Handler *url_prog_hdlr, *url_compl_hdlr;
   Ethumb_Client_Async *et_req;

   const char *src, *ext, *realf;

   double down_perc;

   int tmpfd;
   int w, h, iw, ih, sw, sh;
   int mode, resizes;
   int frame, frame_num, loops;

   Ecore_Timer *anim_tmr;
   Ecore_Timer *smooth_tmr;

   Ecore_Job *restart_job;

   Media_Type type;

   struct 
     {
        Evas_Coord x, y;
        Eina_Bool down : 1;
     } down;

   Eina_Bool nosmooth : 1;
   Eina_Bool downloading : 1;
   Eina_Bool queued : 1;
   Eina_Bool pos_drag : 1;
};

static Evas_Smart *_smart = NULL;
static Evas_Smart_Class _parent_sc = EVAS_SMART_CLASS_INIT_NULL;
static Ethumb_Client *et_client = NULL;
static Eina_Bool et_connected = EINA_FALSE;
static Eina_List *et_queue = NULL;

static void _smart_calculate(Evas_Object *obj);
static void _thumb_type_init(Evas_Object *obj);
static void _ethumb_init(void);

static void 
_img_cb_preloaded(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;

   if (!(sd = data)) return;
   evas_object_show(sd->o_img);
   evas_object_show(sd->o_clip);
}

static Eina_Bool
_img_cb_frame(void *data)
{
   Media *sd;
   double t;
   int frame;

   if (!(sd = data)) return EINA_FALSE;
   sd->frame++;
   frame = ((sd->frame - 1) % sd->frame_num) + 1;
   if ((sd->frame >= sd->frame_num) && (frame == 1))
     {
        int loops;

        if (evas_object_image_animated_loop_type_get(sd->o_img) ==
            EVAS_IMAGE_ANIMATED_HINT_NONE)
          {
             sd->anim_tmr = NULL;
             return EINA_FALSE;
          }

        sd->loops++;
        loops = evas_object_image_animated_loop_count_get(sd->o_img);
        if (loops != 0)
          {
             if (loops < sd->loops)
               {
                  sd->anim_tmr = NULL;
                  return EINA_FALSE;
               }
          }
     }

   evas_object_image_animated_frame_set(sd->o_img, frame);
   t = evas_object_image_animated_frame_duration_get(sd->o_img, frame, 0);
   ecore_timer_interval_set(sd->anim_tmr, t);

   return EINA_TRUE;
}

static void
_img_type_anim_handle(Evas_Object *obj)
{
   Media *sd;
   double t;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (!evas_object_image_animated_get(sd->o_img)) return;
   sd->frame = 1;
   sd->frame_num = evas_object_image_animated_frame_count_get(sd->o_img);
   if (sd->frame_num < 2) return;
   t = evas_object_image_animated_frame_duration_get(sd->o_img, sd->frame, 0);
   sd->anim_tmr = ecore_timer_add(t, _img_cb_frame, sd);
}

static void 
_img_type_init(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   sd->o_img = evas_object_image_filled_add(evas_object_evas_get(obj));
   evas_object_smart_member_add(sd->o_img, obj);
   evas_object_clip_set(sd->o_img, sd->o_clip);
   evas_object_raise(sd->o_event);
   evas_object_event_callback_add(sd->o_img, EVAS_CALLBACK_IMAGE_PRELOADED,
                                  _img_cb_preloaded, sd);
   evas_object_image_load_orientation_set(sd->o_img, EINA_TRUE);
   evas_object_image_file_set(sd->o_img, sd->realf, NULL);
   evas_object_image_size_get(sd->o_img, &sd->iw, &sd->ih);
   evas_object_image_preload(sd->o_img, EINA_FALSE);

   _img_type_anim_handle(obj);
}

static void 
_img_type_calc(Media *sd, Evas_Coord x, Evas_Coord y, Evas_Coord w, Evas_Coord h)
{
   if (!sd) return;
   if ((w <= 0) || (h <= 0) || (sd->iw <= 0) || (sd->ih <= 0))
     {
        w = 1;
        h = 1;
     }
   else
     {
        int iw = 1, ih = 1;

        if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_BG)
          {
             iw = w;
             ih = (sd->ih * w) / sd->iw;
             if (ih < h)
               {
                  ih = h;
                  iw = (sd->iw * h) / sd->ih;
                  if (iw < w) iw = w;
               }
          }
        else if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_POP)
          {
             iw = w;
             ih = (sd->ih * w) / sd->iw;
             if (ih > h)
               {
                  ih = h;
                  iw = (sd->iw * h) / sd->ih;
                  if (iw > w) iw = w;
               }
             if ((iw > sd->iw) || (ih > sd->ih))
               {
                  iw = sd->iw;
                  ih = sd->ih;
               }
          }
        else if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_STRETCH)
          {
             iw = w;
             ih = h;
          }
        x += ((w - iw) / 2);
        y += ((h - ih) / 2);
        w = iw;
        h = ih;
     }

   evas_object_move(sd->o_img, x, y);
   evas_object_resize(sd->o_img, w, h);
}

static void
_thumb_type_calc(Media *sd, Evas_Coord x, Evas_Coord y, Evas_Coord w, Evas_Coord h)
{
   if (!sd) return;
   if ((w <= 0) || (h <= 0) || (sd->iw <= 0) || (sd->ih <= 0))
     {
        w = 1;
        h = 1;
     }
   else
     {
        int iw = 1, ih = 1;

        iw = w;
        ih = (sd->ih * w) / sd->iw;
        if (ih > h)
          {
             ih = h;
             iw = (sd->iw * h) / sd->ih;
             if (iw > w) iw = w;
          }
        if ((iw > sd->iw) || (ih > sd->ih))
          {
             iw = sd->iw;
             ih = sd->ih;
          }

        x += ((w - iw) / 2);
        y += ((h - ih) / 2);
        w = iw;
        h = ih;
     }

   evas_object_move(sd->o_img, x, y);
   evas_object_resize(sd->o_img, w, h);
}

static void
_thumb_cb_preloaded(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;
   Evas_Coord ox, oy, ow, oh;

   if (!(sd = evas_object_smart_data_get(data))) return;
   evas_object_geometry_get(data, &ox, &oy, &ow, &oh);
   _thumb_type_calc(sd, ox, oy, ow, oh);
   evas_object_show(sd->o_img);
   evas_object_show(sd->o_clip);
}

static void
_ethumb_cb_done(Ethumb_Client *ec EINA_UNUSED, const char *file, const char *key, void *data)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;
   sd->et_req = NULL;
   evas_object_event_callback_add(sd->o_img, EVAS_CALLBACK_IMAGE_PRELOADED,
                                  _thumb_cb_preloaded, data);
   evas_object_image_file_set(sd->o_img, file, key);
   evas_object_image_size_get(sd->o_img, &sd->iw, &sd->ih);
   evas_object_image_preload(sd->o_img, EINA_FALSE);
}

static void
_ethumb_cb_error(Ethumb_Client *ec EINA_UNUSED, void *data)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;
   sd->et_req = NULL;
}

static void
_ethumb_disconnect(void *data EINA_UNUSED, Ethumb_Client *ec)
{
   if (ec != et_client) return;
   ethumb_client_disconnect(et_client);
   et_connected = EINA_FALSE;
   et_client = NULL;
   if (et_queue) _ethumb_init();
}

static void
_ethumb_connect(void *data EINA_UNUSED, Ethumb_Client *ec, Eina_Bool ok)
{
   if (ok)
     {
        Evas_Object *o;

        et_connected = EINA_TRUE;

        ethumb_client_on_server_die_callback_set(ec, _ethumb_disconnect,
                                                 NULL, NULL);

        EINA_LIST_FREE(et_queue, o)
          _thumb_type_init(o);
     }
   else
     et_client = NULL;
}

static void
_ethumb_init(void)
{
   if (et_client) return;
   ethumb_client_init();
   et_client = ethumb_client_connect(_ethumb_connect, NULL, NULL);
}

static void
_thumb_type_theme_init(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   if ((sd->realf) && (sd->realf[0] != '/'))
     {
        static const char *icon_theme = NULL;
        const char *f;

        if (!icon_theme)
          {
             Efreet_Icon_Theme *theme;

             theme = efreet_icon_theme_find(getenv("E_ICON_THEME"));
             if (!theme)
               {
                  const char **itr;
                  static const char *themes[] =
                    {
                       "Human", "oxygen", "gnome", "hicolor", NULL
                    };

                  for (itr = themes; *itr; itr++)
                    {
                       theme = efreet_icon_theme_find(*itr);
                       if (theme) break;
                    }
               }

             if (theme)
               icon_theme = eina_stringshare_add(theme->name.internal);
          }
        f = efreet_icon_path_find(icon_theme, sd->realf, sd->iw);
        ethumb_client_file_set(et_client, f, NULL);
     }
   else
     ethumb_client_file_set(et_client, sd->realf, NULL);

   sd->et_req =
     ethumb_client_thumb_async_get(et_client, _ethumb_cb_done,
                                   _ethumb_cb_error, obj);
   sd->queued = EINA_FALSE;
}

static void
_thumb_type_init(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   sd->type = MEDIA_TYPE_THUMB;

   _ethumb_init();

   sd->o_img = evas_object_image_filled_add(evas_object_evas_get(obj));
   evas_object_image_load_orientation_set(sd->o_img, EINA_TRUE);
   evas_object_smart_member_add(sd->o_img, obj);
   evas_object_clip_set(sd->o_img, sd->o_clip);
   evas_object_raise(sd->o_event);

   sd->iw = sd->ih = 64;

   if (!et_connected)
     {
        et_queue = eina_list_append(et_queue, obj);
        sd->queued = EINA_TRUE;
        return;
     }

   _thumb_type_theme_init(obj);
}

static void
_scale_cb_preloaded(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;

   if (!sd->o_tmp)
     {
        evas_object_show(sd->o_img);
        evas_object_show(sd->o_clip);
     }
   else
     {
        evas_object_del(sd->o_img);
        sd->o_img = sd->o_tmp;
        sd->o_tmp = NULL;
        evas_object_show(sd->o_img);
        evas_object_show(sd->o_clip);
     }
}

static void
_scale_type_calc(Evas_Object *obj, Evas_Coord x, Evas_Coord y, Evas_Coord w, Evas_Coord h)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   if ((w <= 0) || (h <= 0) || (sd->iw <= 0) || (sd->ih <= 0))
     {
        w = 1;
        h = 1;
     }
   else
     {
        int iw = 1, ih = 1;

        if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_BG)
          {
             iw = w;
             ih = (sd->ih * w) / sd->iw;
             if (ih < h)
               {
                  ih = h;
                  iw = (sd->iw * h) / sd->ih;
                  if (iw < w) iw = w;
               }
          }
        else if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_POP)
          {
             iw = w;
             ih = (sd->ih * w) / sd->iw;
             if (ih > h)
               {
                  ih = h;
                  iw = (sd->iw * h) / sd->ih;
                  if (iw > w) iw = w;
               }
             /* if ((iw > sd->iw) || (ih > sd->ih)) */
             /*   { */
             /*      iw = sd->iw; */
             /*      ih = sd->ih; */
             /*   } */
          }
        else if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_STRETCH)
          {
             iw = w;
             ih = h;
          }
        x += ((w - iw) / 2);
        y += ((h - ih) / 2);
        w = iw;
        h = ih;
     }

   if (!sd->nosmooth)
     {
        Evas_Coord sw, sh;

        sw = w;
        sh = h;
        if (sw < 256) sw = 256;
        if (sh < 256) sh = 256;
        if ((sw != sd->sw) || (sh != sd->sh))
          {
             sd->o_tmp =
               evas_object_image_filled_add(evas_object_evas_get(sd->o_event));
             evas_object_event_callback_add(sd->o_tmp,
                                            EVAS_CALLBACK_IMAGE_PRELOADED,
                                            _scale_cb_preloaded, sd);
             evas_object_image_load_orientation_set(sd->o_tmp, EINA_TRUE);
             evas_object_image_file_set(sd->o_tmp, sd->realf, NULL);
             evas_object_image_load_size_set(sd->o_tmp, sw, sh);
             evas_object_image_preload(sd->o_tmp, EINA_FALSE);

             evas_object_smart_member_add(sd->o_tmp, obj);
             evas_object_clip_set(sd->o_tmp, sd->o_clip);
             evas_object_raise(sd->o_event);
          }

        sd->sw = sw;
        sd->sh = sh;
     }

   if (sd->o_tmp)
     {
        evas_object_move(sd->o_tmp, x, y);
        evas_object_resize(sd->o_tmp, w, h);
     }

   evas_object_move(sd->o_img, x, y);
   evas_object_resize(sd->o_img, w, h);
}

static void
_scale_type_init(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   sd->o_img = evas_object_image_filled_add(evas_object_evas_get(obj));
   evas_object_event_callback_add(sd->o_img, EVAS_CALLBACK_IMAGE_PRELOADED,
                                  _scale_cb_preloaded, obj);
   evas_object_image_load_orientation_set(sd->o_img, EINA_TRUE);
   evas_object_image_file_set(sd->o_img, sd->realf, NULL);
   evas_object_image_size_get(sd->o_img, &sd->iw, &sd->ih);
   evas_object_image_preload(sd->o_img, EINA_FALSE);
   evas_object_smart_member_add(sd->o_img, obj);
   evas_object_clip_set(sd->o_img, sd->o_clip);
   evas_object_raise(sd->o_event);
}

static Eina_Bool
_unsmooth_timeout(void *data)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return EINA_FALSE;

   sd->smooth_tmr = NULL;
   sd->nosmooth = EINA_FALSE;

   if ((sd->type == MEDIA_TYPE_IMG) || (sd->type == MEDIA_TYPE_SCALE))
     {
        evas_object_image_smooth_scale_set(sd->o_img, !sd->nosmooth);
        if (sd->o_tmp)
          evas_object_image_smooth_scale_set(sd->o_tmp, !sd->nosmooth);

        if (sd->type == MEDIA_TYPE_SCALE)
          {
             Evas_Coord ox, oy, ow, oh;

             evas_object_geometry_get(data, &ox, &oy, &ow, &oh);
             _scale_type_calc(data, ox, oy, ow, oh);
          }
     }
   else if (sd->type == MEDIA_TYPE_MOV)
     emotion_object_smooth_scale_set(sd->o_img, !sd->nosmooth);

   return EINA_FALSE;
}

static void
_smooth_handler(Evas_Object *obj)
{
   Media *sd;
   double interval;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   interval = ecore_animator_frametime_get();
   if (interval <= 0.0) interval = 1.0 / 60.0;

   if (!sd->nosmooth)
     {
        if (sd->resizes >= 2)
          {
             sd->nosmooth = EINA_TRUE;
             sd->resizes = 0;
             if ((sd->type == MEDIA_TYPE_IMG) || (sd->type == MEDIA_TYPE_SCALE))
               {
                  evas_object_image_smooth_scale_set(sd->o_img, !sd->nosmooth);
                  if (sd->o_tmp)
                    evas_object_image_smooth_scale_set(sd->o_tmp, !sd->nosmooth);
               }
             else if (sd->type == MEDIA_TYPE_MOV)
               emotion_object_smooth_scale_set(sd->o_img, !sd->nosmooth);
          }
     }

   if (sd->smooth_tmr) ecore_timer_del(sd->smooth_tmr);
   sd->smooth_tmr =
     ecore_timer_add(interval * 10, _unsmooth_timeout, obj);
}

static void 
_mov_type_calc(Media *sd, Evas_Coord x, Evas_Coord y, Evas_Coord w, Evas_Coord h)
{
   if (!sd) return;

   evas_object_move(sd->o_ctrl, x, y);
   evas_object_resize(sd->o_ctrl, w, h);

   emotion_object_size_get(sd->o_img, &(sd->iw), &(sd->ih));
   if ((w <= 0) || (h <= 0) || (sd->iw <= 0) || (sd->ih <= 0))
     {
        w = 1;
        h = 1;
     }
   else
     {
        int iw = 1, ih = 1;
        double ratio;

        ratio = emotion_object_ratio_get(sd->o_img);
        if (ratio > 0.0) sd->iw = (sd->ih * ratio) + 0.5;
        else ratio = (double)sd->iw / (double)sd->ih;

        if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_BG)
          {
             iw = w;
             ih = w / ratio;
             if (ih < h)
               {
                  ih = h;
                  iw = h * ratio;
                  if (iw < w) iw = w;
               }
          }
        else if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_POP)
          {
             iw = w;
             ih = w / ratio;
             if (ih > h)
               {
                  ih = h;
                  iw = h * ratio;
                  if (iw > w) iw = w;
               }
          }
        else if ((sd->mode & MEDIA_SIZE_MASK) == MEDIA_STRETCH)
          {
             iw = w;
             ih = h;
          }
        x += ((w - iw) / 2);
        y += ((h - ih) / 2);
        w = iw;
        h = ih;
     }

   evas_object_move(sd->o_img, x, y);
   evas_object_resize(sd->o_img, w, h);
}

static void
_mov_cb_frame_decode(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;
   Evas_Coord x, y, w, h;
   double len, pos;

   if (!(sd = evas_object_smart_data_get(data))) return;

   evas_object_geometry_get(data, &x, &y, &w, &h);
   evas_object_show(sd->o_img);
   evas_object_show(sd->o_clip);

   _mov_type_calc(sd, x, y, w, h);

   if (sd->pos_drag) return;

   len = emotion_object_play_length_get(sd->o_img);
   pos = emotion_object_position_get(sd->o_img);
   pos /= len;

   edje_object_part_drag_value_set(sd->o_ctrl, "express.posdrag", pos, pos);
}

static void
_mov_cb_frame_resize(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;
   Evas_Coord x, y, w, h;

   if (!(sd = evas_object_smart_data_get(data))) return;
   evas_object_geometry_get(data, &x, &y, &w, &h);
   _mov_type_calc(sd, x, y, w, h);
}

static void
_mov_cb_restart(void *data)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;
   sd->restart_job = NULL;
   emotion_object_position_set(sd->o_img, 0.0);
   emotion_object_play_set(sd->o_img, EINA_TRUE);
}

static void
_mov_cb_decode_stop(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;
   if (sd->restart_job) ecore_job_del(sd->restart_job);
   sd->restart_job = ecore_job_add(_mov_cb_restart, data);
   evas_object_smart_callback_call(data, "loop", NULL);
}

static void
_mov_cb_progress(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;
   DBG("Movie Progress: '%s' '%3.3f",
       emotion_object_progress_info_get(sd->o_img),
       emotion_object_progress_status_get(sd->o_img));
}

static void
_mov_cb_ref(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(data))) return;
   DBG("Movie Ref: '%s' num '%i'",
       emotion_object_ref_file_get(sd->o_img),
       emotion_object_ref_num_get(sd->o_img));
}

static void
_mov_cb_play(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   _media_play_set(data, EINA_TRUE);
}

static void
_mov_cb_pause(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   _media_play_set(data, EINA_FALSE);
}

static void
_mov_cb_stop(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   _media_stop(data);
}

static void
_mov_cb_vol(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   Media *sd;
   double vx, vy;

   if (!(sd = evas_object_smart_data_get(data))) return;

   edje_object_part_drag_value_get(sd->o_ctrl, "express.voldrag", &vx, &vy);
   _media_volume_set(data, vx + vy);
}

static void
_mov_cb_pos_drag_start(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   Media *sd;
   double vx, vy;

   if (!(sd = evas_object_smart_data_get(data))) return;

   sd->pos_drag = EINA_TRUE;
   edje_object_part_drag_value_get(sd->o_ctrl, "express.posdrag", &vx, &vy);
   _media_position_set(data, vx + vy);
}

static void
_mov_cb_pos_drag_stop(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   Media *sd;
   double pos, len;

   if (!(sd = evas_object_smart_data_get(data))) return;
   if (!sd->pos_drag) return;

   sd->pos_drag = EINA_FALSE;
   len = emotion_object_play_length_get(sd->o_img);
   pos = emotion_object_position_get(sd->o_img);
   pos /= len;
   edje_object_part_drag_value_set(sd->o_ctrl, "express.posdrag", pos, pos);
}

static void
_mov_cb_pos(void *data, Evas_Object *obj EINA_UNUSED, const char *emission EINA_UNUSED, const char *src EINA_UNUSED)
{
   Media *sd;
   double vx, vy;

   if (!(sd = evas_object_smart_data_get(data))) return;

   edje_object_part_drag_value_get(sd->o_ctrl, "express.posdrag", &vx, &vy);
   _media_position_set(data, vx + vy);
}

static void
_mov_type_init(Evas_Object *obj)
{
   Media *sd;
   double vol;
   char *modules[] = { NULL, "gstreamer", "xine", "vlc", "gstreamer1" };
   char *mod = NULL;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   emotion_init();

   sd->o_img = emotion_object_add(evas_object_evas_get(obj));

   if ((_ex_cfg->video.module >= 0) &&
       (_ex_cfg->video.module < (int)EINA_C_ARRAY_LENGTH(modules)))
     mod = modules[_ex_cfg->video.module];

   if (!emotion_object_init(sd->o_img, mod))
     {
        ERR("Could not initialize emotion module: %s", mod);
        evas_object_del(sd->o_img);
        sd->o_img = NULL;
        return;
     }

   evas_object_smart_callback_add(sd->o_img, "frame_decode",
                                  _mov_cb_frame_decode, obj);
   evas_object_smart_callback_add(sd->o_img, "frame_resize",
                                  _mov_cb_frame_resize, obj);
   evas_object_smart_callback_add(sd->o_img, "decode_stop",
                                  _mov_cb_decode_stop, obj);
   evas_object_smart_callback_add(sd->o_img, "progress_change",
                                  _mov_cb_progress, obj);
   evas_object_smart_callback_add(sd->o_img, "ref_change",
                                  _mov_cb_ref, obj);

   emotion_object_file_set(sd->o_img, sd->realf);
   if (((sd->mode & MEDIA_OPTIONS_MASK) & MEDIA_RECOVER)
       && (sd->type == MEDIA_TYPE_MOV) && (sd->o_img))
     emotion_object_last_position_load(sd->o_img);
   else
     _media_position_set(obj, 0.0);

   evas_object_smart_member_add(sd->o_img, obj);
   evas_object_clip_set(sd->o_img, sd->o_clip);
   evas_object_raise(sd->o_event);

   sd->o_ctrl = edje_object_add(evas_object_evas_get(obj));
   _theme_apply(sd->o_ctrl, "express/mediactrl");
   vol = emotion_object_audio_volume_get(sd->o_img);
   edje_object_part_drag_value_set(sd->o_ctrl, "express.voldrag", vol, vol);

   edje_object_signal_callback_add(sd->o_ctrl, "play", "", _mov_cb_play, obj);
   edje_object_signal_callback_add(sd->o_ctrl, "pause", "", _mov_cb_pause, obj);
   edje_object_signal_callback_add(sd->o_ctrl, "stop", "", _mov_cb_stop, obj);
   edje_object_signal_callback_add(sd->o_ctrl, "pos,drag,start", "",
                                   _mov_cb_pos_drag_start, obj);
   edje_object_signal_callback_add(sd->o_ctrl, "pos,drag,stop", "",
                                   _mov_cb_pos_drag_stop, obj);
   edje_object_signal_callback_add(sd->o_ctrl, "pos,drag", "",
                                   _mov_cb_pos, obj);
   edje_object_signal_callback_add(sd->o_ctrl, "vol,drag", "",
                                   _mov_cb_vol, obj);

   evas_object_clip_set(sd->o_ctrl, sd->o_clip);
   evas_object_show(sd->o_ctrl);

   _media_play_set(obj, EINA_TRUE);
   if (_ex_cfg->video.muted) _media_mute_set(obj, EINA_TRUE);
   if (_ex_cfg->video.visualized) _media_visualize_set(obj, EINA_TRUE);
}

static void 
_media_cb_mouse_down(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Media *sd;
   Evas_Event_Mouse_Down *ev;

   ev = event;
   if (!(sd = evas_object_smart_data_get(data))) return;
   if (sd->down.down) return;
   if (ev->button != 1) return;
   sd->down.x = ev->canvas.x;
   sd->down.y = ev->canvas.y;
   sd->down.down = EINA_TRUE;
}

static void 
_media_cb_mouse_up(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Media *sd;
   Evas_Coord dx, dy;
   Evas_Event_Mouse_Up *ev;

   ev = event;
   if (!(sd = evas_object_smart_data_get(data))) return;
   if (!sd->down.down) return;
   sd->down.down = EINA_FALSE;
   dx = abs(ev->canvas.x - sd->down.x);
   dy = abs(ev->canvas.y - sd->down.y);
   if ((dx <= elm_config_finger_size_get()) && 
       (dy <= elm_config_finger_size_get()))
     evas_object_smart_callback_call(data, "clicked", NULL);
}

static Eina_Bool 
_media_cb_prog(void *data, int type EINA_UNUSED, void *event)
{
   Media *sd;
   Evas_Object *obj;
   Ecore_Con_Event_Url_Progress *ev;

   if (!(obj = data)) return EINA_TRUE;
   if (!(ev = event)) return EINA_TRUE;
   if (!(sd = evas_object_smart_data_get(obj))) return EINA_TRUE;
   if (ev->url_con != sd->url) return EINA_TRUE;

   if (ev->down.total > 0.0)
     {
        double perc;

        if (!sd->downloading)
          edje_object_signal_emit(sd->o_busy, "downloading", PACKAGE_NAME);
        sd->downloading = EINA_TRUE;
        perc = (ev->down.now / ev->down.total);
        if (perc != sd->down_perc)
          {
             Edje_Message_Float msg;

             sd->down_perc = perc;
             msg.val = perc;
             edje_object_message_send(sd->o_busy, EDJE_MESSAGE_FLOAT, 1, &msg);
          }
     }

   return EINA_FALSE;
}

static Eina_Bool 
_media_cb_compl(void *data, int type EINA_UNUSED, void *event)
{
   Media *sd;
   Evas_Object *obj;
   Ecore_Con_Event_Url_Complete *ev;

   if (!(obj = data)) return EINA_TRUE;
   if (!(ev = event)) return EINA_TRUE;
   if (!(sd = evas_object_smart_data_get(obj))) return EINA_TRUE;
   if (ev->url_con != sd->url) return EINA_TRUE;

   edje_object_signal_emit(sd->o_busy, "done", PACKAGE_NAME);
   ecore_event_handler_del(sd->url_prog_hdlr);
   ecore_event_handler_del(sd->url_compl_hdlr);
   ecore_con_url_free(sd->url);
   sd->url_prog_hdlr = NULL;
   sd->url_compl_hdlr = NULL;
   sd->url = NULL;

   switch (sd->type)
     {
      case MEDIA_TYPE_IMG:
        _img_type_init(obj);
        break;
      case MEDIA_TYPE_SCALE:
        _scale_type_init(obj);
        break;
      /* case MEDIA_TYPE_EDJE: */
      /*   _edje_type_init(obj); */
      /*   break; */
      case MEDIA_TYPE_MOV:
        _mov_type_init(obj);
        break;
      default:
        break;
     }

   evas_object_raise(sd->o_busy);
   evas_object_raise(sd->o_event);

   _smart_calculate(obj);

   return EINA_FALSE;
}

static void 
_smart_add(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = calloc(1, sizeof(Media)))) return;
   evas_object_smart_data_set(obj, sd);

   _parent_sc.add(obj);

   sd->o_clip = evas_object_rectangle_add(evas_object_evas_get(obj));
   evas_object_color_set(sd->o_clip, 255, 255, 255, 255);
   evas_object_smart_member_add(sd->o_clip, obj);
}

static void 
_smart_del(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (((sd->mode & MEDIA_OPTIONS_MASK) & MEDIA_SAVE) && 
       (sd->type == MEDIA_TYPE_MOV) && (sd->o_img))
     emotion_object_last_position_save(sd->o_img);
   if (sd->url)
     {
        ecore_event_handler_del(sd->url_prog_hdlr);
        ecore_event_handler_del(sd->url_compl_hdlr);
        ecore_con_url_free(sd->url);
     }

   sd->url_prog_hdlr = NULL;
   sd->url_compl_hdlr = NULL;
   sd->url = NULL;

   if (sd->tmpfd >= 0)
     {
        if (sd->realf) unlink(sd->realf);
        close(sd->tmpfd);
     }

   if (sd->src) eina_stringshare_del(sd->src);
   if (sd->realf) eina_stringshare_del(sd->realf);

   if (sd->o_clip) evas_object_del(sd->o_clip);
   if (sd->o_img) evas_object_del(sd->o_img);
   if (sd->o_ctrl) evas_object_del(sd->o_ctrl);
   if (sd->o_busy) evas_object_del(sd->o_busy);
   if (sd->o_event) evas_object_del(sd->o_event);
   if (sd->o_tmp) evas_object_del(sd->o_tmp);

   if (sd->anim_tmr) ecore_timer_del(sd->anim_tmr);
   if (sd->smooth_tmr) ecore_timer_del(sd->smooth_tmr);

   if (sd->restart_job) ecore_job_del(sd->restart_job);

   if ((et_client) && (sd->et_req))
     ethumb_client_thumb_async_cancel(et_client, sd->et_req);
   if (sd->queued) et_queue = eina_list_remove(et_queue, obj);
   sd->et_req = NULL;

   _parent_sc.del(obj);
}

static void 
_smart_resize(Evas_Object *obj, Evas_Coord w, Evas_Coord h)
{
   Media *sd;
   Evas_Coord ow, oh;

   if (!(sd = evas_object_smart_data_get(obj))) return;

   evas_object_geometry_get(obj, NULL, NULL, &ow, &oh);
   if ((ow == w) && (oh == h)) return;
   evas_object_smart_changed(obj);
   evas_object_resize(sd->o_clip, ow, oh);
}

static void 
_smart_calculate(Evas_Object *obj)
{
   Media *sd;
   Evas_Coord ox, oy, ow, oh;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   evas_object_geometry_get(obj, &ox, &oy, &ow, &oh);
   if ((ow != sd->w) || (oh != sd->h)) sd->resizes++;
   else sd->resizes = 0;

   _smooth_handler(obj);

   sd->w = ow;
   sd->h = oh;
   switch (sd->type)
     {
      case MEDIA_TYPE_IMG:
        _img_type_calc(sd, ox, oy, ow, oh);
        break;
      case MEDIA_TYPE_SCALE:
        _scale_type_calc(obj, ox, oy, ow, oh);
        break;
      /* case MEDIA_TYPE_EDJE: */
      /*   _edje_type_calc(sd, ox, oy, ow, oh); */
      /*   break; */
      case MEDIA_TYPE_MOV:
        _mov_type_calc(sd, ox, oy, ow, oh);
        break;
      case MEDIA_TYPE_THUMB:
        _thumb_type_calc(sd, ox, oy, ow, oh);
        break;
      case MEDIA_TYPE_UNKNOWN:
        return;
      default:
        break;
     }
   evas_object_move(sd->o_clip, ox, oy);
   evas_object_resize(sd->o_clip, ow, oh);
   if (sd->o_busy)
     {
        evas_object_move(sd->o_busy, ox, oy);
        evas_object_resize(sd->o_busy, ow, oh);
     }
   if (sd->o_event)
     {
        evas_object_move(sd->o_event, ox, oy);
        evas_object_resize(sd->o_event, ow, oh);
     }
}

static void 
_smart_move(Evas_Object *obj, Evas_Coord x EINA_UNUSED, Evas_Coord y EINA_UNUSED)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   evas_object_smart_changed(obj);
}

static void 
_smart_init(void)
{
   static Evas_Smart_Class sc;

   evas_object_smart_clipped_smart_set(&_parent_sc);

   sc = _parent_sc;
   sc.name = "media";
   sc.version = EVAS_SMART_CLASS_VERSION;
   sc.add = _smart_add;
   sc.del = _smart_del;
   sc.resize = _smart_resize;
   sc.move = _smart_move;
   sc.calculate = _smart_calculate;

   _smart = evas_smart_class_new(&sc);
}

Evas_Object *
_media_add(Evas_Object *parent, const char *src, int mode, Media_Type type)
{
   Evas *evas;
   Evas_Object *obj = NULL;
   Media *sd = NULL;

   if (!parent) return NULL;

   if (!(evas = evas_object_evas_get(parent))) return NULL;

   if (!_smart) _smart_init();

   obj = evas_object_smart_add(evas, _smart);
   if (!(sd = evas_object_smart_data_get(obj))) return obj;

   sd->src = eina_stringshare_add(src);
   sd->mode = mode;
   sd->type = type;
   sd->tmpfd = -1;

#if HAVE_MKSTEMPS
   if (_util_link_is_url(sd->src))// && (type != MEDIA_TYPE_MOV))
     {
        const char *ext = NULL;
        char *buff;

        if (!strncasecmp(sd->src, "www.", 4))
          {
             buff = alloca(strlen(sd->src) + 10);
             strcpy(buff, "http://");
             strcat(buff, sd->src);
          }
        else if (!strncasecmp(sd->src, "ftp.", 4))
          {
             buff = alloca(strlen(sd->src) + 10);
             strcpy(buff, "ftp://");
             strcat(buff, sd->src);
          }
        else
          buff = (char *)sd->src;

        if ((ext = _util_file_extension_get(src, extn_img)))
          sd->ext = ext;
        else if ((ext = _util_file_extension_get(src, extn_scale)))
          sd->ext = ext;
        else if ((ext = _util_file_extension_get(src, extn_edj)))
          sd->ext = ext;
        else if ((ext = _util_file_extension_get(src, extn_mov)))
          sd->ext = ext;
        else
          {
             switch (type)
               {
                case MEDIA_TYPE_IMG:
                  sd->ext = ".png";
                  break;
                case MEDIA_TYPE_SCALE:
                  sd->ext = ".svg";
                  break;
                case MEDIA_TYPE_EDJE:
                  sd->ext = ".edj";
                  break;
                case MEDIA_TYPE_MOV:
                case MEDIA_TYPE_THUMB:
                case MEDIA_TYPE_UNKNOWN:
                  break;
               }
          }

        if (sd->ext)
          {
             Eina_Tmpstr *path;
             char tmp[PATH_MAX];

             snprintf(tmp, sizeof(tmp), "express-media-XXXXXX%s", sd->ext);
             sd->tmpfd = eina_file_mkstemp(tmp, &path);
             if (sd->tmpfd >= 0)
               {
                  if (!(sd->url = ecore_con_url_new(buff)))
                    {
                       ERR("Failed to connect to url: %s", buff);
                       unlink(tmp);
                       close(sd->tmpfd);
                    }
                  else
                    {
                       ecore_con_url_fd_set(sd->url, sd->tmpfd);
                       if (!ecore_con_url_get(sd->url))
                         {
                            unlink(tmp);
                            close(sd->tmpfd);
                            sd->url = NULL;
                         }
                       else
                         {
                            sd->o_busy =
                              edje_object_add(evas_object_evas_get(obj));
                            evas_object_smart_member_add(sd->o_busy, obj);
                            _theme_apply(sd->o_busy, "express/mediabusy");
                            evas_object_show(sd->o_busy);
                            edje_object_signal_emit(sd->o_busy, "busy", 
                                                    PACKAGE_NAME);

                            sd->realf = eina_stringshare_add(path);
                            sd->url_prog_hdlr =
                              ecore_event_handler_add(ECORE_CON_EVENT_URL_PROGRESS,
                                                      _media_cb_prog, obj);
                            sd->url_compl_hdlr =
                              ecore_event_handler_add(ECORE_CON_EVENT_URL_COMPLETE,
                                                      _media_cb_compl, obj);
                         }
                    }

                  eina_tmpstr_del(path);
               }
             else
               ERR("Eina Mkstemp Failed For File: %s", tmp);
          }
     }
#endif

#if (ELM_VERSION_MAJOR == 1) && (ELM_VERSION_MINOR < 13)
   if (!sd->url)
     {
        Efreet_Uri *uri;
        const char *file;

        file = eina_stringshare_printf("file://%s", sd->src);
        uri = efreet_uri_decode(file);
        eina_stringshare_del(file);
        if (!uri) goto err;
        sd->realf = uri->path;
        eina_stringshare_ref(sd->realf);
        efreet_uri_free(uri);
     }
#else
   if (!sd->url) sd->realf = eina_stringshare_add(sd->src);
#endif

   if ((mode & MEDIA_SIZE_MASK) == MEDIA_THUMB)
     _thumb_type_init(obj);
   else
     {
        switch (type)
          {
           case MEDIA_TYPE_IMG:
             if (!sd->url) _img_type_init(obj);
             break;
           case MEDIA_TYPE_SCALE:
             if (!sd->url) _scale_type_init(obj);
             break;
           /* case MEDIA_TYPE_EDJE: */
           /*   break; */
           case MEDIA_TYPE_MOV:
             if (!sd->url) _mov_type_init(obj);
             break;
           default:
             break;
          }
     }

   sd->o_event = evas_object_rectangle_add(evas);
   evas_object_color_set(sd->o_event, 0, 0, 0, 0);
   evas_object_repeat_events_set(sd->o_event, EINA_TRUE);
   evas_object_smart_member_add(sd->o_event, obj);
   evas_object_clip_set(sd->o_event, sd->o_clip);
   evas_object_show(sd->o_event);

   evas_object_event_callback_add(sd->o_event, EVAS_CALLBACK_MOUSE_DOWN,
                                  _media_cb_mouse_down, obj);
   evas_object_event_callback_add(sd->o_event, EVAS_CALLBACK_MOUSE_UP,
                                  _media_cb_mouse_up, obj);

   return obj;

#if (ELM_VERSION_MAJOR == 1) && (ELM_VERSION_MINOR < 13)
err:
   if (obj) evas_object_del(obj);
   return NULL;
#endif
}

void 
_media_mute_set(Evas_Object *obj, Eina_Bool mute)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (sd->type != MEDIA_TYPE_MOV) return;
   emotion_object_audio_mute_set(sd->o_img, mute);
   if (mute)
     edje_object_signal_emit(sd->o_ctrl, "mute,set", PACKAGE_NAME);
   else
     edje_object_signal_emit(sd->o_ctrl, "mute,unset", PACKAGE_NAME);
}

void 
_media_play_set(Evas_Object *obj, Eina_Bool play)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (sd->type != MEDIA_TYPE_MOV) return;
   emotion_object_play_set(sd->o_img, play);
   if (play)
     {
        evas_object_smart_callback_call(obj, "play", NULL);
        edje_object_signal_emit(sd->o_ctrl, "play,set", PACKAGE_NAME);
     }
   else
     {
        evas_object_smart_callback_call(obj, "pause", NULL);
        edje_object_signal_emit(sd->o_ctrl, "pause,set", PACKAGE_NAME);
     }
}

Eina_Bool 
_media_play_get(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return EINA_FALSE;
   if (sd->type != MEDIA_TYPE_MOV) return EINA_FALSE;
   return emotion_object_play_get(sd->o_img);
}

void 
_media_position_set(Evas_Object *obj, double pos)
{
   Media *sd;
   double len = 0.0;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (sd->type != MEDIA_TYPE_MOV) return;
   len = emotion_object_play_length_get(sd->o_img);
   emotion_object_position_set(sd->o_img, len * pos);
}

void 
_media_volume_set(Evas_Object *obj, double vol)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (sd->type != MEDIA_TYPE_MOV) return;
   emotion_object_audio_volume_set(sd->o_img, vol);
   edje_object_part_drag_value_set(sd->o_ctrl, "express.voldrag", vol, vol);
}

void 
_media_visualize_set(Evas_Object *obj, Eina_Bool visualize)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (sd->type != MEDIA_TYPE_MOV) return;
   if (visualize)
     {
        if (emotion_object_vis_supported(sd->o_img, 
                                         EMOTION_VIS_LIBVISUAL_INFINITE))
          emotion_object_vis_set(sd->o_img, EMOTION_VIS_LIBVISUAL_INFINITE);
     }
   else
     emotion_object_vis_set(sd->o_img, EMOTION_VIS_NONE);
}

void 
_media_stop(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return;
   if (sd->type != MEDIA_TYPE_MOV) return;
   evas_object_smart_callback_call(obj, "stop", NULL);
   evas_object_del(obj);
}

const char *
_media_get(const Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return NULL;
   return sd->realf;
}

Media_Type 
_media_src_type_get(const char *src)
{
   Media_Type type = MEDIA_TYPE_UNKNOWN;

   if (_util_file_extension_get(src, extn_img))
     type = MEDIA_TYPE_IMG;
   else if (_util_file_extension_get(src, extn_scale))
     type = MEDIA_TYPE_SCALE;
   else if (_util_file_extension_get(src, extn_edj))
     type = MEDIA_TYPE_EDJE;
   else if (_util_file_extension_get(src, extn_mov))
     type = MEDIA_TYPE_MOV;
   return type;
}

Evas_Object *
_media_control_get(Evas_Object *obj)
{
   Media *sd;

   if (!(sd = evas_object_smart_data_get(obj))) return NULL;
   return sd->o_ctrl;
}

void 
_media_unknown_handle(const char *handler, const char *src)
{
   const char *cmd;
   char buff[PATH_MAX];
   char *esc;

   cmd = "xdg-open";
   if (!(esc = ecore_file_escape_name(src))) return;
   if ((handler) && (*handler)) cmd = handler;
   snprintf(buff, sizeof(buff), "%s %s", cmd, esc);
   free(esc);
   ecore_exe_run(buff, NULL);
}
