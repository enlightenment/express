#ifndef _SELECTOR_H_
# define _SELECTOR_H_ 1

Evas_Object *_selector_create(Evas *evas);
void _selector_channel_add(Evas_Object *obj, Channel *chl, Eina_Bool selected);
void _selector_channel_selected_set(Evas_Object *obj, Channel *chl, Eina_Bool keep);
void _selector_zoom_set(Evas_Object *obj, double zoom);
void _selector_zoom(Evas_Object *obj, double zoom);
void _selector_go(Evas_Object *obj);
void _selector_exit(Evas_Object *obj);

#endif
