#ifndef _UTILS_H_
# define _UTILS_H_ 1

extern const char *extn_img[];
extern const char *extn_scale[];
extern const char *extn_edj[];
extern const char *extn_mov[];
extern const char *extn_aud[];

const char *_util_user_dir_get(void);
char *_util_user_name_get(void);
char *_util_user_fullname_get(void);
Eina_Unicode _util_charset_translate(Eina_Unicode g, void *state);
Eina_Bool _util_dblwidth_slow_get(void *data, int g);
int _util_codepoint_to_utf8(Eina_Unicode g, char *txt);
Eina_Bool _util_link_is_url(const char *str);
Eina_Bool _util_link_is_email(const char *str);
Eina_Bool _util_link_is_protocol(const char *str);
Eina_Bool _util_is_file(const char *str);
char *_util_local_path_get(const Evas_Object *obj, const char *relpath);
Eina_Bool _util_str_has_prefix(const char *str, const char *prefix);
const char *_util_file_extension_get(const char *f, const char **extension);

#if defined(SUPPORT_DBLWIDTH)
static inline Eina_Bool
_util_dblwidth_get(void *sd, int g)
#else
static inline Eina_Bool
_util_dblwidth_get(void *sd EINA_UNUSED, int g EINA_UNUSED)
#endif
{
#if defined(SUPPORT_DBLWIDTH)
   // check for east asian full-width (F), half-width (H), wide (W),
   // narrow (Na) or ambiguous (A) codepoints
   // ftp://ftp.unicode.org/Public/UNIDATA/EastAsianWidth.txt

   // optimize for latin1 non-ambiguous
   if (g <= 0xa0)
     return EINA_FALSE;
   // (F)
   if ((g == 0x3000) ||
       ((g >= 0xff01) && (g <= 0xff60)) ||
       ((g >= 0xffe0) && (g <= 0xffe6)))
     return EINA_TRUE;

   return _util_dblwidth_slow_get(ty, g);
#else
   return EINA_FALSE;
#endif
}

#endif
