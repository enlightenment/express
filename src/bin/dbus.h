#ifndef _DBUS_H_
# define _DBUS_H_ 1

void _dbus_init(void);
void _dbus_shutdown(void);
void _dbus_link_hide(void);
void _dbus_link_mousein(uint64_t win, const char *url, int x, int y);
void _dbus_link_mouseout(uint64_t win, const char *url, int x, int y);

#endif
