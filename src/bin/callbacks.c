#include "private.h"
#include "callbacks.h"
#include "window.h"
#include "channel.h"
#include <time.h>

#define COLOR_SYSTEM 76
#define COLOR_TEXT   65
#define COLOR_SERVER_NOTICE 14
#define COLOR_CHANNEL_NOTICE 18
#define COLOR_TOPIC  16
#define COLOR_PART   74
#define COLOR_JOIN   75
#define COLOR_NICK_CHANGE 78

static int 
_find_crlf(const char *data, int length, int *lf)
{
   int i = 0;

   *lf = 0;
   for (; i < length; i++)
     {
        /* find crlf (\r\n) */
        if ((data[i] == 0x0D) && 
            (i < (length - 1)) && (data[i + 1] == 0x0A))
          {
             *lf = 2;
             return i;
          }

        /* find just lf */
        if (data[i] == 0x0A) 
          {
             *lf = 1;
             return i;
          }
     }

   return -1;
}

void 
_callback_server_connected(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params EINA_UNUSED, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   DBG("Server %s Connected", source);
}

void 
_callback_server_motd(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count, void *data EINA_UNUSED)
{
   Channel *chl;
   unsigned int i = 0;

   /* DBG("Server Motd"); */
   /* DBG("\tServer: %s", source); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", params[0]); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   if (!(chl = _window_channel_find("Status"))) return;
   for (i = 1; i < count; i++)
     _channel_text_append(chl, NULL, params[i],
                          _row_color_simple_create(COLOR_SYSTEM));
}

void 
_callback_server_error(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   if (!strncasecmp(params[0], "Closing Link", 12))
     express_network_disconnect(net);
}

void
_callback_server_notice(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl;

   /* DBG("Server Notice"); */
   /* DBG("\tServer: %s", source); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", params[0]); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   if (!(chl = _window_channel_active_get())) return;
   _channel_text_append(chl, NULL, params[1], 
                        _row_color_simple_create(COLOR_SERVER_NOTICE));
}

void
_callback_channel_message(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;

   /* DBG("Channel Message: %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   if (!(chl = _window_channel_find(params[0]))) return;
   _channel_text_append(chl, source, params[1], 
                        _row_color_simple_create(COLOR_TEXT));
}

void 
_callback_channel_notice(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;

   /* DBG("Channel Notice: %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tServer: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   if (!strcmp(params[0], "*"))
     {
        if (!(chl = _window_channel_find("Status")))
          {
             ERR("Could not find channel with server name: %s", source);
             chl = _window_channel_active_get();
          }
     }
   else
     {
        if (!(chl = _window_channel_find(params[0])))
          chl = _window_channel_active_get();
     }

   if (!chl) return;
   _channel_text_append(chl, NULL, params[1], 
                        _row_color_simple_create(COLOR_CHANNEL_NOTICE));
}

void 
_callback_channel_topic(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;
   char buff[PATH_MAX];

   /* DBG("Channel Topic: %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */
   /* DBG("\t%s", params[2]); */

   if (!(chl = _window_channel_find(params[1]))) return;
   snprintf(buff, sizeof(buff), "Topic for %s is: ", params[1]);
   _channel_text_append(chl, NULL, buff, 
                        _row_color_simple_create(COLOR_TOPIC));
   _channel_text_append(chl, NULL, params[2], 
                        _row_color_simple_create(COLOR_TOPIC));
}

void 
_callback_channel_topic_time(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;
   char buff[PATH_MAX], *strtime;
   time_t stamp;

   /* DBG("Channel Topic Time: %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */
   /* DBG("\t%s", params[2]); */
   /* DBG("\t%s", params[3]); */

   stamp = atol(params[3]);
   strtime = ctime(&stamp);
   strtime[24] = 0;

   if (!(chl = _window_channel_find(params[1]))) return;
   snprintf(buff, sizeof(buff), "Topic for %s set by %s at %s\r\n", 
            params[1], params[2], strtime);
   _channel_text_append(chl, NULL, buff, 
                        _row_color_simple_create(COLOR_TOPIC));
}

void 
_callback_channel_names(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;
   char **users;
   unsigned int n = 0, i = 0;
   int crlf = 0, lf = 0;

   /* DBG("Channel Names: %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */
   /* DBG("\t%s", params[2]); */
   /* DBG("\t%s", params[3]); */

   if (!(chl = _window_channel_find(params[2]))) return;

   if (strlen(params[3]) > 0)
     _channel_userlist_show(chl);
   else
     _channel_userlist_hide(chl);

   users = eina_str_split_full(params[3], " ", -1, &n);
   for (; i < n; i++)
     {
        char buff[PATH_MAX];

        crlf = _find_crlf(users[i], strlen(users[i]), &lf);
        if (crlf > 0)
          eina_strlcpy(buff, users[i], crlf + lf);
        else
          eina_strlcpy(buff, users[i], strlen(users[i]) + 1);

        if (buff[0] == '@')
          _channel_userlist_user_append(chl, buff + 1, EINA_TRUE);
        else
          _channel_userlist_user_append(chl, buff, EINA_FALSE);
     }

   _channel_userlist_go(chl);

   free(users);
}

void 
_callback_command(Express_Network *net EINA_UNUSED, const char *event, const char *source EINA_UNUSED, const char **params, unsigned int count, void *data EINA_UNUSED)
{
   Channel *chl = NULL;
   char buff[PATH_MAX];
   unsigned int i;

   if (!(chl = _window_channel_active_get())) return;

   if (!strncmp(event, "AWAY", 4))
     {
        snprintf(buff, sizeof(buff), "%s", params[1]);
        _channel_text_append(chl, NULL, params[1],
                             _row_color_simple_create(COLOR_TEXT));
     }
   else if (!strncmp(event, "HELP", 4))
     {
        for (i = 2; i < count; i++)
          _channel_text_append(chl, NULL, params[i],
                               _row_color_simple_create(COLOR_SYSTEM));
     }
}

void
_callback_user_quit(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Eina_List *channels, *l;
   Channel *chl;
   char buff[PATH_MAX];

   /* DBG("User Quit"); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[0]); */

   snprintf(buff, sizeof(buff), "%s has quit: %s", source, params[0]);

   channels = _window_channels_user_find(source);
   EINA_LIST_FOREACH(channels, l, chl)
     _channel_text_append(chl, "*", buff, 
                          _row_color_simple_create(COLOR_PART));
}

void 
_callback_user_part(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;
   char buff[PATH_MAX];

   /* DBG("User Part %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   if (!(chl = _window_channel_find(params[0]))) return;

   snprintf(buff, sizeof(buff), "%s has left %s: %s", 
            source, params[0], params[1]);
   _channel_text_append(chl, NULL, buff, _row_color_simple_create(COLOR_PART));
}

void 
_callback_user_join(Express_Network *net, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel *chl = NULL;
   char buff[PATH_MAX + 100], channel[PATH_MAX];
   int crlf = 0, lf = 0;

   crlf = _find_crlf(params[0], strlen(params[0]), &lf);
   if (crlf > 0)
     eina_strlcpy(channel, params[0], crlf + lf - 1);
   else
     eina_strlcpy(channel, params[0], strlen(params[0]) + 1);

   /* DBG("User Join %s", channel); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   /* find the channel */
   if (!(chl = _window_channel_find(channel)))
     {
        Express_Server *srv;

        if ((srv = express_network_server_connected_get(net)))
          {
             const char *srv_name = NULL;

             srv_name = express_network_server_realname_get(srv);
             chl = _window_channel_create(net, channel, srv_name);
             if (!chl) return;
          }
     }

   /* skip user join messages for our own nick */
   if (!strcmp(source, express_network_nickname_get(net)))
     snprintf(buff, sizeof(buff), "Now talking on %s\r\n", channel);
   else
     snprintf(buff, sizeof(buff), "%s has joined %s\r\n", source, channel);

   _channel_text_append(chl, "*", buff, _row_color_simple_create(COLOR_JOIN));
}

void 
_callback_user_private(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Channel * chl = NULL;

   /* DBG("User Private %s", params[0]); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */
   /* DBG("\t%s", params[2]); */
   /* DBG("\t%s", params[3]); */

   if (!(chl = _window_channel_find(source)))
     {
        if (!(chl = _window_channel_find("Status")))
          return;
     }

   _channel_text_append(chl, source, params[1],
                        _row_color_simple_create(COLOR_TEXT));
}

void 
_callback_user_nick(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   Eina_List *channels, *l;
   Channel *chl;
   char buff[PATH_MAX + 100], user[PATH_MAX];
   int crlf = 0, lf = 0;

   crlf = _find_crlf(params[0], strlen(params[0]), &lf);
   if (crlf > 0)
     eina_strlcpy(user, params[0], crlf + lf - 1);
   else
     eina_strlcpy(user, params[0], strlen(params[0]) + 1);

   /* DBG("User Changed Nick %s", user); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */
   /* DBG("\t%s", params[2]); */
   /* DBG("\t%s", params[3]); */

   snprintf(buff, sizeof(buff), "%s is now known as %s\r\n", source, user);

   /* find all channels that this user is in */
   channels = _window_channels_user_find(source);
   EINA_LIST_FOREACH(channels, l, chl)
     _channel_text_append(chl, "*", buff, 
                          _row_color_simple_create(COLOR_NICK_CHANGE));
}

void
_callback_user_mode(Express_Network *net EINA_UNUSED, const char *event EINA_UNUSED, const char *source EINA_UNUSED, const char **params, unsigned int count EINA_UNUSED, void *data EINA_UNUSED)
{
   char user[PATH_MAX];
   int crlf = 0, lf = 0;

   crlf = _find_crlf(params[0], strlen(params[0]), &lf);
   if (crlf > 0)
     eina_strlcpy(user, params[0], crlf + lf - 1);
   else
     eina_strlcpy(user, params[0], strlen(params[0]) + 1);

   /* DBG("User Mode %s", user); */
   /* DBG("\tCount: %d", count); */
   /* DBG("\tUser: %s", source); */
   /* DBG("\tMessage:"); */
   /* DBG("\t%s", params[1]); */

   _window_network_channels_create(net);
}
