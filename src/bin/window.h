#ifndef _WINDOW_H_
# define _WINDOW_H_ 1

Eina_Bool _window_create(void);
Eina_Bool _window_destroy(void);
void _window_update(void);
void _window_size_update(void);

Channel *_window_channel_create(Express_Network *network, const char *name, const char *server);
void _window_channel_destroy(const char *name);
Channel *_window_channel_find(const char *name);
Channel *_window_channel_server_find(const char *server);
Eina_List *_window_channels_user_find(const char *user);

void _window_channel_swallow(Channel *chl);
void _window_channel_unswallow(Channel *chl);
void _window_channel_focus(Channel *chl);
Channel *_window_channel_focused_get(void);
Channel *_window_channel_previous_get(Channel *chl);
Channel *_window_channel_next_get(Channel *chl);
void _window_channel_switch(Channel *chl, Channel *new_chl);
void _window_channel_count_update(void);
Channel *_window_channel_active_get(void);
void _window_channel_activate(Channel *chl);

void _window_options_toggle(Evas_Object *grid, void (*cb_done)(void *data), void *data);

void _window_network_channels_create(Express_Network *net);
void _window_network_channels_destroy(Express_Network *net);

void _window_tabbar_fill(Channel *chl);
void _window_tabbar_update(Channel *chl);
void _window_tabcount_update(void);
void _window_treeview_update(void);

#endif
