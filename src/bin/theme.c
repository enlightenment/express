#include "private.h"
#include "theme.h"

static void 
_theme_cb_reload(void *data EINA_UNUSED, Evas_Object *obj, const char *emission EINA_UNUSED, const char *source EINA_UNUSED)
{
   void (*func)(void *d);
   void *func_data;

   _theme_reload(obj);
   func = evas_object_data_get(obj, "theme_reload_func");
   func_data = evas_object_data_get(obj, "theme_reload_func_data");
   if (func) func(func_data);
}

/* external functions */
const char *
_theme_default_get(void)
{
   static char path[PATH_MAX];

   if (path[0]) return path;

   /* FIXME: make themes configurable */
   snprintf(path, sizeof(path), 
            "%s/themes/default.edj", elm_app_data_dir_get());

   return path;
}

void 
_theme_reload(Evas_Object *obj)
{
   const char *file, *group;

   edje_object_file_get(obj, &file, &group);
   edje_object_file_set(obj, file, group);
}

Eina_Bool 
_theme_apply(Evas_Object *obj, const char *group)
{
   /* check for valid object and group */
   if ((!obj) || (!group)) return EINA_FALSE;

   /* try to set the theme for this object */
   if (edje_object_file_set(obj, _theme_default_get(), group))
     return EINA_TRUE;

   return EINA_FALSE;
}

void 
_theme_reload_enable(Evas_Object *obj)
{
   /* check for valid object */
   if (!obj) return;

   /* catch edje file change signal */
   edje_object_signal_callback_add(obj, "edje,change,file", "edje", 
                                   _theme_cb_reload, NULL);
}
