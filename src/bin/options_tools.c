#include "private.h"
#include "options_tools.h"
#include "options_network.h"
#include "options_servers.h"
#include "options_channels.h"

/* local variables */
static Evas_Object *o_base = NULL;
static Evas_Object *o_box = NULL;
static Evas_Object *o_tb = NULL;
static Eina_Bool _tools_out = EINA_FALSE;
static Ecore_Timer *_del_timer = NULL;
static Config_Network *_cfg_net = NULL;

static enum _opt_mode
{
   OPTION_NONE = 0,
   OPTION_NETWORK,
   OPTION_SERVERS,
   OPTION_CHANNELS
} _mode = 0;

static Eina_Bool 
_cb_del_delay(void *data EINA_UNUSED)
{
   if (o_tb) evas_object_del(o_tb);
   o_tb = NULL;
   if (o_box) evas_object_del(o_box);
   o_box = NULL;

   _del_timer = NULL;
   elm_cache_all_flush();

   return EINA_FALSE;
}

static void 
_cb_extra_done(void *data, Evas_Object *obj EINA_UNUSED, const char *sig EINA_UNUSED, const char *source EINA_UNUSED)
{
   elm_box_clear(o_box);

   switch (_mode)
     {
      case OPTION_NONE:
        break;
      case OPTION_NETWORK:
        _options_network(o_box, data, _cfg_net);
        break;
      case OPTION_SERVERS:
        _options_servers(o_box, data, _cfg_net);
        break;
      case OPTION_CHANNELS:
        _options_channels(o_box, data, _cfg_net);
        break;
     }

   if (_mode != OPTION_NONE)
     edje_object_signal_emit(data, "options,extra,show", PACKAGE_NAME);
}

static void 
_cb_extra_done2(void *data, Evas_Object *obj EINA_UNUSED, const char *sig EINA_UNUSED, const char *source EINA_UNUSED)
{
   if (_del_timer) ecore_timer_del(_del_timer);
   _del_timer = NULL;

   _cb_del_delay(NULL);

   edje_object_signal_callback_del(data, "options,extra,hide,done", 
                                   PACKAGE_NAME, _cb_extra_done2);
}

static void 
_cb_option(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   enum _opt_mode mode = (intptr_t)data;

   if (_mode == mode) return;
   _mode = mode;
   edje_object_signal_emit(o_base, "options,extra,hide", PACKAGE_NAME);
}

void 
_options_tools(Evas_Object *box, Evas_Object *base, Config_Network *cfg_net)
{
   Elm_Object_Item *o_itm;

   _mode = OPTION_NONE;

   if (!cfg_net)
     {
        if (_tools_out) goto kill;
        else return;
     }

   if ((box != o_box) && (o_box))
     {
        evas_object_del(o_tb);
        evas_object_del(o_box);
        o_tb = NULL;
        o_box = NULL;
     }

   o_base = base;
   _cfg_net = cfg_net;

   if (!o_box)
     {
        o_box = elm_box_add(box);
        elm_box_align_set(o_box, 0.0, 0.0);
        WEIGHT_SET(o_box, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
        ALIGN_SET(o_box, EVAS_HINT_FILL, EVAS_HINT_FILL);
        edje_object_part_swallow(base, "options_extra", o_box);
        evas_object_show(o_box);

        o_tb = elm_toolbar_add(box);
        elm_object_style_set(o_tb, "item_horizontal");
        elm_object_focus_allow_set(o_tb, EINA_TRUE);
        elm_toolbar_select_mode_set(o_tb, ELM_OBJECT_SELECT_MODE_ALWAYS);
        elm_toolbar_align_set(o_tb, 0.5);
        elm_toolbar_homogeneous_set(o_tb, EINA_TRUE);
        WEIGHT_SET(o_tb, EVAS_HINT_EXPAND, 0.0);
        ALIGN_SET(o_tb, EVAS_HINT_FILL, EVAS_HINT_FILL);

#define TB_APPEND(_icon, _label, _opt_mode) \
   elm_toolbar_item_append(o_tb, _icon, _label, _cb_option, \
                           (void *) OPTION_##_opt_mode)

        o_itm = 
          TB_APPEND("network-workgroup", "Network", NETWORK);
        elm_toolbar_item_selected_set(o_itm, EINA_TRUE);

        TB_APPEND("network-server", "Servers", SERVERS);
        TB_APPEND("network-wireless", "Channels", CHANNELS);
#undef TB_APPEND

        /* edje_object_part_swallow(base, "options_extra", o_box); */
        edje_object_part_swallow(base, "options_tools", o_tb);
        evas_object_show(o_tb);
     }
   else if ((o_box) && (!_tools_out))
     {
        /* edje_object_part_swallow(base, "options_tools", o_tb); */
        /* edje_object_part_swallow(base, "options_extra", o_box); */
        edje_object_signal_emit(base, "options,tools,show", PACKAGE_NAME);
        edje_object_signal_emit(base, "options,extra,show", PACKAGE_NAME);
     }

   if (!_tools_out)
     {
        edje_object_signal_emit(base, "options,tools,show", PACKAGE_NAME);
        edje_object_signal_callback_add(base, "options,extra,hide,done", 
                                        PACKAGE_NAME, _cb_extra_done, base);

        edje_object_signal_emit(base, "options,extra,hide", PACKAGE_NAME);
        elm_object_focus_set(o_tb, EINA_TRUE);
        _tools_out = EINA_TRUE;

        if (_del_timer) ecore_timer_del(_del_timer);
        _del_timer = NULL;
     }
   else
     {
kill:
        edje_object_signal_callback_del(base, "options,extra,hide,done", 
                                        PACKAGE_NAME, _cb_extra_done);
        edje_object_signal_callback_add(base, "options,extra,hide,done", 
                                        PACKAGE_NAME, _cb_extra_done2, base);

        if (o_tb) elm_object_focus_set(o_tb, EINA_FALSE);
        edje_object_signal_emit(base, "options,extra,hide", PACKAGE_NAME);
        edje_object_signal_emit(base, "options,tools,hide", PACKAGE_NAME);
        _tools_out = EINA_FALSE;

        if (_del_timer) ecore_timer_del(_del_timer);
        _del_timer = ecore_timer_add(2.0, _cb_del_delay, NULL);
     }
}
