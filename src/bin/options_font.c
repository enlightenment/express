#include "private.h"
#include "options_font.h"
#include "config.h"
#include "theme.h"

#define TEST_STRING "oislOIS.015!|,"

/* local structures */
typedef struct _Font Font;
struct _Font
{
   Elm_Object_Item *item;
   const char *name;
   Evas_Object *grid;
   Eina_Bool bitmap : 1;
};

/* local variables */
static Eina_List *_fonts = NULL;
static Eina_Hash *_fonts_hash = NULL;
static Evas_Object *o_slider, *o_small, *o_big, *o_list;

static void 
_cb_box_del(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Eina_List *fl;
   Font *f;

   if (!(fl = data)) return;

   EINA_LIST_FREE(fl, f)
     {
        if (f->name) eina_stringshare_del(f->name);
        f->name = NULL;
        free(f);
     }

   eina_hash_free(_fonts_hash);
   _fonts_hash = NULL;
   _fonts = NULL;
}

static int 
_cb_fonts_sort(const void *d1, const void *d2)
{
   return strcasecmp(d1, d2);
}

static char *
_cb_font_text_get(void *data, Evas_Object *obj EINA_UNUSED, const char *part EINA_UNUSED)
{
   Font *f;
   char buff[PATH_MAX], *p;

   if (!(f = data)) return NULL;
   eina_strlcpy(buff, f->name, sizeof(buff));
   buff[0] = toupper(buff[0]);
   if ((p = strrchr(buff, '.'))) *p = 0;
   return strdup(buff);
}

static void 
_cb_font_sel(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Font *f;

   if (!(f = data)) return;

   if ((_ex_cfg->font.name) && (!strcmp(f->name, _ex_cfg->font.name)))
     return;

   eina_stringshare_replace(&_ex_cfg->font.name, f->name);
   _ex_cfg->font.bitmap = f->bitmap;
   _config_save();

   elm_object_disabled_set(o_small, f->bitmap);
   elm_object_disabled_set(o_big, f->bitmap);
   elm_object_disabled_set(o_slider, f->bitmap);
}

static void 
_cb_size_sel(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Evas_Object *grid;
   int size = 0;

   if (!(grid = data)) return;

   size = elm_slider_value_get(obj) + 0.5;
   if (_ex_cfg->font.size == size) return;

   _ex_cfg->font.size = size;
   elm_genlist_realized_items_update(o_list);
   _config_save();
}

static Eina_Bool 
_cb_preview_eval_delay(void *data)
{
   Font *f;
   Evas *evas;
   Evas_Object *obj;
   Evas_Coord ox = 0, oy = 0, ow = 0, oh = 0;
   Evas_Coord vx = 0, vy = 0, vw = 0, vh = 0;

   if (!(obj = data)) return EINA_FALSE;

   if (!evas_object_visible_get(obj)) goto finish;
   if (edje_object_part_swallow_get(obj, "text.preview")) goto finish;
   if (!(f = evas_object_data_get(obj, "font"))) goto finish;

   evas_object_geometry_get(obj, &ox, &oy, &ow, &oh);
   if ((ow < 2) || (oh < 2)) goto finish;

   evas = evas_object_evas_get(obj);
   evas_output_viewport_get(evas, &vx, &vy, &vw, &vh);

   if (ELM_RECTS_INTERSECT(ox, oy, ow, oh, vx, vy, vw, vh))
     {
        Evas_Object *o;

        o = evas_object_text_add(evas);
        evas_object_color_set(o, 0, 0, 0, 255);
        evas_object_text_text_set(o, TEST_STRING);
        evas_object_scale_set(o, elm_config_scale_get());
        if (f->bitmap)
          {
             char buff[PATH_MAX];

             snprintf(buff, sizeof(buff), "%s/fonts/%s", 
                      elm_app_data_dir_get(), f->name);

             evas_object_text_font_set(o, buff, _ex_cfg->font.size);
          }
        else
          evas_object_text_font_set(o, f->name, _ex_cfg->font.size);

        evas_object_geometry_get(o, NULL, NULL, &ow, &oh);
        evas_object_size_hint_min_set(o, ow, oh);
        edje_object_part_swallow(obj, "text.preview", o);
     }

finish:
   evas_object_data_del(obj, "delay");
   return EINA_FALSE;
}

static void 
_cb_preview_eval(void *data, Evas *evas, Evas_Object *obj, void *event EINA_UNUSED)
{
   Font *f;
   Evas_Coord ox = 0, oy = 0, ow = 0, oh = 0;
   Evas_Coord vx = 0, vy = 0, vw = 0, vh = 0;

   if (!(f = data)) return;

   if (!evas_object_visible_get(obj)) return;
   if (edje_object_part_swallow_get(obj, "text.preview")) return;

   evas_object_geometry_get(obj, &ox, &oy, &ow, &oh);
   if ((ow < 2) || (oh < 2)) return;

   evas_output_viewport_get(evas, &vx, &vy, &vw, &vh);
   if (ELM_RECTS_INTERSECT(ox, oy, ow, oh, vx, vy, vw, vh))
     {
        Ecore_Timer *tmr;
        double rnd = 0.2;

        if ((tmr = evas_object_data_get(obj, "delay"))) 
          return;
        else
          evas_object_data_set(obj, "font", f);

        rnd += (double)((rand() % 100) / 500.0);
        tmr = ecore_timer_add(rnd, _cb_preview_eval_delay, obj);
        evas_object_data_set(obj, "delay", tmr);
     }
}

static void 
_cb_preview_del(void *data EINA_UNUSED, Evas *evas EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   Evas_Object *o;
   Ecore_Timer *tmr;

   if ((tmr = evas_object_data_get(obj, "delay")))
     {
        ecore_timer_del(tmr);
        evas_object_data_del(obj, "delay");
     }

   if ((o = edje_object_part_swallow_get(obj, "text.preview")))
     evas_object_del(o);
}

static Evas_Object *
_cb_font_content_get(void *data, Evas_Object *obj, const char *part)
{
   Font *f;

   if (!(f = data)) return NULL;
   if (!strcmp(part, "elm.swallow.icon"))
     {
        Evas_Object *o;
        double scale = 1.0;

        scale = elm_config_scale_get();
        o = edje_object_add(evas_object_evas_get(obj));
        _theme_apply(o, "express/fontpreview");
        _theme_reload_enable(o);
        evas_object_size_hint_min_set(o, (96 * scale), (40 * scale));
        evas_object_event_callback_add(o, EVAS_CALLBACK_MOVE, 
                                       _cb_preview_eval, f);
        evas_object_event_callback_add(o, EVAS_CALLBACK_RESIZE, 
                                       _cb_preview_eval, f);
        evas_object_event_callback_add(o, EVAS_CALLBACK_SHOW, 
                                       _cb_preview_eval, f);
        evas_object_event_callback_add(o, EVAS_CALLBACK_DEL, 
                                       _cb_preview_del, NULL);
        return o;
     }
   return NULL;
}

static char *
_cb_group_text_get(void *data, Evas_Object *obj EINA_UNUSED, const char *part EINA_UNUSED)
{
   return strdup(data);
}

static void
_cb_font_entry(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->font.use_entry = elm_check_state_get(obj);
   _config_save();
}

void 
_options_font(Evas_Object *box, Evas_Object *grid)
{
   Evas_Object *o_frame, *hbox, *vbox, *o_check;
   Elm_Object_Item *grp_it = NULL, *sel_it = NULL;
   Elm_Genlist_Item_Class *it_class, *it_grp;
   char buff[PATH_MAX], *file, *fname, *s;
   Eina_List *files, *fl, *l;
   Font *f;

   /* font frame */
   o_frame = elm_frame_add(box);
   WEIGHT_SET(o_frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o_frame, "Font");
   evas_object_show(o_frame);

   vbox = elm_box_add(box);
   elm_box_horizontal_set(vbox, EINA_FALSE);
   elm_box_homogeneous_set(vbox, EINA_FALSE);
   WEIGHT_SET(vbox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(vbox, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(o_frame, vbox);
   evas_object_show(vbox);

   o_check = elm_check_add(box);
   WEIGHT_SET(o_check, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o_check, EVAS_HINT_FILL, 0.5);
   elm_object_tooltip_text_set(o_check, "Use this font for entry box");
   elm_object_text_set(o_check, "Use font for entry box");
   elm_check_state_set(o_check, _ex_cfg->font.use_entry);
   evas_object_smart_callback_add(o_check, "changed", _cb_font_entry, NULL);
   elm_box_pack_end(vbox, o_check);
   evas_object_show(o_check);

   /* font size */
   hbox = elm_box_add(box);
   WEIGHT_SET(hbox, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(hbox, EVAS_HINT_FILL, 0.5);
   elm_box_horizontal_set(hbox, EINA_TRUE);
   elm_box_pack_end(vbox, hbox);
   evas_object_show(hbox);

   o_small = elm_label_add(box);
   elm_object_text_set(o_small, "<font_size=6>A</font_size>");
   elm_box_pack_end(hbox, o_small);
   evas_object_show(o_small);

   o_slider = elm_slider_add(box);
   WEIGHT_SET(o_slider, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o_slider, EVAS_HINT_FILL, 0.5);
   elm_slider_span_size_set(o_slider, 40);
   elm_slider_unit_format_set(o_slider, "%1.0f");
   elm_slider_indicator_format_set(o_slider, "%1.0f");
   elm_slider_min_max_set(o_slider, 5, 45);
   elm_slider_value_set(o_slider, _ex_cfg->font.size);
   evas_object_smart_callback_add(o_slider, "delay,changed", 
                                  _cb_size_sel, grid);
   elm_box_pack_end(hbox, o_slider);
   evas_object_show(o_slider);

   o_big = elm_label_add(box);
   elm_object_text_set(o_big, "<font_size=24>A</font_size>");
   elm_box_pack_end(hbox, o_big);
   evas_object_show(o_big);

   /* font list */
   it_class = elm_genlist_item_class_new();
   it_class->item_style = "end_icon";
   it_class->func.text_get = _cb_font_text_get;
   it_class->func.content_get = _cb_font_content_get;

   it_grp = elm_genlist_item_class_new();
   it_grp->item_style = "group_index";
   it_grp->func.text_get = _cb_group_text_get;

   o_list = elm_genlist_add(box);
   WEIGHT_SET(o_list, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_list, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_genlist_mode_set(o_list, ELM_LIST_COMPRESS);
   elm_genlist_homogeneous_set(o_list, EINA_TRUE);
   elm_box_pack_end(vbox, o_list);
   evas_object_show(o_list);

   snprintf(buff, sizeof(buff), "%s/fonts", elm_app_data_dir_get());
   if ((files = ecore_file_ls(buff)))
     {
        grp_it = 
          elm_genlist_item_append(o_list, it_grp, "Bitmap", NULL, 
                                  ELM_GENLIST_ITEM_GROUP, NULL, NULL);
        elm_genlist_item_select_mode_set(grp_it, 
                                         ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
     }

   EINA_LIST_FREE(files, file)
     {
        if (!(f = calloc(1, sizeof(Font))))
          continue;

        f->name = eina_stringshare_add(file);
        f->bitmap = EINA_TRUE;
        f->grid = grid;

        f->item = 
          elm_genlist_item_append(o_list, it_class, f, grp_it, 
                                  ELM_GENLIST_ITEM_NONE, _cb_font_sel, f);
        if ((_ex_cfg->font.bitmap) && (_ex_cfg->font.name) && 
            (!strcmp(_ex_cfg->font.name, f->name)))
          {
             sel_it = f->item;
             elm_genlist_item_selected_set(f->item, EINA_TRUE);
             elm_object_disabled_set(o_small, EINA_TRUE);
             elm_object_disabled_set(o_big, EINA_TRUE);
             elm_object_disabled_set(o_slider, EINA_TRUE);
          }

        _fonts = eina_list_append(_fonts, f);
        free(file);
     }

   fl = evas_font_available_list(evas_object_evas_get(box));
   fl = eina_list_sort(fl, eina_list_count(fl), _cb_fonts_sort);

   if (!_fonts_hash)
     _fonts_hash = eina_hash_string_superfast_new(NULL);

   if (_fonts)
     {
        grp_it = 
          elm_genlist_item_append(o_list, it_grp, "Standard", NULL, 
                                  ELM_GENLIST_ITEM_GROUP, NULL, NULL);
        elm_genlist_item_select_mode_set(grp_it, 
                                         ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
     }

   EINA_LIST_FOREACH(fl, l, fname)
     {
        snprintf(buff, sizeof(buff), "%s", fname);
        if ((s = strchr(buff, ':'))) *s = 0;
        fname = buff;
        if (!eina_hash_find(_fonts_hash, fname))
          {
             if (!(f = calloc(1, sizeof(Font))))
               continue;

             f->name = eina_stringshare_add(fname);
             f->bitmap = EINA_FALSE;
             f->grid = grid;

             f->item = 
               elm_genlist_item_append(o_list, it_class, f, grp_it, 
                                       ELM_GENLIST_ITEM_NONE, _cb_font_sel, f);
             if ((!_ex_cfg->font.bitmap) && (_ex_cfg->font.name) && 
                 (!strcmp(_ex_cfg->font.name, f->name)))
               {
                  sel_it = f->item;
                  elm_genlist_item_selected_set(f->item, EINA_TRUE);
               }

             eina_hash_add(_fonts_hash, fname, f);
             _fonts = eina_list_append(_fonts, f);
          }
     }

   if (fl) evas_font_available_list_free(evas_object_evas_get(box), fl);

   elm_genlist_item_show(sel_it, ELM_GENLIST_ITEM_SCROLLTO_TOP);

   elm_genlist_item_class_free(it_class);
   elm_genlist_item_class_free(it_grp);

   elm_box_pack_end(box, o_frame);

   /* hook box delete so we can free Font allocations */
   evas_object_event_callback_add(o_frame, EVAS_CALLBACK_DEL, 
                                  _cb_box_del, _fonts);
}
