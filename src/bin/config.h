#ifndef _CONFIG_H_
# define _CONFIG_H_ 1

typedef struct _Config_Channel Config_Channel;
typedef struct _Config_Server Config_Server;
typedef struct _Config_Network Config_Network;
typedef struct _Config Config;

struct _Config_Channel
{
   const char *name;
   const char *pass;
};

struct _Config_Server
{
   int port;
   const char *name;
};

struct _Config_Network
{
   const char *name;
   const char *nickname;
   const char *username;
   const char *nick_passwd;
   const char *server_passwd;
   Eina_Bool autoconnect;
   Eina_Bool bypass_proxy;
   Eina_Bool use_ssl;
   Eina_List *servers;
   Eina_List *channels;
};

struct _Config 
{
   int version;

   struct 
     {
        int size;
        const char *name;
        Eina_Bool bitmap;
        Eina_Bool use_entry;
     } font;

   struct 
     {
        double zoom;
        int opacity;
        int scrollback;
        int tabs;
        Eina_Bool translucent;
        Eina_Bool use_gravatar;
     } gui;

   struct 
     {
        int module;
        Eina_Bool muted;
        Eina_Bool visualized;
     } video;

   Eina_List *networks;
};

extern Config *_ex_cfg;

Eina_Bool _config_init(void);
Eina_Bool _config_shutdown(void);
void _config_load(void);
void _config_save(void);

#endif
