#include "private.h"
#include "window.h"
#include "config.h"
#include "theme.h"
#include "channel.h"
#include "selector.h"
#include "options.h"
#include "callbacks.h"

typedef struct _Group
{
   char *name;
   Eina_List *channels;
} Group;

struct _Window
{
   Evas *evas;

   Evas_Object *o_win;
   Evas_Object *o_bg;
   Evas_Object *o_conform;
   Evas_Object *o_base;
   Evas_Object *o_opts;
   Evas_Object *o_sel;
   Evas_Object *o_sel_bg;

   Eina_List *channels;

   Eina_Bool focused : 1;
};

/* local variables */
static Window *_win = NULL;
static Ecore_Job *_size_job = NULL;
static Ecore_Idler *_idler = NULL;

/* local functions */
static void 
_cb_delete_request(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   elm_exit();
}

static void 
_cb_del(void *data EINA_UNUSED, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   /* window object is already deleted at this point */
   _win->o_win = NULL;

   /* cleanup window structure */
   _window_destroy();
}

static void 
_cb_size_job(void *data EINA_UNUSED)
{
   Eina_List *l = NULL;
   Channel *chl;
   Evas_Coord mw = 0, mh = 0;
   Evas_Coord defw = 0, defh = 0;
   Evas_Coord sx = 0, sy = 0;

   _size_job = NULL;

   defw = 480 * elm_config_scale_get();
   defh = 310 * elm_config_scale_get();

   /* try to get the active channel */
   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        if (!_channel_active_get(chl)) continue;

        /* get minimum size of the channel */
        _channel_size_min_get(chl, &mw, &mh);

        /* trap for case where channel size is too small. This is used to 
         * enforce at least Some minimum size on the window */
        if (mw < defw) mw = defw;
        if (mh < defh) mh = defh;

        /* get step size of the channel */
        _channel_size_step_get(chl, &sx, &sy);

        /* set main window base size */
        elm_win_size_base_set(_win->o_win, (mw - sx), (mh - sy));

        /* set main window step size */
        elm_win_size_step_set(_win->o_win, sx, sy);

        /* set min size on bg */
        evas_object_size_hint_min_set(_win->o_bg, mw, mh);

        if (!_channel_size_done_get(chl))
          _channel_size_done_set(chl, EINA_TRUE);

        break;
     }
}

static void 
_cb_selector_destroy(void)
{
   Channel *chl;
   Eina_List *l;

   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        if (_channel_unswallowed_get(chl))
          {
             _channel_image_unset(chl);

             _channel_unswallowed_set(chl, EINA_FALSE);
          }
     }

   evas_object_del(_win->o_sel_bg);
   _win->o_sel_bg = NULL;

   evas_object_del(_win->o_sel);
   _win->o_sel = NULL;
}

static void 
_cb_selector_selected(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event)
{
   Channel *chl;
   Eina_List *l;

   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        if (event == _channel_image_get(chl))
          {
             /* swallow this channel */
             _window_channel_swallow(chl);

             /* activate this channel */
             _window_channel_activate(chl);

             /* destroy selector */
             _cb_selector_destroy();

             break;
          }
     }
}

static void 
_cb_selector_exit(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Channel *chl;
   Eina_List *l;

   /* destroy selector */
   _cb_selector_destroy();

   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        if (_channel_active_get(chl))
          {
             /* swallow this channel */
             _window_channel_swallow(chl);

             /* focus this channel */
             _window_channel_activate(chl);

             break;
          }
     }
}

static void 
_cb_selector_ending(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   edje_object_signal_emit(_win->o_sel_bg, "end", PACKAGE_NAME);
}

static void 
_cb_focus_in(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Channel *chl;

   if (!_win->focused) elm_win_urgent_set(_win->o_win, EINA_FALSE);
   _win->focused = EINA_TRUE;
   elm_win_keyboard_mode_set(_win->o_win, ELM_WIN_KEYBOARD_TERMINAL);

   if ((chl = _window_channel_active_get()))
     _channel_focus(chl);
}

static void 
_cb_focus_out(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Channel *chl;

   _win->focused = EINA_FALSE;
   elm_win_keyboard_mode_set(_win->o_win, ELM_WIN_KEYBOARD_OFF);

   if ((chl = _window_channel_active_get()))
     _channel_unfocus(chl);
}

static Eina_Bool 
_cb_idle(void *data EINA_UNUSED)
{
   Config_Network *cfg_net;
   Express_Network *net;
   static unsigned int i = 0;

   /* see if we have any more networks to process */
   if (!(cfg_net = eina_list_nth(_ex_cfg->networks, i)))
     return EINA_FALSE;

   /* test if we have seen this network before */
   if (!(net = express_network_find(cfg_net->name)))
     {
        Express_Callbacks cbs;
        Config_Server *srv;
        Eina_List *l = NULL;

        memset(&cbs, 0, sizeof(cbs));

        cbs.data = _win;
        cbs.command = _callback_command;
        cbs.connect = _callback_server_connected;
        cbs.motd = _callback_server_motd;
        cbs.notice = _callback_server_notice;
        cbs.error = _callback_server_error;
        cbs.channel_names = _callback_channel_names;
        cbs.channel_msg = _callback_channel_message;
        cbs.channel_notice = _callback_channel_notice;
        cbs.topic = _callback_channel_topic;
        cbs.topic_time = _callback_channel_topic_time;
        cbs.quit = _callback_user_quit;
        cbs.part = _callback_user_part;
        cbs.join = _callback_user_join;
        cbs.priv_msg = _callback_user_private;
        cbs.nick = _callback_user_nick;
        cbs.user_mode = _callback_user_mode;

        /* TODO: Implement remaining callbacks
         * 
         * NB: See src/lib/Express.h for callback comments */
        /* cbs.mode = ; */
        /* cbs.kick = ; */
        /* cbs.invite = ; */
        /* cbs.numeric = ; */
        /* cbs.unknown = ; */

        /* TODO: Implement these at a later date */
        /* cbs.ctcp_request = ; */
        /* cbs.ctcp_reply = ; */
        /* cbs.dcc_chat_request = ; */
        /* cbs.dcc_send_request = ; */

        /* try to create a new network */
        if (!(net = express_network_create(&cbs, cfg_net->name)))
          goto out;

        /* set network properties */
        express_network_username_set(net, cfg_net->username);
        express_network_nickname_set(net, cfg_net->nickname);
        express_network_nick_password_set(net, cfg_net->nick_passwd);
        express_network_server_password_set(net, cfg_net->server_passwd);
        express_network_autoconnect_set(net, cfg_net->autoconnect);
        express_network_bypass_proxy_set(net, cfg_net->bypass_proxy);
        express_network_use_ssl_set(net, cfg_net->use_ssl);

        /* if this is a new network, add the servers */
        EINA_LIST_FOREACH(cfg_net->servers, l, srv)
          express_network_server_add(net, srv->name, srv->port);

        if (cfg_net->autoconnect)
          {
             if (!express_network_connected_get(net))
               {
                  /* start connection process */
                  express_network_connect(net);
               }
          }
     }

   i++;

out:
   /* test if we have any more networks to process */
   if (i < eina_list_count(_ex_cfg->networks)) return EINA_TRUE;

   /* no more networks, cancel the idler */
   return EINA_FALSE;
}

static void 
_cb_tab_activate(void *data, Evas_Object *obj EINA_UNUSED, const char *sig EINA_UNUSED, const char *src EINA_UNUSED)
{
   Channel *chl, *pchl;

   chl = data;

   /* unswallow current channel */
   if ((pchl = _window_channel_active_get()))
     {
        if (pchl != chl)
          _window_channel_unswallow(pchl);
     }

   _window_channel_activate(chl);
   _window_channel_swallow(chl);

   _window_tabcount_update();
}

static void 
_cb_show(void *data EINA_UNUSED, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   if (!_idler) ecore_idler_add(_cb_idle, NULL);
}

static Evas_Object *
_window_new(void)
{
   Evas_Object *win, *icon;
   char buff[PATH_MAX];

   /* create new elm window */
   win = elm_win_add(NULL, PACKAGE_NAME, ELM_WIN_BASIC);
   elm_win_title_set(win, "Express");
   elm_win_icon_name_set(win, "Express");

   /* set function to call on a delete request */
   evas_object_smart_callback_add(win, "delete,request", 
                                  _cb_delete_request, win);

   /* create window icon */
   icon = evas_object_image_add(evas_object_evas_get(win));
   snprintf(buff, sizeof(buff), 
            "%s/icons/express.png", elm_app_data_dir_get());
   evas_object_image_file_set(icon, buff, NULL);

   /* set icon on window */
   elm_win_icon_object_set(win, icon);

   return win;
}

/* external functions */
Eina_Bool 
_window_create(void)
{
   Channel *chl;

   /* try to allocate space for new window */
   if (!(_win = calloc(1, sizeof(Window)))) return EINA_FALSE;

   /* create window object */
   _win->o_win = _window_new();
   evas_object_smart_callback_add(_win->o_win, "focus,in", _cb_focus_in, NULL);
   evas_object_smart_callback_add(_win->o_win, "focus,out", 
                                  _cb_focus_out, NULL);
   evas_object_event_callback_add(_win->o_win, EVAS_CALLBACK_SHOW, 
                                  _cb_show, NULL);

   /* store the canvas for easy reuse */
   _win->evas = evas_object_evas_get(_win->o_win);

   /* add a callback when this window gets deleted */
   evas_object_event_callback_add(_win->o_win, EVAS_CALLBACK_DEL, 
                                  _cb_del, NULL);

   /* add window background */
   _win->o_bg = evas_object_rectangle_add(_win->evas);
   WEIGHT_SET(_win->o_bg, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   FILL_SET(_win->o_bg, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_color_set(_win->o_bg, 0, 0, 0, 255);
   elm_win_resize_object_add(_win->o_win, _win->o_bg);
   evas_object_show(_win->o_bg);

   /* add a conformant here */
   _win->o_conform = elm_conformant_add(_win->o_win);
   WEIGHT_SET(_win->o_conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   FILL_SET(_win->o_conform, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_win_resize_object_add(_win->o_win, _win->o_conform);
   evas_object_show(_win->o_conform);

   /* add base object */
   _win->o_base = edje_object_add(_win->evas);
   _theme_apply(_win->o_base, "express/base");
   WEIGHT_SET(_win->o_base, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   FILL_SET(_win->o_base, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(_win->o_conform, _win->o_base);
   evas_object_show(_win->o_base);

   /* try to create a default channel */
   if ((chl = _window_channel_create(NULL, "Status", NULL)))
     {
        /* swallow channel background */
        _window_channel_swallow(chl);

        /* focus this channel */
        _window_channel_activate(chl);
     }

   /* update window transparency from config */
   _window_update();

   /* show the window */
   evas_object_show(_win->o_win);

   return EINA_TRUE;
}

Eina_Bool 
_window_destroy(void)
{
   Channel *chl;

   if (!_win) return EINA_TRUE;

   /* delete any existing idler */
   if (_idler) ecore_idler_del(_idler);
   _idler = NULL;

   /* delete any pending size job */
   if (_size_job) ecore_job_del(_size_job);
   _size_job = NULL;

   /* delete channels */
   EINA_LIST_FREE(_win->channels, chl)
     _channel_destroy(chl);

   /* delete the base */
   if (_win->o_base) evas_object_del(_win->o_base);

   /* delete conformant */
   if (_win->o_conform) evas_object_del(_win->o_conform);

   /* delete the background */
   if (_win->o_bg) evas_object_del(_win->o_bg);

   /* delete window object */
   if (_win->o_win) 
     {
        evas_object_event_callback_del_full(_win->o_win, EVAS_CALLBACK_SHOW, 
                                            _cb_show, NULL);

        evas_object_event_callback_del_full(_win->o_win, EVAS_CALLBACK_DEL, 
                                            _cb_del, NULL);

        evas_object_del(_win->o_win);
     }

   /* free allocated window structure */
   free(_win);
   _win = NULL;

   return EINA_TRUE;
}

Channel *
_window_channel_find(const char *name)
{
   Eina_List *l = NULL;
   Channel *chl;

   /* loop existing channels */
   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        const char *chl_name;

        /* get channel name and compare */
        chl_name = _channel_name_get(chl);
        if ((chl_name) && (strcmp(chl_name, name))) continue;

        return chl;
     }

   return NULL;
}

Channel *
_window_channel_server_find(const char *server)
{
   Eina_List *l = NULL;
   Channel *chl;

   /* loop existing channels */
   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        const char *srv_name;

        srv_name = _channel_server_name_get(chl);
        if ((srv_name) && (!strcmp(srv_name, server))) 
          return chl;
     }

   return NULL;
}

Eina_List *
_window_channels_user_find(const char *user)
{
   Eina_List *l, *ret = NULL;
   Channel *chl;

   EINA_LIST_FOREACH(_win->channels, l, chl)
     if (_channel_user_find(chl, user))
       ret = eina_list_append(ret, chl);

   return ret;
}

void 
_window_update(void)
{
   Eina_List *l = NULL;
   Channel *chl;
   Edje_Message_Int msg;

   if (!_win) return;

   /* adjust opacity of window */
   if (_ex_cfg->gui.translucent)
     msg.val = _ex_cfg->gui.opacity;
   else
     msg.val = 100;
   edje_object_message_send(_win->o_base, EDJE_MESSAGE_INT, 1, &msg);
   edje_object_message_send(_win->o_sel_bg, EDJE_MESSAGE_INT, 1, &msg);

   /* set window alpha based on config */
   elm_win_alpha_set(_win->o_win, _ex_cfg->gui.translucent);

   /* show/hide window background based on config */
   if (!_ex_cfg->gui.translucent) evas_object_show(_win->o_bg);
   else evas_object_hide(_win->o_bg);

   /* loop window channels and update */
   EINA_LIST_FOREACH(_win->channels, l, chl)
     _channel_update(chl);

   if (_ex_cfg->gui.tabs)
     _window_tabcount_update();

   _window_size_update();
}

void 
_window_size_update(void)
{
   /* delete previous size job */
   if (_size_job) ecore_job_del(_size_job);

   /* add a new job to update window size */
   _size_job = ecore_job_add(_cb_size_job, NULL);
}

Channel *
_window_channel_create(Express_Network *network, const char *name, const char *server)
{
   Channel *chl;

   if (!(chl = _channel_create(_win->evas, _win->o_win, name, server))) 
     return NULL;

   /* append this channel to the list */
   _win->channels = eina_list_append(_win->channels, chl);

   _channel_window_set(chl, _win->o_win);

   _channel_network_set(chl, network);

   /* force channel config update */
   _channel_update(chl);

   /* force channel size update */
   _channel_size_update(chl);

   /* update channel count */
   _window_channel_count_update();

   /* update tabcount */
   if (_ex_cfg->gui.tabs)
     _window_tabcount_update();

   return chl;
}

void 
_window_channel_destroy(const char *name)
{
   Channel *chl;

   /* try to find this channel */
   if (!(chl = _window_channel_find(name))) return;

   /* remove from list */
   _win->channels = eina_list_remove(_win->channels, chl);

   /* if this is the active channel, we need to reset window title */
   if (_channel_active_get(chl))
     {
        char buff[PATH_MAX];

        /* set window title */
        snprintf(buff, sizeof(buff), "%s", elm_app_name_get());
        elm_win_title_set(_win->o_win, buff);
     }

   /* unswallow from the window */
   _window_channel_unswallow(chl);

   /* destroy this channel */
   _channel_destroy(chl);
}

void 
_window_channel_swallow(Channel *chl)
{
   Evas_Object *bg;

   bg = _channel_background_get(chl);
   edje_object_part_swallow(_win->o_base, "base.content", bg);
   evas_object_show(bg);
}

void 
_window_channel_unswallow(Channel *chl)
{
   Evas_Object *bg;

   bg = _channel_background_get(chl);
   edje_object_part_unswallow(_win->o_base, bg);
   evas_object_hide(bg);
}

void 
_window_channel_focus(Channel *chl)
{
   Eina_List *l = NULL;
   Channel *pchl;

   /* unactivate any previously active channels */
   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        if (pchl != chl)
          {
             if (_channel_focused_get(pchl))
               _channel_unfocus(pchl);
          }
     }

   /* focus this channel */
   _channel_focus(chl);
}

Channel *
_window_channel_active_get(void)
{
   Channel *chl;
   Eina_List *l;

   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        if (_channel_active_get(chl)) 
          return chl;
     }

   return NULL;
}

void 
_window_channel_activate(Channel *chl)
{
   Eina_List *l = NULL;
   Channel *pchl;
   char buff[PATH_MAX];

   /* unactivate any previously active channels */
   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        if (pchl != chl)
          {
             if (_channel_active_get(pchl))
               _channel_active_set(pchl, EINA_FALSE);
          }
     }

   /* focus this channel */
   _channel_active_set(chl, EINA_TRUE);

   if (_channel_missed_get(chl))
     _channel_missed_set(chl, EINA_FALSE);

   /* update window channel count */
   _window_channel_count_update();

   /* update tabcount */
   if (_ex_cfg->gui.tabs)
     _window_tabcount_update();

   /* set window title */
   snprintf(buff, sizeof(buff), "%s - %s", elm_app_name_get(), 
            _channel_name_get(chl));
   elm_win_title_set(_win->o_win, buff);
}

Channel *
_window_channel_focused_get(void)
{
   Channel *chl;
   Eina_List *l;

   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        if (_channel_focused_get(chl)) 
          return chl;
     }

   return NULL;
}

Channel *
_window_channel_previous_get(Channel *chl)
{
   Eina_List *l;

   l = eina_list_data_find_list(_win->channels, chl);
   if ((l) && (l->prev)) return l->prev->data;
   return NULL;
}

Channel *
_window_channel_next_get(Channel *chl)
{
   Eina_List *l;

   l = eina_list_data_find_list(_win->channels, chl);
   if ((l) && (l->next)) return l->next->data;
   return NULL;
}

void 
_window_channel_switch(Channel *chl, Channel *new_chl)
{
   Channel *pchl;
   Eina_List *l = NULL;
   double z = 1.0;
   int count = 0;

   /* delete old selector objects */
   if (_win->o_sel_bg) evas_object_del(_win->o_sel_bg);
   if (_win->o_sel) evas_object_del(_win->o_sel);

   /* hide channel background */
   _channel_background_hide(chl);

   /* create selector background */
   _win->o_sel_bg = edje_object_add(_win->evas);
   _theme_apply(_win->o_sel_bg, "express/selector/background");

   edje_object_signal_emit(_win->o_sel_bg, "begin", PACKAGE_NAME);

   /* create selector */
   if (!(_win->o_sel = _selector_create(_win->evas)))
     {
        evas_object_del(_win->o_sel_bg);
        _win->o_sel_bg = NULL;
        return;
     }

   /* loop channels, creating an img to represent each one */
   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        _channel_base_unswallow(pchl);

        /* create an image to represent this channel */
        _channel_image_create(pchl);

        /* add this channel to the selector */
        _selector_channel_add(_win->o_sel, pchl, (pchl == chl));
     }

   edje_object_part_swallow(_win->o_sel_bg, "content", _win->o_sel);
   evas_object_show(_win->o_sel);

   edje_object_part_swallow(_win->o_base, "base.content", _win->o_sel_bg);
   evas_object_show(_win->o_sel_bg);

   evas_object_smart_callback_add(_win->o_sel, "selected", 
                                  _cb_selector_selected, NULL);
   evas_object_smart_callback_add(_win->o_sel, "exit", 
                                  _cb_selector_exit, NULL);
   evas_object_smart_callback_add(_win->o_sel, "ending", 
                                  _cb_selector_ending, NULL);

   _selector_go(_win->o_sel);

   /* calculate zoom based on number of channels */
   count = eina_list_count(_win->channels);
   if (count >= 1)
     z = (1.0 / (sqrt(count)) * 0.8);
   if (z > 1.0) z = 1.0;

   /* set selector zoom */
   _selector_zoom_set(_win->o_sel, z);
   _selector_zoom(_win->o_sel, z);

   if (chl != new_chl)
     {
        _selector_channel_selected_set(_win->o_sel, new_chl, EINA_TRUE);
        _selector_exit(_win->o_sel);
     }

   elm_object_focus_set(_win->o_sel, EINA_TRUE);
}

void 
_window_channel_count_update(void)
{
   Eina_List *l = NULL;
   Channel *pchl;
   int missed = 0, cnt = 0, n = 0;

   n = eina_list_count(_win->channels);
   if (n < 1) return;

   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        if (_channel_missed_get(pchl)) missed++;
        cnt++;

        _channel_spacer_create(pchl);
        _channel_count_set(pchl, cnt, n);
     }

   EINA_LIST_FOREACH(_win->channels, l, pchl)
     _channel_missed_count_set(pchl, missed);

   if (missed > 0)
     elm_win_urgent_set(_win->o_win, EINA_TRUE);
}

void 
_window_options_toggle(Evas_Object *grid, void (*cb_done)(void *data), void *data)
{
   _options_toggle(_win->o_win, _win->o_base, grid, cb_done, data);
}

void 
_window_network_channels_create(Express_Network *net)
{
   Eina_List *l;
   Config_Network *cfg_net;
   Channel *chl;
   Express_Server *srv;
   const char *name, *srv_name = NULL;
   /* int i = -1; */

   /* get the name of this network */
   name = express_network_name_get(net);

   /* get which server is connected on this network */
   if ((srv = express_network_server_connected_get(net)))
     {
        /* get the name of this server */
        srv_name = express_network_server_realname_get(srv);
     }

   /* go through network configs and find this network */
   EINA_LIST_FOREACH(_ex_cfg->networks, l, cfg_net)
     {
        Eina_List *c;
        Config_Channel *cfg_chl;

        if (strcmp(cfg_net->name, name)) continue;

        /* loop network channels and create */
        EINA_LIST_FOREACH(cfg_net->channels, c, cfg_chl)
          {
             if (!express_network_channel_join(net, cfg_chl->name, 
                                               cfg_chl->pass))
               continue;

             /* try to create a channel */
             if (!(chl = _window_channel_create(net, cfg_chl->name, srv_name)))
               {
                  express_network_channel_part(net, cfg_chl->name);
                  continue;
               }

             _channel_network_set(chl, net);
          }

        break;
     }

   /* if (i >= 0) */
   /*   { */
        /* remove the default channel window
         *
         * TODO:
         * NB: This COULD become a config option for 'server message window'
         * and optionally be left open to display server messages */
        /* _window_channel_destroy("Status"); */
     /* } */

   /* _window_channel_count_update(NULL); */
}

void 
_window_network_channels_destroy(Express_Network *net)
{
   Eina_List *l;
   Config_Network *cfg_net;
   const char *name;

   /* get the name of this network */
   name = express_network_name_get(net);

   /* go through network configs and find this network */
   EINA_LIST_FOREACH(_ex_cfg->networks, l, cfg_net)
     {
        Eina_List *c;
        Config_Channel *cfg_chl;

        if (strcmp(cfg_net->name, name)) continue;

        /* loop network channels and destroy */
        EINA_LIST_FOREACH(cfg_net->channels, c, cfg_chl)
          {
             /* destroy this channel */
             _window_channel_destroy(cfg_chl->name);
          }

        break;
     }

   /* if we have no more channel windows, create a default */
   if (eina_list_count(_win->channels) < 1)
     {
        Channel *chl;

        /* try to create a default channel */
        if ((chl = _window_channel_create(NULL, "Status", NULL)))
          {
             /* swallow channel background */
             _window_channel_swallow(chl);

             /* focus this channel */
             _window_channel_focus(chl);
          }
     }

   _window_treeview_update();
}

void 
_window_tabbar_fill(Channel *chl)
{
   Eina_List *l;
   Channel *pchl;
   int n = 0, i = 0, j = 0;
   Evas_Object *o;

   n = eina_list_count(_win->channels);
   if (n < 1) return;

   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        if (pchl == chl) break;
        i++;
     }

   if (i > 0)
     _channel_tabbar_lbox_create(chl, _win->o_win);
   if (i < (n - 1))
     _channel_tabbar_rbox_create(chl, _win->o_win);

   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        if (pchl != chl)
          {
             Evas_Coord w = 0, h = 0;

             o = edje_object_add(_win->evas);
             _theme_apply(o, "express/tabbar_back");
             WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
             FILL_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
             edje_object_part_text_set(o, "tab.title", _channel_name_get(pchl));
             edje_object_size_min_calc(o, &w, &h);
             evas_object_size_hint_min_set(o, w, h);
             if (j < i)
               _channel_tabbar_lbox_append(chl, o);
             else if (j > i)
               _channel_tabbar_rbox_append(chl, o);

             evas_object_data_set(o, "channel", pchl);
             evas_object_show(o);
             edje_object_signal_callback_add(o, "tab,activate", PACKAGE_NAME, 
                                             _cb_tab_activate, pchl);

             if (_ex_cfg->gui.tabs)
               _channel_tabbar_update(pchl);
          }
        j++;
     }
}

void 
_window_tabbar_update(Channel *chl)
{
   Channel *pchl;

   if (!_ex_cfg->gui.tabs) return;

   pchl = _window_channel_active_get();
   if ((pchl) && (pchl != chl))
     {
        /* update tab color in pchl */
        _channel_tabbar_update(pchl);
     }
}

void 
_window_tabcount_update(void)
{
   Eina_List *l;
   Channel *pchl;
   int n = 0, i = 0;
   double v1 = 0.0, v2 = 0.0;

   if (!_ex_cfg->gui.tabs) return;

   n = eina_list_count(_win->channels);
   if (n < 1) return;

   EINA_LIST_FOREACH(_win->channels, l, pchl)
     {
        if ((n > 1) && (_ex_cfg->gui.tabs))
          {
             v1 = (double)i / (double)n;
             v2 = (double)(i + 1) / (double)n;
             _channel_tabspacer_create(pchl, v1, v2);
             _channel_tabbar_clear(pchl);
             _window_tabbar_fill(pchl);
          }
        else
          {
             _channel_tabbar_clear(pchl);
             _channel_tabspacer_destroy(pchl);
          }
        i++;
     }
}

static void
_cb_groups_free(void *data)
{
   Group *group;

   group = data;
   free(group->name);
   free(group);
}

void 
_window_treeview_update(void)
{
   Eina_List *l, *ll;
   Channel *chl, *lchl;
   Eina_Hash *groups;
   Group *group;
   Eina_Iterator *itr;
   void *data = NULL;

   groups = eina_hash_string_superfast_new(_cb_groups_free);

   EINA_LIST_FOREACH(_win->channels, l, chl)
     _channel_treeview_clear(chl);

   lchl = _window_channel_find("Status");

   /* add Status to each channel */
   EINA_LIST_FOREACH(_win->channels, l, chl)
     _channel_treeview_item_add(chl, lchl);

   EINA_LIST_FOREACH(_win->channels, l, chl)
     {
        Express_Network *net;

        net = _channel_network_get(chl);
        if (net)
          {
             const char *name;

             name = express_network_name_get(net);
             group = eina_hash_find(groups, name);
             if (!group)
               {
                  group = calloc(1, sizeof(Group));
                  group->name = strdup(name);

                  eina_hash_add(groups, name, group);
               }

             if (!eina_list_data_find(group->channels, chl))
               group->channels = eina_list_append(group->channels, chl);
          }
     }

   itr = eina_hash_iterator_data_new(groups);
   while (eina_iterator_next(itr, &data))
     {
        group = (Group *)data;

        EINA_LIST_FOREACH(_win->channels, l, chl)
          {
             Elm_Object_Item *it;

             it = _channel_treeview_group_add(chl, strdup(group->name));

             EINA_LIST_FOREACH(group->channels, ll, lchl)
               _channel_treeview_group_item_add(chl, lchl, it);
          }
     }
   eina_iterator_free(itr);

   eina_hash_free(groups);
}
