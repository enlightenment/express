#ifndef _THEME_H_
# define _THEME_H_ 1

const char *_theme_default_get(void);
void _theme_reload(Evas_Object *obj);
Eina_Bool _theme_apply(Evas_Object *obj, const char *group);
void _theme_reload_enable(Evas_Object *obj);

#endif
