#include "private.h"
#include "options_networks.h"
#include "config.h"
#include "options_tools.h"
#include "utils.h"

/* local variables */
static Evas_Object *o_box = NULL;
static Evas_Object *o_list = NULL;
static Evas_Object *o_edit = NULL;
static Evas_Object *o_del = NULL;

static void 
_cb_net_sel(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   elm_object_disabled_set(o_del, EINA_FALSE);
   elm_object_disabled_set(o_edit, EINA_FALSE);
}

static void 
_cb_net_add(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Evas_Object *base;
   Config_Network *cfg_net;
   char *usr;

   if (!(base = data)) return;
   if (!(cfg_net = malloc(sizeof(Config_Network)))) return;

   cfg_net->servers = NULL;
   cfg_net->channels = NULL;
   cfg_net->name = eina_stringshare_add("New Network");
   cfg_net->nick_passwd = NULL;
   cfg_net->server_passwd = NULL;
   cfg_net->autoconnect = EINA_TRUE;
   cfg_net->bypass_proxy = EINA_FALSE;
   cfg_net->use_ssl = EINA_FALSE;

   cfg_net->nickname = NULL;
   if ((usr = _util_user_name_get()))
     cfg_net->nickname = eina_stringshare_add(usr);

   cfg_net->username = NULL;
   if ((usr = _util_user_fullname_get()))
     cfg_net->username = eina_stringshare_add(usr);

   _ex_cfg->networks = eina_list_append(_ex_cfg->networks, cfg_net);
   _config_save();

   elm_box_clear(o_box);
   _options_tools(o_box, base, cfg_net);
}

static void 
_cb_net_del(void *data EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Elm_Object_Item *item;

   if ((item = elm_list_selected_item_get(o_list)))
     {
        Config_Network *cfg_net;

        if ((cfg_net = elm_object_item_data_get(item)))
          {
             Config_Server *srv;
             Config_Channel *chl;

             _ex_cfg->networks = 
               eina_list_remove(_ex_cfg->networks, cfg_net);

             EINA_LIST_FREE(cfg_net->servers, srv)
               {
                  if (srv->name) eina_stringshare_del(srv->name);
                  free(srv);
               }

             EINA_LIST_FREE(cfg_net->channels, chl)
               {
                  if (chl->name) eina_stringshare_del(chl->name);
                  free(chl);
               }

             if (cfg_net->name) 
               eina_stringshare_del(cfg_net->name);
             if (cfg_net->name) 
               eina_stringshare_del(cfg_net->nick_passwd);
             if (cfg_net->name) 
               eina_stringshare_del(cfg_net->server_passwd);

             free(cfg_net);

             _config_save();
          }

        elm_object_item_del(item);

        elm_object_disabled_set(o_del, EINA_TRUE);
        elm_object_disabled_set(o_edit, EINA_TRUE);
     }
}

static void 
_cb_net_edit(void *data, Evas_Object *obj EINA_UNUSED, void *event EINA_UNUSED)
{
   Evas_Object *base;
   Elm_Object_Item *item;

   if (!(base = data)) return;
   if ((item = elm_list_selected_item_get(o_list)))
     {
        Config_Network *cfg_net;

        if (!(cfg_net = elm_object_item_data_get(item)))
          return;

        elm_box_clear(o_box);
        _options_tools(o_box, base, cfg_net);
     }
}

void 
_options_networks(Evas_Object *box, Evas_Object *base)
{
   Evas_Object *o_frame, *tb, *o;
   Eina_List *l = NULL;
   Config_Network *cfg_net;

   o_box = box;

   /* network frame */
   o_frame = elm_frame_add(box);
   elm_object_text_set(o_frame, "Networks");
   WEIGHT_SET(o_frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(box, o_frame);
   evas_object_show(o_frame);

   tb = elm_table_add(box);
   elm_table_homogeneous_set(tb, EINA_FALSE);
   elm_table_padding_set(tb, 4, 2);
   WEIGHT_SET(tb, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(tb, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(o_frame, tb);
   evas_object_show(tb);

   /* network list */
   o_list = elm_list_add(box);
   WEIGHT_SET(o_list, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_list, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_table_pack(tb, o_list, 0, 0, 1, 4);
   evas_object_show(o_list);

   EINA_LIST_FOREACH(_ex_cfg->networks, l, cfg_net)
     {
        Evas_Object *icon;

        icon = elm_icon_add(o_list);
        elm_icon_standard_set(icon, "network-workgroup");
        evas_object_size_hint_aspect_set(icon, 
                                         EVAS_ASPECT_CONTROL_VERTICAL, 1, 1);
        elm_list_item_append(o_list, cfg_net->name, icon, NULL, 
                             _cb_net_sel, cfg_net);
     }
   elm_list_go(o_list);

   o = elm_button_add(box);
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "clicked", _cb_net_add, base);
   elm_object_text_set(o, "Add");
   elm_table_pack(tb, o, 1, 0, 1, 1);
   evas_object_show(o);

   o_del = elm_button_add(box);
   WEIGHT_SET(o_del, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o_del, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o_del, "clicked", _cb_net_del, NULL);
   elm_object_text_set(o_del, "Remove");
   elm_object_disabled_set(o_del, EINA_TRUE);
   elm_table_pack(tb, o_del, 1, 1, 1, 1);
   evas_object_show(o_del);

   o_edit = elm_button_add(box);
   WEIGHT_SET(o_edit, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o_edit, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o_edit, "clicked", _cb_net_edit, base);
   elm_object_text_set(o_edit, "Edit");
   elm_object_disabled_set(o_edit, EINA_TRUE);
   elm_table_pack(tb, o_edit, 1, 2, 1, 1);
   evas_object_show(o_edit);
}
