#include "private.h"
#include "options_general.h"
#include "config.h"

static Evas_Object *o_opacity;

static void
_cb_gravatar(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->gui.use_gravatar = elm_check_state_get(obj);
   _config_save();
}

static void 
_cb_translucent(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->gui.translucent = elm_check_state_get(obj);
   elm_object_disabled_set(o_opacity, !_ex_cfg->gui.translucent);
   _config_save();
}

static void 
_cb_opacity(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->gui.opacity = elm_slider_value_get(obj);
   _config_save();
}

static void 
_cb_scrollback(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->gui.scrollback = elm_slider_value_get(obj) + 0.5;
   _config_save();
}

static void 
_cb_tabs(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->gui.tabs = elm_radio_value_get(obj);
   _config_save();
}

void 
_options_general(Evas_Object *box, Evas_Object *grid EINA_UNUSED)
{
   Evas_Object *o, *o_frame, *vbox, *group;

   /* general frame */
   o_frame = elm_frame_add(box);
   WEIGHT_SET(o_frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o_frame, "User Interface");
   evas_object_show(o_frame);

   vbox = elm_box_add(box);
   WEIGHT_SET(vbox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(vbox, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(o_frame, vbox);
   evas_object_show(vbox);

   group = elm_radio_add(box);
   WEIGHT_SET(group, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(group, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(group, "Use Tabs");
   elm_radio_state_value_set(group, 1);
   evas_object_smart_callback_add(group, "changed", _cb_tabs, NULL);
   elm_box_pack_end(vbox, group);
   evas_object_show(group);

   o = elm_radio_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o, "Use Treeview");
   elm_radio_state_value_set(o, 0);
   elm_radio_group_add(o, group);
   evas_object_smart_callback_add(o, "changed", _cb_tabs, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   elm_radio_value_set(group, _ex_cfg->gui.tabs);

   o = elm_separator_add(box);
   elm_separator_horizontal_set(o, EINA_TRUE);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_check_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_tooltip_text_set(o, "Translucent");
   elm_object_text_set(o, "Translucent");
   elm_check_state_set(o, _ex_cfg->gui.translucent);
   evas_object_smart_callback_add(o, "changed", _cb_translucent, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o_opacity = o = elm_slider_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.0);
   elm_slider_span_size_set(o, 40);
   elm_slider_unit_format_set(o, "%1.0f%%");
   elm_slider_indicator_format_set(o, "%1.0f%%");
   elm_slider_min_max_set(o, 0, 100);
   elm_slider_value_set(o, _ex_cfg->gui.opacity);
   evas_object_smart_callback_add(o, "delay,changed", _cb_opacity, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_separator_add(box);
   elm_separator_horizontal_set(o, EINA_TRUE);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_check_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_tooltip_text_set(o, "Show Gravatar image (if associated) when mouse hover on email address.");
   elm_object_text_set(o, "Gravatar Integration");
   elm_check_state_set(o, _ex_cfg->gui.use_gravatar);
   evas_object_smart_callback_add(o, "changed", _cb_gravatar, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_separator_add(box);
   elm_separator_horizontal_set(o, EINA_TRUE);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_label_add(box);
   elm_object_text_set(o, "Scrollback:");
   WEIGHT_SET(o, 0.0, 0.0);
   ALIGN_SET(o, 0.0, 0.5);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_slider_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.0);
   elm_slider_span_size_set(o, 40);
   elm_slider_unit_format_set(o, "%1.0f");
   elm_slider_indicator_format_set(o, "%1.0f");
   elm_slider_min_max_set(o, 0, 10000);
   elm_slider_value_set(o, _ex_cfg->gui.scrollback);
   evas_object_smart_callback_add(o, "delay,changed", _cb_scrollback, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   elm_box_pack_end(box, o_frame);
}
