#ifndef _GRID_SAVE_H
# define _GRID_SAVE_H 1

# include "grid.h"

void _grid_save_register(Evas_Object *obj);
void _grid_save_unregister(Evas_Object *obj);
void _grid_save_freeze(void);
void _grid_save_thaw(void);
Grid_Save *_grid_save_new(int w);
void _grid_save_free(Grid_Save *gs);
Grid_Save *_grid_save_extract(Grid_Save *gs);

#endif
