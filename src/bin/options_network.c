#include "private.h"
#include "options_network.h"
#include "config.h"
#include "utils.h"

static void 
_cb_name_changed(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;
   const char *txt;

   if (!(cfg_net = data)) return;
   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_net->name, txt);
   _config_save();
}

static void 
_cb_nick_pass_changed(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;
   const char *txt;

   if (!(cfg_net = data)) return;
   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_net->nick_passwd, txt);
   _config_save();
}

static void 
_cb_srv_pass_changed(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;
   const char *txt;

   if (!(cfg_net = data)) return;
   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_net->server_passwd, txt);
   _config_save();
}

static void 
_cb_nickname_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;
   const char *txt;

   if (!(cfg_net = data)) return;
   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_net->nickname, txt);
   _config_save();
}

static void 
_cb_username_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;
   const char *txt;

   if (!(cfg_net = data)) return;
   txt = elm_entry_entry_get(obj);
   eina_stringshare_replace(&cfg_net->username, txt);
   _config_save();
}

static void 
_cb_autoconnect(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;

   if (!(cfg_net = data)) return;
   cfg_net->autoconnect = elm_check_state_get(obj);
   _config_save();
}

static void 
_cb_ssl(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;

   if (!(cfg_net = data)) return;
   cfg_net->use_ssl = elm_check_state_get(obj);
   _config_save();
}

static void 
_cb_proxy(void *data, Evas_Object *obj, void *event EINA_UNUSED)
{
   Config_Network *cfg_net;

   if (!(cfg_net = data)) return;
   cfg_net->bypass_proxy = elm_check_state_get(obj);
   _config_save();
}

void 
_options_network(Evas_Object *box, Evas_Object *base EINA_UNUSED, Config_Network *cfg_net)
{
   Evas_Object *vbox, *fr, *tb, *o, *hbox;

   if (!cfg_net->nickname)
     {
        char *usr;

        if ((usr = _util_user_name_get()))
          cfg_net->nickname = eina_stringshare_add(usr);
     }

   if (!cfg_net->username)
     {
        char *usr;

        if ((usr = _util_user_fullname_get()))
          cfg_net->username = eina_stringshare_add(usr);
     }

   fr = elm_frame_add(box);
   elm_object_text_set(fr, "Network");
   WEIGHT_SET(fr, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(fr, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(box, fr);
   evas_object_show(fr);

   vbox = elm_box_add(box);
   elm_box_horizontal_set(vbox, EINA_FALSE);
   elm_box_align_set(vbox, 0.0, 0.0);
   WEIGHT_SET(vbox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(vbox, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(fr, vbox);
   evas_object_show(vbox);

   tb = elm_table_add(box);
   elm_table_homogeneous_set(tb, EINA_FALSE);
   elm_table_padding_set(tb, 4, 2);
   WEIGHT_SET(tb, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(tb, EVAS_HINT_FILL, 0.0);
   elm_box_pack_end(vbox, tb);
   evas_object_show(tb);

   o = elm_label_add(box);
   elm_object_text_set(o, "Name");
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_table_pack(tb, o, 0, 0, 1, 1);
   evas_object_show(o);

   o = elm_entry_add(box);
   elm_entry_entry_set(o, cfg_net->name);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   elm_entry_input_panel_layout_set(o, ELM_INPUT_PANEL_LAYOUT_PASSWORD);
   elm_scroller_policy_set(o, ELM_SCROLLER_POLICY_OFF, 
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "changed,user", 
                                  _cb_name_changed, cfg_net);
   elm_table_pack(tb, o, 1, 0, 2, 1);
   evas_object_show(o);

   o = elm_label_add(box);
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_object_text_set(o, "Nick Name");
   elm_table_pack(tb, o, 0, 1, 1, 1);
   evas_object_show(o);

   o = elm_entry_add(box);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   elm_entry_input_panel_layout_set(o, ELM_INPUT_PANEL_LAYOUT_NORMAL);
   elm_scroller_policy_set(o, ELM_SCROLLER_POLICY_OFF, 
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o, cfg_net->nickname);
   evas_object_smart_callback_add(o, "changed,user", 
                                  _cb_nickname_changed, cfg_net);
   elm_table_pack(tb, o, 1, 1, 2, 1);
   evas_object_show(o);

   o = elm_label_add(box);
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_object_text_set(o, "User Name");
   elm_table_pack(tb, o, 0, 2, 1, 1);
   evas_object_show(o);

   o = elm_entry_add(box);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   elm_entry_input_panel_layout_set(o, ELM_INPUT_PANEL_LAYOUT_NORMAL);
   elm_scroller_policy_set(o, ELM_SCROLLER_POLICY_OFF, 
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o, cfg_net->username);
   evas_object_smart_callback_add(o, "changed,user", 
                                  _cb_username_changed, cfg_net);
   elm_table_pack(tb, o, 1, 2, 2, 1);
   evas_object_show(o);

   o = elm_label_add(box);
   elm_object_text_set(o, "Nickserv Password");
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_table_pack(tb, o, 0, 3, 1, 1);
   evas_object_show(o);

   o = elm_entry_add(box);
   elm_entry_entry_set(o, cfg_net->nick_passwd);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_password_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   elm_entry_input_panel_layout_set(o, ELM_INPUT_PANEL_LAYOUT_PASSWORD);
   elm_scroller_policy_set(o, ELM_SCROLLER_POLICY_OFF, 
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "changed,user", 
                                  _cb_nick_pass_changed, cfg_net);
   elm_table_pack(tb, o, 1, 3, 2, 1);
   evas_object_show(o);

   o = elm_label_add(box);
   elm_object_text_set(o, "Server Password");
   WEIGHT_SET(o, EVAS_HINT_FILL, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_table_pack(tb, o, 0, 4, 1, 1);
   evas_object_show(o);

   o = elm_entry_add(box);
   elm_entry_entry_set(o, cfg_net->server_passwd);
   elm_entry_single_line_set(o, EINA_TRUE);
   elm_entry_password_set(o, EINA_TRUE);
   elm_entry_scrollable_set(o, EINA_TRUE);
   elm_entry_input_panel_layout_set(o, ELM_INPUT_PANEL_LAYOUT_PASSWORD);
   elm_scroller_policy_set(o, ELM_SCROLLER_POLICY_OFF, 
                           ELM_SCROLLER_POLICY_OFF);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "changed,user", 
                                  _cb_srv_pass_changed, cfg_net);
   elm_table_pack(tb, o, 1, 4, 2, 1);
   evas_object_show(o);

   o = elm_separator_add(box);
   elm_separator_horizontal_set(o, EINA_TRUE);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   hbox = elm_box_add(box);
   elm_box_horizontal_set(hbox, EINA_TRUE);
   elm_box_align_set(hbox, 0.5, 0.0);
   WEIGHT_SET(hbox, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(hbox, 0.5, EVAS_HINT_FILL);
   elm_box_pack_end(vbox, hbox);
   evas_object_show(hbox);

   o = elm_check_add(box);
   elm_object_text_set(o, "Autoconnect");
   elm_check_state_set(o, cfg_net->autoconnect);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "changed", _cb_autoconnect, cfg_net);
   elm_box_pack_end(hbox, o);
   evas_object_show(o);

   o = elm_check_add(box);
   elm_object_text_set(o, "Use SSL");
   elm_check_state_set(o, cfg_net->use_ssl);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "changed", _cb_ssl, cfg_net);
   elm_box_pack_end(hbox, o);
   evas_object_show(o);

   o = elm_check_add(box);
   elm_object_text_set(o, "Bypass Proxy");
   elm_check_state_set(o, cfg_net->bypass_proxy);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   evas_object_smart_callback_add(o, "changed", _cb_proxy, cfg_net);
   elm_box_pack_end(hbox, o);
   evas_object_show(o);
}
