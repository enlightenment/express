#include "private.h"
#include "options_video.h"
#include "config.h"

static void
_cb_mute_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->video.muted = elm_check_state_get(obj);
   _config_save();
}

static void
_cb_visual_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   _ex_cfg->video.visualized = elm_check_state_get(obj);
   _config_save();
}

static void
_cb_module_changed(void *data EINA_UNUSED, Evas_Object *obj, void *event EINA_UNUSED)
{
   int v;

   v = elm_radio_value_get(obj);
   if (v == _ex_cfg->video.module) return;
   _ex_cfg->video.module = v;
   _config_save();
}

void 
_options_video(Evas_Object *box, Evas_Object *grid EINA_UNUSED)
{
   Evas_Object *o_frame, *vbox, *o, *o_mod;

   /* video frame */
   o_frame = elm_frame_add(box);
   WEIGHT_SET(o_frame, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(o_frame, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_text_set(o_frame, "Video");
   evas_object_show(o_frame);

   vbox = elm_box_add(box);
   elm_box_horizontal_set(vbox, EINA_FALSE);
   elm_box_homogeneous_set(vbox, EINA_FALSE);
   WEIGHT_SET(vbox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   ALIGN_SET(vbox, EVAS_HINT_FILL, EVAS_HINT_FILL);
   elm_object_content_set(o_frame, vbox);
   evas_object_show(vbox);

   o = elm_check_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "Audio Muted");
   elm_check_state_set(o, _ex_cfg->video.muted);
   evas_object_smart_callback_add(o, "changed", _cb_mute_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_check_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "Audio Visualized");
   elm_check_state_set(o, _ex_cfg->video.visualized);
   evas_object_smart_callback_add(o, "changed", _cb_visual_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_separator_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_separator_horizontal_set(o, EINA_TRUE);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_label_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, 0.0, EVAS_HINT_FILL);
   elm_object_text_set(o, "Video Engine:");
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o_mod = o = elm_radio_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "Automatic");
   elm_radio_state_value_set(o, 0);
   evas_object_smart_callback_add(o, "changed", _cb_module_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_radio_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "Gstreamer");
   elm_radio_state_value_set(o, 1);
   elm_radio_group_add(o, o_mod);
   evas_object_smart_callback_add(o, "changed", _cb_module_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_radio_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "Xine");
   elm_radio_state_value_set(o, 2);
   elm_radio_group_add(o, o_mod);
   evas_object_smart_callback_add(o, "changed", _cb_module_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_radio_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "VLC");
   elm_radio_state_value_set(o, 3);
   elm_radio_group_add(o, o_mod);
   evas_object_smart_callback_add(o, "changed", _cb_module_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   o = elm_radio_add(box);
   WEIGHT_SET(o, EVAS_HINT_EXPAND, 0.0);
   ALIGN_SET(o, EVAS_HINT_FILL, 0.5);
   elm_object_text_set(o, "Gstreamer 1.X");
   elm_radio_state_value_set(o, 4);
   elm_radio_group_add(o, o_mod);
   evas_object_smart_callback_add(o, "changed", _cb_module_changed, NULL);
   elm_box_pack_end(vbox, o);
   evas_object_show(o);

   elm_radio_value_set(o, _ex_cfg->video.module);

   elm_box_pack_end(box, o_frame);
}
