#ifndef _GRID_H_
# define _GRID_H_ 1

#define COL_DEF 0
#define COL_BLACK 1
#define COL_RED 2
#define COL_GREEN 3
#define COL_YELLOW 4
#define COL_BLUE 5
#define COL_MAGENTA 6
#define COL_CYAN 7
#define COL_WHITE 8
#define COL_INVIS 9

#define COL_INVERSE 10
#define COL_INVERSEBG 11

typedef struct _Grid_Att Grid_Att;
struct _Grid_Att
{
   unsigned char fg, bg;
   unsigned short bold : 1;
   unsigned short faint : 1;
#if defined(SUPPORT_ITALIC)
   unsigned short italic : 1;
#endif
#if defined(SUPPORT_DBLWIDTH)
   unsigned short dblwidth : 1;
#endif
   unsigned short underline : 1;
   unsigned short blink : 1;
   unsigned short blink2 : 1;
   unsigned short inverse : 1;
   unsigned short invisible : 1;
   unsigned short strike : 1;
   unsigned short fg256 : 1;
   unsigned short bg256 : 1;
   unsigned short fgintense : 1;
   unsigned short bgintense : 1;

   unsigned short autowrapped : 1;
   unsigned short newline : 1;
   unsigned short tab : 1;
   unsigned short fraktur : 1;
#if defined(SUPPORT_80_132_COLUMNS)
   unsigned short is_80_132_mode_allowed : 1;
#endif
};

typedef struct _Grid_Cell Grid_Cell;
struct _Grid_Cell
{
   int codepoint;
   Grid_Att att;
};

typedef struct _Grid_Save Grid_Save;
struct _Grid_Save
{
   unsigned int gen : 8;
   unsigned int comp : 1;
   unsigned int z : 1;
   unsigned int w : 22;
   Grid_Cell cell[1];
};

typedef struct _Grid_Save_Comp Grid_Save_Comp;
struct _Grid_Save_Comp
{
   unsigned int gen : 8;
   unsigned int comp : 1;
   unsigned int z : 1;
   unsigned int w : 22; // compressed size in bytes
   unsigned int wout; // output width in Grid_Cells
};

typedef struct _Grid_State Grid_State;
struct _Grid_State 
{
   int cx, cy;
   Grid_Att att;
   unsigned char charset;
   unsigned char charsetch;
   unsigned char chset[4];
   int scroll_y1, scroll_y2;
   int had_cr_x, had_cr_y;
   int margin_top; // soon, more to come...
   unsigned int multibyte : 1;
   unsigned int alt_kp : 1;
   unsigned int insert : 1;
   unsigned int appcursor : 1;
   unsigned int wrap : 1;
   unsigned int wrapnext : 1;
   unsigned int hidecursor : 1;
   unsigned int crlf : 1;
   unsigned int had_cr : 1;
   unsigned int send_bs : 1;
   unsigned int kbd_lock : 1;
   unsigned int reverse : 1;
   unsigned int no_autorepeat : 1;
   unsigned int cjk_ambiguous_wide : 1;
};

typedef struct _Grid Grid;
struct _Grid
{
   Evas_Object_Smart_Clipped_Data __clipped_data;

   Evas *evas;
   Evas_Object *obj;
   Evas_Object *win;
   Evas_Object *o_event;
   Evas_Object *o_theme;

   int w, h;

   Eina_Unicode *buff;
   int bufflen;
   unsigned char oldbuff[4];

   int scroll;
   int circular_offset, circular_offset2;
   int backmax, backpos, backscroll_num;

   Grid_Cell *cells, *cells2;
   Grid_Save **back;
   Grid_State state, save, swap;

   struct
     {
        int size, chw, chh;
        const char *name;
     } font;

   struct 
     {
        int w, h;
        Evas_Object *obj;
     } grid;

   struct 
     {
        Eina_Bool active : 1;
     } selection;

   struct 
     {
        int cx, cy, button;
     } mouse;

   struct 
     {
        char *str;
        int suspend;
        int x1, y1, x2, y2;
        Eina_List *objs;
        Eina_Bool clicked : 1;
        struct
          {
             Evas_Object *dndobj;
             Evas_Coord x, y;
             Eina_Bool down : 1;
             Eina_Bool dnd: 1;
             Eina_Bool dndobjdel : 1;
          } down;
     } link;

   int nicklen;
   unsigned int altbuf : 1;

   Ecore_Job *mouse_move_job;
   Ecore_Animator *anim;
   Ecore_Timer *delayed_link_up_tmr;
   Ecore_Timer *delayed_size_tmr;
   Ecore_Timer *delayed_mouse_over_tmr;
};

Evas_Object *_grid_add(Evas *evas);
void _grid_update(Evas_Object *obj);
void _grid_window_set(Evas_Object *obj, Evas_Object *win);
void _grid_theme_set(Evas_Object *obj, Evas_Object *theme);
void _grid_resize(Evas_Object *obj, int nw, int nh);
void _grid_text_append(Evas_Object *obj, const char *txt, int len, Row_Color *color);
void _grid_nicklen_set(Evas_Object *obj, int len);
const char *_grid_link_get(Evas_Object *obj);

#define GRID_CELLS(SD, X, Y) \
  SD->cells[X + (((Y + SD->circular_offset) % SD->h) * SD->w)]

#define GRID_FMTCLR(ATT) \
   (ATT).autowrapped = (ATT).newline = (ATT).tab = 0

#endif
