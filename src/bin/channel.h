#ifndef _CHANNEL_H_
# define _CHANNEL_H_ 1

Channel *_channel_create(Evas *evas, Evas_Object *win, const char *name, const char *server);
void _channel_destroy(Channel *chl);
void _channel_update(Channel *chl);
void _channel_focused_set(Channel *chl, Eina_Bool focus);
Eina_Bool _channel_focused_get(Channel *chl);
void _channel_focus(Channel *chl);
void _channel_unfocus(Channel *chl);

void _channel_active_set(Channel *chl, Eina_Bool active);
Eina_Bool _channel_active_get(Channel *chl);

const char *_channel_name_get(Channel *chl);
const char *_channel_server_name_get(Channel *chl);

void _channel_size_update(Channel *chl);
void _channel_size_min_get(Channel *chl, int *w, int *h);
void _channel_size_step_get(Channel *chl, int *w, int *h);
void _channel_size_request_get(Channel *chl, int *w, int *h);
Eina_Bool _channel_size_done_get(Channel *chl);
void _channel_size_done_set(Channel *chl, Eina_Bool done);

Evas_Object *_channel_background_get(Channel *chl);
void _channel_background_hide(Channel *chl);
void _channel_background_show(Channel *chl);

Evas_Object *_channel_base_get(Channel *chl);
void _channel_base_unswallow(Channel *chl);

Evas_Object *_channel_image_get(Channel *chl);
void _channel_image_unset(Channel *chl);
void _channel_image_create(Channel *chl);

Eina_Bool _channel_missed_get(Channel *chl);
void _channel_missed_set(Channel *chl, Eina_Bool missed);
void _channel_missed_count_set(Channel *chl, int count);
void _channel_count_set(Channel *chl, int count, int total);

Eina_Bool _channel_unswallowed_get(Channel *chl);
void _channel_unswallowed_set(Channel *chl, Eina_Bool swallowed);

void _channel_spacer_create(Channel *chl);

void _channel_text_append(Channel *chl, const char *user, const char *txt, Row_Color *color);

void _channel_window_set(Channel *chl, Evas_Object *win);
void _channel_network_set(Channel *chl, Express_Network *net);
Express_Network *_channel_network_get(Channel *chl);

void _channel_userlist_show(Channel *chl);
void _channel_userlist_hide(Channel *chl);
void _channel_userlist_user_append(Channel *chl, const char *user, Eina_Bool op);
void _channel_userlist_go(Channel *chl);

void _channel_tabbar_lbox_create(Channel *chl, Evas_Object *win);
void _channel_tabbar_rbox_create(Channel *chl, Evas_Object *win);
void _channel_tabbar_lbox_append(Channel *chl, Evas_Object *o);
void _channel_tabbar_rbox_append(Channel *chl, Evas_Object *o);
void _channel_tabbar_clear(Channel *chl);
void _channel_tabbar_update(Channel *chl);
void _channel_tabspacer_create(Channel *chl, double v1, double v2);
void _channel_tabspacer_destroy(Channel *chl);

void _channel_treeview_clear(Channel *chl);
Elm_Object_Item *_channel_treeview_group_add(Channel *chl, const char *group);
void _channel_treeview_group_item_add(Channel *pchl, Channel *chl, Elm_Object_Item *group);
void _channel_treeview_item_add(Channel *pchl, Channel *chl);

Eina_Bool _channel_user_find(Channel *chl, const char *user);

#endif
