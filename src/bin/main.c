#include "private.h"
#include "config.h"
#include "theme.h"
#include "window.h"
#include "dbus.h"
#include "gravatar.h"

/* external variables */
int _log_dom = -1;

/* public functions */
EAPI_MAIN int
elm_main(int argc, char **argv)
{
   int i = 0;

   for (; i < argc; i++)
     {
        if ((!strcmp(argv[i], "-v")) || (!strcmp(argv[i], "-version")) ||
            (!strcmp(argv[i], "--version")))
          {
             printf("Version: %s\n", PACKAGE_VERSION);
             exit(0);
          }
     }

   /* create logging domain */
   _log_dom = eina_log_domain_register("express", EXPRESS_DEFAULT_LOG_COLOR);
   if (_log_dom < 0)
     {
        EINA_LOG_CRIT("Could not create logging domain");
        elm_shutdown();
        return EXIT_FAILURE;
     }

   /* try to init config subsystem */
   if (!_config_init()) goto config_err;

   /* try to init express library */
   if (!express_init()) goto express_err;

   _dbus_init();
   gravatar_init();

   /* set elm policy */
   elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);

   /* set elm app directories */
   elm_app_compile_bin_dir_set(PACKAGE_BIN_DIR);
   elm_app_compile_data_dir_set(PACKAGE_DATA_DIR);
   elm_app_compile_locale_set(PACKAGE_LOCALE_DIR);

   /* set elm app info */
   elm_app_info_set(elm_main, PACKAGE_NAME, "themes/default.edj");
   elm_app_name_set("Express");

   /* set elm theme overlay */
   elm_theme_overlay_add(NULL, _theme_default_get());

   /* try to create main window */
   if (!_window_create()) goto win_err;

   /* start main loop */
   elm_run();

   gravatar_shutdown();
   _dbus_shutdown();

   /* destroy main window */
   _window_destroy();

   /* clear elm theme overlay */
   elm_theme_overlay_del(NULL, _theme_default_get());

   /* shutdown express library */
   express_shutdown();

   /* shutdown config subystem */
   _config_shutdown();

   /* unregister log domain */
   eina_log_domain_unregister(_log_dom);
   _log_dom = -1;

   /* shutdown elementary */
   elm_shutdown();

   return EXIT_SUCCESS;

win_err:
   /* clear elm theme overlay */
   elm_theme_overlay_del(NULL, _theme_default_get());

   gravatar_shutdown();
   _dbus_shutdown();

   /* shutdown express library */
   express_shutdown();
express_err:
   /* shutdown config subystem */
   _config_shutdown();
config_err:
   /* unregister log domain */
   eina_log_domain_unregister(_log_dom);
   _log_dom = -1;

   /* shutdown elementary */
   elm_shutdown();

   return EXIT_FAILURE;
}
ELM_MAIN()
